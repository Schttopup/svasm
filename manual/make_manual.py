
#  generate subdocs :
#  - instruction set
#    - opcode, bits, flags, bin instruction, mnem instruction, operands
#  - opcode map
#  - command list
#  - macro list
#  - std lib
#  make index
#  build twice


# instruction operand types : (reg8, reg16, regs)=4 ; (val8, syscall)=8 ; (val16, addr)=16

import os
import json
import subprocess


LATEX_PROGRAM = "pdflatex"
LATEX_PARAMS = ["-file-line-error", "-interaction=nonstopmode", "-synctex=1", "-output-format=pdf"]


def generate_instructions(data):
    def parse_description(desc, operands):
        if isinstance(desc, list):
            desc_string = "\n".join(d.replace("\n", "\\\\") for d in desc)
        else:
            desc_string = desc.replace("\n", "\\\\")
        newstring = ""
        while desc_string:
            tag_idx = desc_string.find("{{")
            if tag_idx >= 0:
                newstring += desc_string[0:tag_idx]
                tag_idx_end = desc_string.find("}}")
                if tag_idx_end < 0:
                    raise ValueError("Missing tag end '}}' in description string")
                tag = desc_string[tag_idx+2:tag_idx_end]
                desc_string = desc_string[tag_idx_end+2:]
                if tag:
                    tag_str = ""
                    while tag:
                        if tag[0].isdigit():
                            numberstr = ""
                            while tag and tag[0].isdigit():
                                numberstr += tag[0]
                                tag = tag[1:]
                            number = int(numberstr)
                            oplist = []
                            for o in operands:
                                if o[number - 1]['name'] not in oplist:
                                    oplist.append(o[number - 1]['name'])
                            names = "/".join(oplist)

                            tag_str += f"<{names}>"
                        else:
                            tag_str += tag[0]
                        tag = tag[1:]
                    newstring += f"\\texttt{{{tag_str}}}"
            else:
                newstring += desc_string
                desc_string = ""
        return newstring

    operand_types = {
        "reg8": "register\\textsubscript{8}",
        "reg16": "register\\textsubscript{16}",
        "regpc": "PC",
        "regsp": "SP",
        "val8": "value\\textsubscript{8}",
        "val16": "value\\textsubscript{16}",
        "addr": "address"
    }
    address_types = {"address": "A", "register": "R"}

    latex_str = ""
    for cat in data["categories"]:
        first_of_section = True
        cat_name = cat["name"]
        if "hidden" in cat and cat["hidden"] is True:
            latex_str += f"\\begin{{unbreakable}}\n\\section*{{{cat_name}}}\n\\addcontentsline{{toc}}{{section}}{{{cat_name}}}\n"
        else:
            latex_str += f"\\begin{{unbreakable}}\n\\section{{{cat_name}}}\n"
        latex_str += f"\\label{{sec:ins_{cat_name.lower().replace(' ', '_')}}}\n"
        index_str = (cat_name if "index" not in cat else cat["index"]) + " instructions"
        latex_str += f"\\index{{{index_str}}}\n"
        for ins in cat["instructions"]:
            if first_of_section:
                first_of_section = False
            else:
                latex_str += f"\\begin{{unbreakable}}\n"
            ins_name = ins["name"]
            ins_desc = ins["description"]
            ins_desc = parse_description(ins_desc, [v["operands"] for v in ins["variants"]])
            latex_str += f"\\subsection{{{ins_name.upper()}}}\\index{{{ins_name}}}\\label{{subsec:ins_{ins_name}}}\n"
            has_bits = any("bits" in v and v["bits"] is not None for v in ins["variants"])
            has_addr = any("addressing" in v and v["addressing"] is not None for v in ins["variants"])
            latex_str += f"\\noindent {ins_desc}\n"
            latex_str += f"\\vspace{{0.5\\baselineskip}}\\\\\n\\noindent\\begin{{tabular}}{{|l|{'l|' if has_bits else ''}{'l|' if has_addr else ''}l|l|Sl|l|l|}}\\hline\n"
            head_str = ("\\textbf{Bits} & " if has_bits else "") + ("\\textbf{Addr.} & " if has_addr else "")
            latex_str += f"\\textbf{{Opcode}} & {head_str}\\textbf{{Operands}} & \\textbf{{Len.}} & \\textbf{{Binary}} & \\textbf{{Instruction}} & \\textbf{{Flags}} \\\\\\hline\\hline\n"
            for var in ins["variants"]:
                latex_str += f"\\texttt{{0x{var['opcode']:02X}}} & "
                if has_bits:
                    latex_str += f"{var['bits']} & " if "bits" in var and var["bits"] is not None else "N/A & "
                if has_addr:
                    latex_str += f"{address_types[var['addressing']]} & " if "addressing" in var and var["addressing"] is not None else "N/A & "
                length = 1
                nb_regs = 0
                last_reg = None
                operands = var["operands"]
                if operands:
                    # operand list
                    operand_list = "\\makecell[l]{"
                    for position, op in enumerate(var["operands"]):
                        if position > 0:
                            operand_list += "\\\\"
                        if "literal" in op:
                            operand_list += f"({position+1}) {op['name']}"
                        else:
                            operand_list += f"({position+1}) {op['name']}: {operand_types[op['type']]}"
                    operand_list += "}"
                    # binary instruction str
                    binary_operands = operands
                    if "binary_operands" in var and var["binary_operands"] is not None:
                        binary_operands = var["binary_operands"]
                    binary_instruction = f"\\makecell[l]{{\\underwrite[0pt]{{\\textbf{{{var['opcode']:02X}}}}}{{}}"
                    for position, op in enumerate(binary_operands):
                        if op["type"] == "val8":
                            length += 1
                            binary_instruction += f" \\underwrite{{vv}}{{{position+1}}}"
                        elif op["type"] == "val16":
                            length += 2
                            binary_instruction += f" \\underwrite{{vv vv}}{{{position+1}}}"
                        elif op["type"] == "addr":
                            length += 2
                            binary_instruction += f" \\underwrite{{aa aa}}{{{position+1}}}"
                        else:  # register
                            if nb_regs % 2 == 0:
                                length += 1
                                last_reg = position+1
                            else:
                                binary_instruction += f" \\underwrite{{r}}{{{position+1}}}\\underwrite{{r}}{{{last_reg}}}"
                                last_reg = None
                            nb_regs += 1
                    if last_reg is not None:
                        binary_instruction += f" \\underwrite[0pt]{{0}}{{}}\\underwrite{{r}}{{{last_reg}}}"
                        last_reg = None
                    binary_instruction += "}"
                    # code instruction str
                    code_instruction = f"{ins_name}"
                    if "operand_order" in var and var["operand_order"]:
                        order = var["operand_order"]
                    else:
                        order = range(len(operands))
                    for position, o in enumerate(order):
                        op = operands[o]
                        if position > 0:
                            code_instruction += ","
                        if "literal" in op:
                            code_instruction += f" {op['literal']}"
                        else:
                            code_instruction += f" <{op['name']}>"
                else:
                    operand_list = "N/A"
                    code_instruction = f"{ins_name}"
                    binary_instruction = f"\\textbf{{\\texttt{{{var['opcode']:02X}}}}}"
                latex_str += f"{operand_list} & "
                latex_str += f"{length} & "
                latex_str += f"{binary_instruction} & "
                latex_str += f"\\texttt{{{code_instruction}}} & "
                if "flags" in var and var["flags"] is not None:
                    latex_str += ", ".join(var["flags"])
                else:
                    latex_str += "N/A"
                latex_str += "\\\\\\hline\n"

            latex_str += "\\end{tabular}\n\\end{unbreakable}\n"
    return latex_str


def generate_opcodes(data):
    address_types = {"address": "A", "register": "R"}
    opcodes = {}
    for cat in data["categories"]:
        for ins in cat["instructions"]:
            for var in ins["variants"]:
                opcodes[var["opcode"]] = {
                    "name": ins["name"],
                    "bits": var["bits"] if "bits" in var else None,
                    "addressing": var["addressing"] if "addressing" in var else None
                }
    opcodes_str = "\\begin{tabular}{|r||c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}\\hline"
    opcodes_str += "&x0&x1&x2&x3&x4&x5&x6&x7&x8&x9&xA&xB&xC&xD&xE&xF\\\\\\hline\\hline"
    opcode_list = sorted(opcodes.keys())
    max_opcode = max(opcode_list)
    op_x = 0
    op_y = 0
    while True:
        if op_y * 16 + op_x > max_opcode:
            break
        if op_x == 0:
            opcodes_str += f"{op_y:X}x & "
        op_n = op_y * 16 + op_x
        if op_n in opcodes:
            op = opcodes[op_n]
            if op["bits"] or op["addressing"]:
                opcodes_str += "\\makecell{"
            opcodes_str += f"\\hyperref[subsec:ins_{op['name']}]{{{op['name'].upper()}}}"
            if op["bits"] or op["addressing"]:
                opcodes_str += "\\\\\\small{"
                if op["bits"]:
                    opcodes_str += f"{op['bits']}b"
                if op["bits"] and op["addressing"]:
                    opcodes_str += " "
                if op["addressing"]:
                    opcodes_str += f"{address_types[op['addressing']]}"
                opcodes_str += "}}"
        op_x += 1
        if op_x >= 16:
            op_x = 0
            op_y += 1
            opcodes_str += "\\\\\\hline\n"
        else:
            opcodes_str += " & "
    opcodes_str += "\\end{tabular}"
    return opcodes_str


def generate_commands(data):
    def parse_description(desc, arguments, current):
        if isinstance(desc, list):
            desc_string = "\n".join(desc)
        else:
            desc_string = desc
        newstring = ""
        while desc_string:
            tag_idx = desc_string.find("{{")
            if tag_idx >= 0:
                newstring += desc_string[0:tag_idx]
                tag_idx_end = desc_string.find("}}")
                if tag_idx_end < 0:
                    raise ValueError("Missing tag end '}}' in description string")
                tag = desc_string[tag_idx+2:tag_idx_end]
                desc_string = desc_string[tag_idx_end+2:]
                if tag:
                    number = int(tag)
                    arg_name = arguments[number-1]["name"]
                    newstring += f"\\textit{{{arg_name}}}"
            else:
                newstring += desc_string
                desc_string = ""
        desc_string = newstring
        newstring = ""
        while desc_string:
            tag_idx = desc_string.find("[[")
            if tag_idx >= 0:
                newstring += desc_string[0:tag_idx]
                tag_idx_end = desc_string.find("]]")
                if tag_idx_end < 0:
                    raise ValueError("Missing tag end ']]' in description string")
                tag = desc_string[tag_idx+2:tag_idx_end]
                desc_string = desc_string[tag_idx_end+2:]
                if tag:
                    if tag == current:
                        newstring += f"\\texttt{{.{tag}}}"
                    elif " " in tag:
                        newstring += f"\\texttt{{{tag}}}"
                    else:
                        newstring += f"\\hyperref[subsec:cmd_{tag}]{{\\texttt{{.{tag}}}}}"
            else:
                newstring += desc_string
                desc_string = ""
        return newstring
    
    latex_str = ""
    for cat in data["categories"]:
        first_of_section = True
        cat_name = cat["name"]
        if "hidden" in cat and cat["hidden"] is True:
            latex_str += f"\\begin{{unbreakable}}\n\\section*{{{cat_name}}}\n\\addcontentsline{{toc}}{{section}}{{{cat_name}}}\n"
        else:
            latex_str += f"\\begin{{unbreakable}}\n\\section{{{cat_name}}}\n"
        latex_str += f"\\label{{sec:cmd_{cat_name.lower().replace(' ', '_')}}}\n"
        index_str = (cat_name if "index" not in cat else cat["index"]) + " commands"
        latex_str += f"\\index{{{index_str}}}\n"
        for cmd in cat["commands"]:
            if first_of_section:
                first_of_section = False
            else:
                latex_str += f"\\begin{{unbreakable}}\n"
            cmd_name = cmd["name"]
            cmd_desc = cmd["description"]
            cmd_desc = parse_description(cmd_desc, None, cmd_name)
            latex_str += f"\\subsection{{{cmd_name}}}\\index{{{cmd_name}@.{cmd_name}}}\\label{{subsec:cmd_{cmd_name}}}\n"
            latex_str += f"{cmd_desc}\n"
            if "variants" in cmd:
                latex_str += "\\begin{description}\n"
                for var in cmd["variants"]:
                    if "arguments" in var and var["arguments"]:
                        var_argnames = " ".join(f"<{a['name']}{'...' if 'repeatable' in a and a['repeatable'] is True else ''}>" for a in var["arguments"])
                        latex_str += f"\\item[\\texttt{{.{cmd_name} {var_argnames}}}]\\hfill\\\\\n"
                    else:
                        latex_str += f"\\item[\\texttt{{.{cmd_name}}}]\\hfill\\\\\n"
                    var_desc = var["description"]
                    if "arguments" in var and var["arguments"]:
                        var_desc = parse_description(var_desc, var["arguments"], cmd_name)
                    else:
                        var_desc = parse_description(var_desc, None, cmd_name)
                    latex_str += f"{var_desc}\n\n"
                    if "arguments" in var and var["arguments"]:
                        latex_str += "\\begin{tabular}{|c|c|c|l|}\\hline\n\\textbf{Argument} & \\textbf{Type} & \\textbf{Eval.} & \\textbf{Description}\\\\\\hline\\hline\n"
                        for arg in var["arguments"]:
                            latex_str += f"\\textit{{{arg['name']}}} & "
                            latex_str += f"{arg['type']} & "
                            latex_str += "yes & " if "evaluated" not in arg or arg["evaluated"] is True else "no & "
                            latex_str += f"{arg['description']}\\\\\\hline\n"
                        latex_str += "\\end{tabular}\n"
                latex_str += "\\end{description}\n"
            latex_str += "\\end{unbreakable}\n"
    return latex_str


def generate_macros(data):
    def parse_description(desc, arguments, current):
        if isinstance(desc, list):
            desc_string = "\n".join(desc)
        else:
            desc_string = desc
        newstring = ""
        while desc_string:
            tag_idx = desc_string.find("{{")
            if tag_idx >= 0:
                newstring += desc_string[0:tag_idx]
                tag_idx_end = desc_string.find("}}")
                if tag_idx_end < 0:
                    raise ValueError("Missing tag end '}}' in description string")
                tag = desc_string[tag_idx+2:tag_idx_end]
                desc_string = desc_string[tag_idx_end+2:]
                if tag:
                    number = int(tag)
                    arg_name = arguments[number-1]["name"]
                    newstring += f"\\textit{{{arg_name}}}"
            else:
                newstring += desc_string
                desc_string = ""
        desc_string = newstring
        return desc_string

    latex_str = ""
    for cat in data["categories"]:
        first_of_section = True
        cat_name = cat["name"]
        if "hidden" in cat and cat["hidden"] is True:
            latex_str += f"\\begin{{unbreakable}}\n\\section*{{{cat_name}}}\n\\addcontentsline{{toc}}{{section}}{{{cat_name}}}\n"
        else:
            latex_str += f"\\begin{{unbreakable}}\n\\section{{{cat_name}}}\n"
        latex_str += f"\\label{{sec:var_{cat_name.lower().replace(' ', '_')}}}\n"
        index_str = (cat_name if "index" not in cat else cat["index"]) + " macros"
        latex_str += f"\\index{{{index_str}}}\n"
        for mac in cat["macros"]:
            if first_of_section:
                first_of_section = False
            else:
                latex_str += f"\\begin{{unbreakable}}\n"
            mac_name = mac["name"].replace("_", "\\_")
            mac_name_subsec = mac["name"]
            mac_desc = mac["description"]
            mac_desc = parse_description(mac_desc, None, mac_name)
            latex_str += f"\\subsection{{{mac_name}}}\\index{{{mac_name}@\\_{mac_name}}}\\label{{subsec:mac_{mac_name_subsec}}}\n"
            latex_str += f"{mac_desc}\n"
            if "variants" in mac:
                latex_str += "\\begin{description}\n"
                for var in mac["variants"]:
                    if "arguments" in var and var["arguments"]:
                        var_argnames = ", ".join(f"<{a['name']}>" if a['name'] else "..." for a in var["arguments"])
                        latex_str += f"\\item[\\texttt{{\\_{mac_name}({var_argnames})}}]\\hfill\\\\\n"
                    else:
                        latex_str += f"\\item[\\texttt{{\\_{mac_name}()}}]\\hfill\\\\\n"
                    var_desc = var["description"]
                    if "arguments" in var and var["arguments"]:
                        var_desc = parse_description(var_desc, var["arguments"], mac_name)
                    else:
                        var_desc = parse_description(var_desc, None, mac_name)
                    latex_str += f"{var_desc}\n\n"
                    latex_str += "\\begin{tabular}{|c|c|c|l|}\\hline\n\\textbf{Argument} & \\textbf{Type} & \\textbf{Description}\\\\\\hline\\hline\n"
                    if "arguments" in var and var["arguments"]:
                        for arg in var["arguments"]:
                            latex_str += f"\\textit{{{arg['name'] or '...'}}} & "
                            latex_str += f"{arg['type']} & "
                            latex_str += f"{arg['description']}\\\\\\hline\n"
                    latex_str += f"\\hline\n\\textbf{{Return}} & {var['return']['type']} & {var['return']['description']}\\\\\\hline\n"
                    latex_str += "\\end{tabular}\n"
                latex_str += "\\end{description}\n"
            latex_str += "\\end{unbreakable}\n"
    return latex_str


def generate_variables(data):
    latex_str = "\\begin{tabular}{|r|l|}\n\\hline\n\\textbf{Name} & \\textbf{Value}\\\\\\hline\\hline\n"
    for v in data["variables"]:
        name = v['name'].replace("_", "\\_")
        latex_str += f"\\index{{{name}@\\_{name}}}\\label{{var_{v['name']}}} "
        latex_str += f"\\texttt{{\\_{name}}} & {v['description']}\\\\\n"
    latex_str += "\\hline\n\\end{tabular}\n"
    return latex_str


def make_index(source, build_folder):
    if os.path.exists(os.path.join(build_folder, source)):
        subprocess.run(["makeindex", source], cwd=build_folder)


def build_document(source, build_folder, runs=1):
    output_dir_arg = f"-output-directory={build_folder}"
    aux_dir_arg = f"-aux-directory={build_folder}"
    args = [LATEX_PROGRAM] + LATEX_PARAMS + [output_dir_arg, aux_dir_arg, source]
    for _ in range(runs):
        subprocess.run(args)


def main():
    main_build_folder = os.path.join(os.curdir, "build")
    if not os.path.exists(main_build_folder):
        os.mkdir(main_build_folder)
    subdoc_build_folder = os.path.join(os.curdir, "subdocs/build")
    if not os.path.exists(subdoc_build_folder):
        os.mkdir(subdoc_build_folder)

    with open("subdocs/instructions.json", "r") as f:
        instruction_data = json.load(f)
        latex_instructions = generate_instructions(instruction_data)
        latex_opcodes = generate_opcodes(instruction_data)
    with open("subdocs/build/instructions.tex", "w") as f:
        f.write(latex_instructions)
    with open("subdocs/build/opcodes.tex", "w") as f:
        f.write(latex_opcodes)

    with open("subdocs/commands.json", "r") as f:
        command_data = json.load(f)
        latex_commands = generate_commands(command_data)
    with open("subdocs/build/commands.tex", "w") as f:
        f.write(latex_commands)

    with open("subdocs/macros.json", "r") as f:
        macro_data = json.load(f)
        latex_macros = generate_macros(macro_data)
    with open("subdocs/build/macros.tex", "w") as f:
        f.write(latex_macros)

    with open("subdocs/variables.json", "r") as f:
        variable_data = json.load(f)
        latex_variables = generate_variables(variable_data)
    with open("subdocs/build/variables.tex", "w") as f:
        f.write(latex_variables)

    make_index("sv_assembly.idx", main_build_folder)
    build_document("sv_assembly.tex", main_build_folder, 1)


if __name__ == '__main__':
    main()
