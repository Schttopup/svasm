\documentclass[twoside, openright]{report}

\usepackage[margin=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[hidelinks]{hyperref}
\usepackage{nameref}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{array}
\usepackage{makecell}
\usepackage{cellspace}
\usepackage{pdflscape}
\usepackage{indentfirst}
\usepackage{listings}
\usepackage{imakeidx}
\usepackage[nobottomtitles]{titlesec}
\usepackage[bottom]{footmisc}
\usepackage{csquotes}

\usepackage{calc}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric,shapes,calc,arrows,positioning}
\newcommand\textbox[3]{\parbox[c][#1][c]{#2}{\centering#3}}

\newcommand{\underwrite}[3][]{\(\genfrac{}{}{#1}{}{\textstyle \text{\texttt{#2}}}{\scriptstyle \text{\texttt{#3}}}\)}
\newenvironment{unbreakable}{\par\nobreak\vfil\penalty0\vfilneg\vtop\bgroup}{\par\xdef\tpd{\the\prevdepth}\egroup\prevdepth=\tpd}

\title{SV Assembly Reference Manual}
\author{Schttopup}

\setcounter{tocdepth}{1}
\makeindex[intoc]

\begin{document}
	\maketitle
	\tableofcontents
	\listoffigures
	
	\chapter*{Introduction}
	\addcontentsline{toc}{chapter}{Introduction}
	\label{ch:intro}
		The \textit{SV Assembly Reference Manual} provides a technical reference to SV Assembly.
		It describes its architecture as well as the execution and programming environments.
		
		\section*{SV Assembly}
        \addcontentsline{toc}{section}{SV Assembly}
			SV Assembly is a scripting language designed to run in a virtual machine as an extension of host applications.
			SV Assembly source code can be compiled to an intermediate bytecode which is interpreted by the virtual machine.
			The primary focus of SV Assembly is to provide a fast scripting language suitable for multiple independent and small-sized instances;
			as such, it introduces limitations compared to other programming languages, but allows for efficient general purpose calculations while leaving the possibility of accelerating domain-specific tasks.
		
		\section*{Overview of this manual}
        \addcontentsline{toc}{section}{Overview of this manual}
			A description of this manual's contents follows :
			\begin{description}
				\item[\nameref{ch:intro}] presents the SV Assembly and gives an overview of the contents of this manual.
				\item[Chapter~\ref{ch:environment} - \nameref{ch:environment}] details the virtual processor architecture and memory model.
				\item[Chapter~\ref{ch:format} - \nameref{ch:format}] presents the binary format used by the assembly's instructions and data.
				\item[Chapter~\ref{ch:instructions} - \nameref{ch:instructions}] lists the instruction set of the assembly.
				\item[Chapter~\ref{ch:opcodes} - \nameref{ch:opcodes}] shows a map of opcodes by binary value.
                \item[Chapter~\ref{ch:syntax} - \nameref{ch:syntax}] details the syntax of SV Assembly sources.
                \item[Chapter~\ref{ch:commands} - \nameref{ch:commands}] lists the commands of the SV Assembly language.
                \item[Chapter~\ref{ch:macros} - \nameref{ch:macros}] documents the built-in macros of the SV Assembly language.
                \item[Chapter~\ref{ch:lib} - \nameref{ch:lib}] documents the API of the SV Assembly standard library.
				\item[Chapter~\ref{ch:svb} - \nameref{ch:svb}] details the structure of the output files produced by compiling assembly code.
			\end{description}
	
	\chapter{Execution environment}
	\label{ch:environment}
		SV Assembly uses a virtual machine as its execution environment.
		This allows for fine control over resource usage and security.
		The virtual machine is a self-contained processor with its own set of registers, as well as its own memory space.
				
		\section{Memory model}
			\subsection{Memory size}
				\label{sec:memsize}
				 As SV Assembly is designed for multiple small-sized instances, it uses 16-bit memory addressing without bank switching, which allows for up to 64KiB of addressable memory.
				 It is composed of two distinct segments: constant data and variable data.
				 These can be of any size, as long as they do not overlap and their combined size do not excess 64KiB.
				 The segments may be non-contiguous;
				 in that case, unused memory space does not need to be allocated.
				 Figure~\ref{fig:memory} shows various layout examples.
				 
				\begin{figure}
					\begin{subfigure}[t]{0.3\textwidth}
						\centering
						\input{figures/memory1.tex}
						\caption[Default layout]{Full default layout. Constant segment starts at 0 and is 32KiB long, variable segment starts at 32KiB and is 32KiB long.}
						\label{fig:memory1}
					\end{subfigure}
					\hfill
					\begin{subfigure}[t]{0.3\textwidth}
						\centering
						\input{figures/memory2.tex}
						\caption[Custom layout]{Custom layout. Constant segment starts at 0 and is 8KiB long, variable segment starts at 40KiB and is 4KiB long.}
						\label{fig:memory2}
					\end{subfigure}
					\hfill
					\begin{subfigure}[t]{0.3\textwidth}
						\centering
						\input{figures/memory3.tex}
						\caption[Invalid layout]{Invalid layout. Constant segment does not start at 0 and the two segments overlap.}
						\label{fig:memory3}
					\end{subfigure}
					\caption{Memory layout examples}
					\label{fig:memory}
				\end{figure}
				 
			\subsection{Constant data segment}
				The constant data segment holds the bytecode that is executed by the virtual machine.
				As its name suggests, it is read-only and cannot be modified.
				Thanks to that, it is possible to use a single constant data segment for multiple identical and independent instances, thus avoiding duplication.

				This segment always starts at address 0, and is not optional (it must be at least 1 byte long).

			\subsection{Variable data segment}
				The variable data segment holds the runtime variables that the SV Assembly program needs.
				It has read and write permissions.
				This segment is not initialized when the program starts, and unlike the constant data segment, it cannot be used to store persistent data.
				However, this segment also has permission for execution: this allows for dynamic code loading or generation.

				The variable data segment has no size or location requirements other than those detailed in section~\ref{sec:memsize}.
				It is possible to completely omit this segment, but doing so would prevent the SV Assembly program to use variable data.

			\subsection{Stack space}
				An SV Assembly program may use a stack handled by stack-specific instructions.
				This stack may be of any size and location within the variable data segment.
				Stack space should not be used to store regular variables.
				Using the stack is optional, but not defining a stack space prevents the program to use stack-specific instructions.
			
		\section{Processor architecture}
			At any given time during the execution of an SV Assembly program, the state of the processor may be represented by a set of registers and flags.

			\subsection{Registers}
				The SV Assembly virtual processor contains 8 general-purpose 16-bit registers, each of which being able to be split into two 8-bit registers.
				Two special registers are internal and may only be read under certain conditions.
				\begin{figure}
					\centering
					\input{figures/registers.tex}
					\caption[SV Assembly virtual processor registers]{SV Assembly virtual processor registers. In white are the general-purpose registers, in light gray the accumulator, and in dark gray the special registers.}
				\label{fig:registers}
				\end{figure}

				\subsubsection{General-purpose registers}
					All 8 general-purpose registers may be read from and written to at any given time.
					These registers are named \texttt{A} to \texttt{D} and \texttt{W} to \texttt{Z};
					each one of them may be split into two packed 8-bit registers by appending \texttt{L} or \texttt{H} for the respective low and high byte of the register.
					That way, the same registers may be used with 8-bit and 16-bit instructions.
					8-bit instructions operating on one half of a 16-bit register do not modify the other half of this register.

				\subsubsection{Accumulator}
					The register \texttt{A} (and its 8-bit parts, \texttt{AL} and \texttt{AH}) is known as the \textit{accumulator}.
					While still being a general-purpose register, thus sharing their properties, it is used as an output by all bitwise and arithmetic instructions.
					The results of 16-bit instructions are stored in the whole \texttt{A} register, and those of 8-bit instructions in the \texttt{AL} register.
					\texttt{AH} is not modified by 8-bit instructions.

				\subsubsection{Special registers}
					Two registers, \texttt{PC} and \texttt{SP}, are considered \textit{special registers}.
					They are not writable nor readable, and are used internally by the processor.
					The program counter \texttt{PC} holds the address of the current instruction being executed;
					it is incremented after each instruction by its length in bytes, or explicitly modified for some instructions (\hyperref[sec:ins_jumps]{jumps}, \hyperref[subsec:ins_call]{\texttt{CALL}} and \hyperref[subsec:ins_ret]{\texttt{RET}}).
					The stack pointer \texttt{SP} holds the address of the top of the stack;
					it is modified by \hyperref[sec:ins_stack_management]{stack-related instructions}.

                    Although these registers are not directly readable, their content may be copied to any general-purpose 16-bit register with the \hyperref[subsec:ins_mv]{\texttt{MV}} instruction.

			\subsection{Flags}
				The processor contains 3 flags that can be checked and set by conditional jumps, arithmetic operations and comparison instructions.
				\begin{description}
					\item[Carry (C)] This flag keeps track of the carry in bitwise and arithmetic instructions.
						It is set in case of overflow, and cleared otherwise. It can also be set and cleared by dedicated instructions.
						It may be used to perform logic and arithmetic on data over 2 bytes, and for conditional branching.
					\item[Zero (Z)] This flag is set when instructions yield a value of 0, and is cleared otherwise.
						It is used for conditional branching.
					\item[Test (T)] This flag is exclusively used for conditional branching.
						Comparator instructions may set or clear this flag depending on their condition.
				\end{description}
				Instructions that are not concerned by a flag do not alter its value.

			\subsection{System calls}
				One central feature of SV Assembly is its ability to call native functions from the virtual machine.
				Such functions are named \textit{system calls}.
				Like common system calls of operating system kernels, they are able to perform computations outside of the program and provide I/O functionality, and they may access and alter all processor registers and flags as well as variable memory.
				They may for instance be used to accelerate specific computationally expensive workloads or to provide access to a file system.
				System calls are implementation-defined: there is no guarantee that one virtual machine has the same set of system calls as another.
				They should be used only with their corresponding virtual machine implementation.

			\subsection{Errors}
				 Whenever an error occurs (e.g invalid memory access, invalid instruction\ldots), the current instruction is required to have no effect.
				However, further error management is undefined and implementation-specific, it is up to the implementation to decide if the program should be stopped, if an error flag should be set, or any other action.
				An SV Assembly program shall avoid errors and not rely on undefined behavior.
	
	\chapter{Instruction format}
	\label{ch:format}
		\section{Endianness}
			SV Assembly uses big-endian ordering.
			This ensures that when reading a value with the wrong size, the value only loses precision, instead of discarding its most significant bytes.
			This is especially useful when treating integer values as fixed-point numbers.

			TODO : insert figure for endianness

		\section{Instructions}
			SV Assembly instructions are all 1-byte long.
			However, most of them are followed by one or more operands that specify memory addresses, registers or raw values.
			Memory addresses are always 16-bit long.
			Raw values are 8-bit or 16-bit long, depending on the type of instruction.
			If an instruction takes only one register operand (potentially with other non-register operands), it is stored on the 4 least significant bits and the rest of the byte is unused.
			If an instruction takes two register operands, the fist one is stored in the 4 LSB and the second in the 4 MSB.
			Register binary values are listed in figure~\ref{fig:registervalues}.
			\begin{figure}[H]
				\centering
				\begin{tabular}{|r|ccc|}
					\hline
					Register & Full & High & Low\\\hline\hline
					A & 0x0 & 0x0 & 0x1\\
					B & 0x2 & 0x2 & 0x3\\
					C & 0x4 & 0x4 & 0x5\\
					D & 0x6 & 0x6 & 0x7\\\hline
					W & 0x8 & 0x8 & 0x9\\
					X & 0xA & 0xA & 0xB\\
					Y & 0xC & 0xC & 0xD\\
					Z & 0xE & 0xE & 0xF\\\hline
				\end{tabular}
				\caption{Register binary values}
				\label{fig:registervalues}
			\end{figure}

		\section{Data}
			SV Assembly programs can directly handle 8-bit and 16-bit unsigned integers.
			However, special instructions allow calculations to be performed on multiple bytes, thus expanding the range of representable numbers (albeit at a slower speed).
			Moreover, depending on the implementation, system calls may be used to manage other data types.
	
	\chapter{Instruction set}
	\label{ch:instructions}
        \input{subdocs/build/instructions.tex}
		
	\begin{landscape}
	    \chapter{Opcode map}
		\label{ch:opcodes}
		    \input{subdocs/build/opcodes.tex}
	\end{landscape}

	\chapter{SV Assembly syntax}
    \label{ch:syntax}

		\section{Statements}
        \label{sec:statements}
            SV Assembly source files are composed of a sequence of statements.
            Each line of the source file contains zero or one statement; it may also be prefixed by a label, and/or end with a comment.
            Statements cannot span across multiple lines.

            There are three types of statements : instructions, commands, and macro calls.

			\subsection{Instructions}
            \label{subsec:stat_instructions}
                Instructions are statements that instruct the assembler to generate the corresponding bytecode at the current address.
                An instruction statement is composed of several parts: one instruction keyword, corresponding to the mnemonic of the instruction to generate, in lowercase; followed by the appropriate number of operands of that instruction, separated by commas (\texttt{,}).

                Operands may be any valid expression, as long as they evaluate to a \hyperref[subsec:val_number]{number}, a \hyperref[subsec:val_register]{register}, or a \hyperref[subsec:val_label]{label} (see \hyperref[sec:values]{section \ref{sec:values}}).
                The order of operands follows the logic of the Intel syntax, where the destination operand comes first, followed by the source operand.

                See \hyperref[ch:instructions]{chapter~\ref{ch:instructions}} for mnemonics, operand counts and operand types of SV Assembly instructions.

                \begin{figure}[H]
                    \centering
                    \texttt{ldi reg\_function(), :label}
                    \caption[Instruction statement example]{Example of an instruction statement with multiple operands. The first operand is an expression evaluating to a register, the second operand is a label. Both operands are separated by a comma.}
                    \label{fig:stat_instruction}
                \end{figure}

			\subsection{Commands}
            \label{subsec:stat_commands}
                Commands are statements that control the flow of the assembly process, in a broad sense.
                A command statement is composed of several parts: the command keyword, in lowercase, prefixed by a dot (\texttt{.}); followed by the appropriate number of expressions, separated by whitespace.
                Command arguments may or may not be expanded before the command is invoked, and is up to the command itself.
                For instance, macro definitions do not expand the macro calls they are composed of.

                A detailed list of the available commands and their usage is provided in \hyperref[ch:commands]{chapter~\ref{ch:commands}}.

                \begin{figure}[H]
                    \centering
                    \texttt{.set variable get\_value()}
                    \caption[Commmand statement example]{Example of a command statement with multiple arguments. The first argument is not expanded, but the second is.}
                    \label{fig:stat_command}
                \end{figure}

			\subsection{Macro calls}
            \label{subsec:stat_macros}
                Macro calls are statements that tell the assembler to call a macro at the current location.
                Such macros are detailed in subsection~\hyperref[subsubsec:macro_function]{\ref{subsubsec:macro_function}}.

                A macro call statement is composed of a single macro call expression: the name of the macro to call, followed by a pair of parentheses (\texttt{()}), inside which the arguments (which can be any valid expression) are separated by commas (\texttt{,}).
                Macro call arguments are always expanded before the macro is called.

                \begin{figure}[H]
                    \centering
                    \texttt{MACRO\_CALL(constant, nested())}
                    \caption[Macro call statement example]{Example of a macro call statement. Here, the second argument is a nested macro call.}
                    \label{fig:stat_macro}
                \end{figure}

            \subsection{Labels}
            \label{subsec:stat_labels}
                Labels are not statements.
                They instruct the assembler to save the current address in the specified label.
                If the label already exists, it is overwritten, but generates a warning.
                Label references are assigned to the last defined label of the same name at the end of the assembly process.

                A label is composed of the label name followed by a colon character (\texttt{:}). A label name may include alphanumeric characters and underscores, but must not start with a digit.

                \begin{figure}[H]
                    \centering
                    \texttt{label:}
                    \caption[Label example]{Example of a label definition.}
                    \label{fig:stat_label}
                \end{figure}

			\subsection{Comments}
            \label{subsec:stat_comments}
                Comments are not statements.
                They do not interact with the assembly process in any way, and their only use is to provide information or documentation about the code they are associated to.
                Comments start with a semicolon character (\texttt{;}), and extend until the end of the line.
                They may be placed right after a statement, as well as alone on their own line.

                \begin{figure}[H]
                    \centering
                    \texttt{<statement> ; Comment\\; Another comment}
                    \caption[Comment example]{Example of two comments, one after a statement, the other by itself.}
                    \label{fig:stat_comment}
                \end{figure}

		\section{Macro system}
        \label{sec:macros}
            SV Assembly supports an extensive macro system.
            Macros can be defined, assigned, overwritten and recalled at any point in the assembly source (with a few exceptions\footnote{Macros can be shadowed by local macros of the same name, in which case they are not accessible. Block sections also prevent global macros to be set.}).

            The term \enquote{macro} is used in a broad sense, as SV Assembly macros may be callable as well as constant.
            As such, a macro can expand to a different macro.

            All macro definitions are global by default, but can be made local with the \hyperref[subsec:cmd_local]{\texttt{.local}} command.
            Global macros are accessible anytime they are defined; local macros can only be accessed in the scope they are defined in.
            If a local macro is defined with the same name as an existing global macro, the global macro is shadowed and a warning is issued.

			\subsection{Expressions}
            \label{subsec:expressions}
                Expressions designate anything that can be expanded to a constant macro.
                They can range from simple constant macros to nested macro calls and arrays.

                \hyperref[sec:values]{Values} are the simplest form of expressions, as they directly expand to themselves.
                \hyperref[subsec:val_label]{Labels} and \hyperref[subsec:val_array]{arrays} can hold nested expressions, respectively as offsets and contained values.
                Macro calls are expressions, and their arguments are expressions as well.

			\subsection{Macro types}
            \label{subsec:macro_types}
                SV Assembly handles four different macro types: \hyperref[subsubsec:macro_constant]{\textbf{constant}}, \hyperref[subsubsec:macro_function]{\textbf{function}}, \hyperref[subsubsec:macro_inline]{\textbf{inline}} and \hyperref[subsubsec:macro_block]{\textbf{block}}.

                \subsubsection{Constant}
                \label{subsubsec:macro_constant}
                    Constant macros only hold a value.
                    They are defined using the \hyperref[subsec:cmd_set]{\texttt{.set}} command\footnote{The \hyperref[subsec:cmd_local]{\texttt{.local}} command, if provided with a second argument, can be used to set a local constant directly without having to call the two distinct commands \texttt{.local} and \texttt{.set}.}.
                    Upon definition, the expression is evaluated to its final value.
                    As their name suggest, constants are immutable: the value is copied when it is used in a macro call, an instruction or a command, and it is destroyed and replaced if it is updated with a new value.

                \subsubsection{Function}
                \label{subsubsec:macro_function}
                    Function macros may contain any sequence of statements that could normally occur at the place the macro is called.
                    They are defined using the \hyperref[subsec:cmd_macro]{\texttt{.macro}} command.

                    Function macros can only be called as a statement, and not as an expression.
                    They can be called in block macros at any stack depth, as long as the function macro doesn't contain anything that would be forbidden in block macros\footnote{Instructions, labels, and certain commands are not allowed in such contexts.}.
                    They do not return any value.

                \subsubsection{Inline}
                \label{subsubsec:macro_inline}
                    Inline macros map a macro call to another macro call, and return the latter's value.
                    They are defined using the \hyperref[subsec:cmd_define]{\texttt{.define}} command.

                    Inline macros can only be called as expressions, and not as a statement.

                \subsubsection{Block}
                \label{subsubsec:macro_block}
                    Block macros are the mix of function and inline macros: they contain a sequence of statements and return a value.
                    They are defined using the \hyperref[subsec:cmd_block]{\texttt{.block}} command.

                    Block macros can only be called as expressions, and not as a statement.

            \subsection{Scopes}
            \label{subsec:scopes}
                Macro scopes define the locations at which a macro can be read or assigned.
                There are two different scope types : global and local.
                Global macros may be accessed at any scope, as long as they are not shadowed by a local macro of the same name.
                Local macros may be accessed only in the scope they are defined in, and in subsequent scopes, up to a macro call.
                All macro types may be global or local.

                By default, macros are created global.
                To create a local macro, the command \hyperref[subsec:cmd_local]{\texttt{.local}} is used

                Scope example with multiple levels, local \& globals


		\section{Values}
        \label{sec:values}
			SV Assembly instructions, commands and macros accept different types of values to represent their data.
			These values can be literal, in that case the value itself is specified directly where it is used.
			They can also be stored in constants, to be recalled later by specifying the constant's symbol name.

			SV Assembly allows 5 value types : \hyperref[subsec:val_number]{\textbf{number}}, \hyperref[subsec:val_string]{\textbf{string}}, \hyperref[subsec:val_register]{\textbf{register}}, \hyperref[subsec:val_label]{\textbf{label}} and \hyperref[subsec:val_array]{\textbf{array}}.
			A sixth possibility is the absence of a value, which is called \hyperref[subsec:val_nil]{\textbf{nil}}.

			\subsection{Number}
				\label{subsec:val_number}
				Number values can hold 64-bit signed integers.
				Arithmetic, bitwise and logic operations can be performed on them.
				Decimal numbers (base 10) are represented with the number itself with no additional symbol, hexadecimal numbers (base 16) are preceded with a dollar sign (\texttt{\$}) and binary numbers (base 2) are preceded with an ampersand symbol (\texttt{\&}).
				There is no octal (base 8) representation.
				Negative numbers are prefixed with a minus sign (\texttt{-}), before any potential base indicator.

				The truth value of a number is equal to 0 if the number is null, and 1 if it is non-null.
				The type of a number is \enquote{number}.
				The string representation of a number is the same as its syntax, and a base ( \([2;36]\) ) may be specified.

			\subsection{String}
				\label{subsec:val_string}
				String values can hold arrays of characters.
				They are represented by the characters they are composed of, enclosed in quote characters (\texttt{"}).
				Escape characters can be used to represent special characters that are otherwise not allowed in strings, as shown in figure~\ref{fig:val_stresc}.
				Raw ASCII values can be inserted by specifying \texttt{\textbackslash xAB}, where AB is the two-digit hexadecimal representation of the character's ASCII code.
				\begin{figure}[H]
					\centering
					\begin{tabular}{|c|c|c|}
						\hline
						Character & ASCII code & Escape character\\\hline
						Backslash & 0x00 & \texttt{\textbackslash\textbackslash}\\\hline
						Quotation mark & 0x00 & \texttt{\textbackslash "}\\\hline
						Apostrophe & 0x00 & \texttt{\textbackslash '}\\\hline
						New line & 0x00 & \texttt{\textbackslash n}\\\hline
						Carriage return & 0x00 & \texttt{\textbackslash r}\\\hline
						Tabulation & 0x00 & \texttt{\textbackslash t}\\\hline
						Raw ASCII value & Any & \texttt{\textbackslash xAB}\\\hline
					\end{tabular}
					\caption{String escape characters}
					\label{fig:val_stresc}
				\end{figure}
				The truth value of a string is equal to 0 if the string is empty, and 1 if it contains 1 character or more.
				The type of a string is \enquote{string}.
				The string representation of a string is the same as its syntax, including the enclosing quote characters; non-printable characters are escaped.

			\subsection{Register}
				\label{subsec:val_register}
				Register values can represent any register of a SV processor, including read-only special registers.
				They are represented by the name of the register in lowercase, prefixed by a percentage symbol (\texttt{\%}).
				For instance, register A is represented as \texttt{\%a}, register SP is represented as \texttt{\%sp}.
				The truth value of a register is 1 in all cases.
				The type of a register is \enquote{register}.
				The string representation of a register is the same as its syntax.

			\subsection{Label}
				\label{subsec:val_label}
				References to labels are represented by specifying the name of the label preceded by a colon character (\texttt{:}).
				They may optionally contain an offset, by adding a \texttt{+} or a \texttt{-} character, depending on the sign of the desired offset, followed by an expression.
				This expression must expand to an integer in the range \([-65535;65535]\); additionally, the sum of the label address and the offset must be in addressable range.

				Offsets are evaluated at variable expansion time, but the label address is evaluated after the translation phase.
				This allows labels to be referenced before they appear in the code, but separates the final address computation in two parts: extra care should be taken in case two labels with the same name coexist, as the final address is the last encountered one.
				Figure~\ref{fig:val_label} shows an example of a function used as a positive offset for a label.
				\begin{figure}[H]
					\centering
					\texttt{:label\_name + offset\_function()}
					\caption[Label value example with offset]{Example of a label with an offset. The label \texttt{label} is referenced, calling the function \texttt{offset\_function} as offset.}
					\label{fig:val_label}
				\end{figure}
				The truth value of a label is 1 in all cases.
				The type of a label is \enquote{label}.
				The string representation of a label is composed of a colon character, followed by the label name, a \texttt{+} or a \texttt{-} (depending on the sign of the offset), then its offset.
				If the offset is 0, the sign and offset value are omitted.
				A base ( \([2;36]\) ) may be applied to the offset.

			\subsection{Array}
				\label{subsec:val_array}
				Arrays are containers that contain multiple other values of any type.
				They are represented by a pair of square brackets (\texttt{[]}) inside of which their inner values are specified, one by one, separated by commas (\texttt{,}), as shown in figure~\ref{fig:val_array}.
				As for labels, they can contain expressions which are expanded when the value is evaluated.
				Arrays may be nested, and there is no depth limit.
				\begin{figure}[H]
					\centering
					\texttt{[123, "text", \%a, []]}
					\caption[Array value example]{Example of an array containing different value types, including another array.}
					\label{fig:val_array}
				\end{figure}
				The truth value of an array is equal to 0 if the array is empty, and 1 if it contains 1 element or more (even if said elements have null truth values themselves).
				The type of an array is \enquote{array}.
				The string representation of an array is composed of a pair of square brackets, enclosing the string representation of the elements of the array, each separated by a comma followed by a space.
				A base ( \([2;36]\) ) may be recursively applied to the elements of the array.

			\subsection{Nil}
				\label{subsec:val_nil}
				Nil values represent the absence of a value.
				These values do not have a representation, but the built-in macro \hyperref[var_nil]{\texttt{\_nil}} returns a nil value.
				The truth value of nil is 0 in all cases.
				The type of nil is \enquote{nil}.
				The string representation of nil is \enquote{nil} (without quotes).

        \section{Grammar definition}
            \lstinputlisting{syntax.txt}

	\chapter{Assembler commands}
    \label{ch:commands}
        \input{subdocs/build/commands.tex}

	\chapter{Built-in macros}
    \label{ch:macros}
        \input{subdocs/build/macros.tex}

    \chapter{Built-in variables}
    \label{ch:variables}
        \input{subdocs/build/variables.tex}

	\chapter{Standard library}
    \label{ch:lib}
	
	\chapter{SV Binary files}
	\label{ch:svb}
		Output files produced by compiling SV Assembly are structured with SSF. The tree name of SV Binary files is "SVB\textvisiblespace" (note the trailing space).
		They are divided in four chunks:
		\begin{description}
			\item[HEAD chunk:] Contains the size of the constant and variable segments, the start address of the variable segment, and the start address and size of the stack.
				All values are 16-bit unsigned integers with big-endian ordering.
			\item[DATA chunk:] Contains the content of the constant data segment.
			\item[EXPT chunk:] Contains the list of exported memory addresses.
				Each export is composed of one 16-bit unsigned integer with big-endian ordering, followed by an ASCII zero-terminated string.
				Exports follow each other without padding.
				This chunk is optional.
			\item[META chunk:] Contains the list of key-value pairs of metadata.
                Each metadata is composed the key, immediately followed by the corresponding value, both of which ASCII zero-terminated strings.
                Metadata pairs follow each other without padding.
				This chunk is optional.
		\end{description}

    \printindex

\end{document}