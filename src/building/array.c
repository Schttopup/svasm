/**
 * \file array.c
 * \brief Defines functions related to argument arrays.
 * \author Schttopup
 */

#include "array.h"
#include <sdefs.h>
#include "arg.h"


SVAsm_Array* SVAsm_array_create(void)
{
    SVAsm_Array *array = NULL;
    MMALLOC(array, SVAsm_Array, 1);
    array->args = NULL;
    array->argCount = 0;
    return array;
}

SVAsm_Array* SVAsm_array_copy(SVAsm_Array *array)
{
    if(!array)
        return NULL;
    SVAsm_Array *newArray = NULL;
    MMALLOC(newArray, SVAsm_Array, 1);
    newArray->args = NULL;
    MMALLOC(newArray->args, struct SVAsm_Arg_*, array->argCount);
    for(size_t i = 0; i < array->argCount; i++)
        newArray->args[i] = SVAsm_arg_copy(array->args[i]);
    newArray->argCount = array->argCount;
    return newArray;
}

void SVAsm_array_destroy(SVAsm_Array *array)
{
    if(!array)
        return;
    for(size_t i = 0; i < array->argCount; i++)
        SVAsm_arg_destroy(array->args[i]);
    FFREE(array->args);
    FFREE(array);
}


struct SVAsm_Arg_* SVAsm_array_get(SVAsm_Array *array, size_t index)
{
    if(!array)
        return NULL;
    if(index >= array->argCount)
        return NULL;
    return array->args[index];
}

void SVAsm_array_set(SVAsm_Array *array, size_t index, struct SVAsm_Arg_ *arg)
{
    if(!array)
        return;
    if(index >= array->argCount)
        return;
    SVAsm_arg_destroy(array->args[index]);
    array->args[index] = arg;
}

void SVAsm_array_insert(SVAsm_Array *array, size_t index, struct SVAsm_Arg_ *arg)
{
    if(!array)
        return;
    if(index > array->argCount)
        return;
    array->argCount++;
    RREALLOC(array->args, struct SVAsm_Arg_*, array->argCount);
    for(size_t i = array->argCount - 1; i > index; i--)
        array->args[i] = array->args[i - 1];
    array->args[index] = arg;
}

void SVAsm_array_remove(SVAsm_Array *array, size_t index)
{
    if(!array)
        return;
    if(index >= array->argCount)
        return;
    SVAsm_arg_destroy(array->args[index]);
    array->argCount--;
    for(size_t i = index; i < array->argCount; i++)
        array->args[i] = array->args[i + 1];
    RREALLOC(array->args, struct SVAsm_Arg_*, array->argCount)
}
