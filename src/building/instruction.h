/**
 * \file instruction.h
 * \brief Declares structures and functions related to processor instructions.
 * \author Schttopup
 */

#ifndef SVASM_INSTRUCTION_H
#define SVASM_INSTRUCTION_H

#include "macrocall.h"
#include "arg.h"
#include "../parsing/token.h"
#include "../parsing/register.h"


/**
 * \brief Instruction keywords.
 */
enum SVAsm_Keyword_
{
    SVASM_KEYWORD_NONE,
    SVASM_KEYWORD_NOP,

    SVASM_KEYWORD_LDI,
    SVASM_KEYWORD_LD,
    SVASM_KEYWORD_LDA,
    SVASM_KEYWORD_ST,
    SVASM_KEYWORD_STA,
    SVASM_KEYWORD_MV,

    SVASM_KEYWORD_NOT,
    SVASM_KEYWORD_OR,
    SVASM_KEYWORD_AND,
    SVASM_KEYWORD_XOR,
    SVASM_KEYWORD_SHL,
    SVASM_KEYWORD_SHR,
    SVASM_KEYWORD_ROL,
    SVASM_KEYWORD_ROR,

    SVASM_KEYWORD_ADD,
    SVASM_KEYWORD_SUB,
    SVASM_KEYWORD_ADDC,
    SVASM_KEYWORD_SUBC,
    SVASM_KEYWORD_SC,
    SVASM_KEYWORD_CC,
    SVASM_KEYWORD_INC,
    SVASM_KEYWORD_DEC,
    SVASM_KEYWORD_PUSH,
    SVASM_KEYWORD_POP,

    SVASM_KEYWORD_JMP,
    SVASM_KEYWORD_JZ,
    SVASM_KEYWORD_JNZ,
    SVASM_KEYWORD_JC,
    SVASM_KEYWORD_JNC,
    SVASM_KEYWORD_JT,
    SVASM_KEYWORD_JNT,
    SVASM_KEYWORD_CALL,
    SVASM_KEYWORD_RET,
    SVASM_KEYWORD_BRK,
    SVASM_KEYWORD_SYS,

    SVASM_KEYWORD_CEQ,
    SVASM_KEYWORD_CNEQ,
    SVASM_KEYWORD_CLT,
    SVASM_KEYWORD_CNLT,
    SVASM_KEYWORD_CGT,
    SVASM_KEYWORD_CNGT,

    SVASM_KEYWORD_LAST
};

/// Type name for instruction keywords.
typedef enum SVAsm_Keyword_ SVAsm_Keyword;

/// String values for instruction keywords.
extern char const * const SVAsm_keywordValues[SVASM_KEYWORD_LAST];


/**
 * \brief Operand types.
 */
enum SVAsm_OperandType_
{
    SVASM_OPERAND_NONE,
    SVASM_OPERAND_REGISTER,
    SVASM_OPERAND_NUMBER,
    SVASM_OPERAND_SYMBOL,
    SVASM_OPERAND_MACRO,
    SVASM_OPERAND_LABEL
};

/// Type name for operand types.
typedef enum SVAsm_OperandType_ SVAsm_OperandType;


struct SVAsm_MacroCall_;

/**
 * \brief Instruction operand.
 */
struct SVAsm_Operand_
{
    SVAsm_OperandType type;         //!< Operand type
    union SVAsm_OperandValue_       //!< Operand value
    {
        SVAsm_Register reg;         //!< Register value
        SVAsm_LabelCall *label;
        SVAsm_NumberType number;    //!< Numeral value
        char *text;                 //!< Symbol value
        struct SVAsm_MacroCall_ *macro;
    } value;
};

/// Type name for operand structure.
typedef struct SVAsm_Operand_ SVAsm_Operand;


/**
 * \brief Processor instruction.
 */
struct SVAsm_Instruction_
{
    SVAsm_Keyword keyword;
    SVAsm_Operand **operands;
    size_t operandCount;
};

/// Type name for processor instructions.
typedef struct SVAsm_Instruction_ SVAsm_Instruction;


/**
 * \brief Creates an instruction operand.
 * \param type The operand type
 * \param value The operand value
 * \return The created operand
 */
SVAsm_Operand *SVAsm_operand_create(SVAsm_OperandType type, void *value);

SVAsm_Operand *SVAsm_operand_copy(SVAsm_Operand *operand);

SVAsm_Operand *SVAsm_operand_fromArg(SVAsm_Arg *arg);

/**
 * \brief Destroys an instruction operand.
 * \param operand The operand to destroy
 */
void SVAsm_operand_destroy(SVAsm_Operand *operand);


/**
 * \brief Creates an instruction.
 * \param keyword The keyword of the instruction
 * \return The created instruction
 */
SVAsm_Instruction *SVAsm_instruction_create(SVAsm_Keyword keyword);

/**
 * \brief Destroys an instruction.
 * \param instruction The instruction to destroy
 */
void SVAsm_instruction_destroy(SVAsm_Instruction *instruction);

/**
 * \brief Adds an operand to an instruction.
 * \param instruction The instruction
 * \param operand The operand to add
 */
void SVAsm_instruction_addOperand(SVAsm_Instruction *instruction, SVAsm_Operand *operand);

/**
 * \brief Associates a keyword value to a string.
 * \param text The string
 * \return The keyword
 */
SVAsm_Keyword SVAsm_instruction_getKeyword(char const *text);


#endif //SVASM_INSTRUCTION_H
