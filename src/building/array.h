/**
 * \file array.h
 * \brief Declares structures and functions related to argument arrays.
 * \author Schttopup
 */

#ifndef SVASM_ARRAY_H
#define SVASM_ARRAY_H


#include <stdlib.h>


struct SVAsm_Arg_;

/**
 * Argument array.
 */
struct SVAsm_Array_
{
    struct SVAsm_Arg_ **args;   //!< Argument list
    size_t argCount;            //!< Argument count
};

/// Type name for argument arrays
typedef struct SVAsm_Array_ SVAsm_Array;


/**
 * Creates an argument array.
 * \return The created argument array
 */
SVAsm_Array* SVAsm_array_create(void);

/**
 * Copies an argument array. Inner arguments are copied as well.
 * \param array The array to copy
 * \return The copied array
 */
SVAsm_Array* SVAsm_array_copy(SVAsm_Array *array);

/**
 * Destroys an array.
 * \param array The array to destroy
 */
void SVAsm_array_destroy(SVAsm_Array *array);


/**
 * Gets an array element at a given position.
 * \param array The array to get the element from
 * \param index The index of the element
 * \return The corresponding element, or NULL if the index is out of bounds
 */
struct SVAsm_Arg_* SVAsm_array_get(SVAsm_Array *array, size_t index);

/**
 * Sets a value for an array element at a given position.
 * \param array The array to set the element in
 * \param index The index of the element
 * \param arg The new value for the element
 */
void SVAsm_array_set(SVAsm_Array *array, size_t index, struct SVAsm_Arg_ *arg);

/**
 * Inserts an element in an array at a given position.
 * \param array The array to insert an element in
 * \param index The index of the element before which to insert the new element
 * \param arg The element to insert
 */
void SVAsm_array_insert(SVAsm_Array *array, size_t index, struct SVAsm_Arg_ *arg);

/**
 * Removes an element from an array at a given position.
 * \param array The array to remove the element from
 * \param index The index of the element to remove
 */
void SVAsm_array_remove(SVAsm_Array *array, size_t index);


#endif //SVASM_ARRAY_H
