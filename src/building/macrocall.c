/**
 * \file macrocall.c
 * \brief Defines functions related to macro calls.
 * \author Schttopup
 */

#include "macrocall.h"
#include <sdefs.h>
#include <stdlib.h>
#include <string.h>


SVAsm_MacroCall* SVAsm_macroCall_create(char const *name)
{
    if(!name)
        return NULL;
    SVAsm_MacroCall *call = NULL;
    MMALLOC(call, SVAsm_MacroCall, 1);
    call->name = NULL;
    MMALLOC(call->name, char, strlen(name) + 1);
    strcpy(call->name, name);
    call->argCount = 0;
    call->args = NULL;
    return call;
}

SVAsm_MacroCall* SVAsm_macroCall_copy(SVAsm_MacroCall *call)
{
    if(!call)
        return NULL;
    SVAsm_MacroCall *newCall = NULL;
    MMALLOC(newCall, SVAsm_MacroCall, 1);
    MMALLOC(newCall->name, char, strlen(call->name) + 1);
    strcpy(newCall->name, call->name);
    newCall->argCount = call->argCount;
    newCall->args = NULL;
    MMALLOC(newCall->args, SVAsm_Arg*, call->argCount);
    for(size_t i = 0; i < call->argCount; i++)
        newCall->args[i] = SVAsm_arg_copy(call->args[i]);
    return newCall;
}

void SVAsm_macroCall_destroy(SVAsm_MacroCall *call)
{
    if(!call)
        return;
    for(size_t i = 0; i < call->argCount; i++)
        SVAsm_arg_destroy(call->args[i]);
    FFREE(call->args);
    FFREE(call->name);
    FFREE(call);
}

void SVAsm_macroCall_add(SVAsm_MacroCall *call, SVAsm_Arg *arg)
{
    if(!call || !arg)
        return;
    RREALLOC(call->args, SVAsm_Arg*, call->argCount + 1);
    call->args[call->argCount] = arg;
    call->argCount++;
}

