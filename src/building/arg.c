/**
 * \file arg.c
 * \brief Defines functions related to command and macro arguments.
 * \author Schttopup
 */

#include "arg.h"
#include "macrocall.h"
#include <string.h>
#include <sdefs.h>
#include <stdbool.h>
#include <stdio.h>


char const *SVAsm_argTypeValues[SVASM_ARG_LAST] = {
        "nil",
        "number",
        "symbol",
        "string",
        "macro",
        "label",
        "register",
        "array"
};


SVAsm_Arg* SVAsm_arg_create(SVAsm_ArgType type, void const *value)
{
    if(type != SVASM_ARG_NONE && !value)
        return NULL;
    SVAsm_Arg *arg = NULL;
    MMALLOC(arg, SVAsm_Arg, 1);
    arg->type = type;
    switch(type)
    {
        case SVASM_ARG_NONE:
            break;
        case SVASM_ARG_NUMBER:
            arg->value.number = (*(SVAsm_NumberType*)value);
            break;
        case SVASM_ARG_REGISTER:
            arg->value.reg = (*(SVAsm_Register*)value);
            break;
        case SVASM_ARG_SYMBOL:
        case SVASM_ARG_STRING:
            arg->value.text = NULL;
            MMALLOC(arg->value.text, char, strlen((char*)value) + 1);
            strcpy(arg->value.text, value);
            break;
        case SVASM_ARG_LABEL:
            arg->value.label = (SVAsm_LabelCall*)value;
            break;
        case SVASM_ARG_MACRO:
            arg->value.macro = (SVAsm_MacroCall*)value;
            break;
        case SVASM_ARG_ARRAY:
            arg->value.array = (SVAsm_Array*)value;
            break;
        default:
            FFREE(arg);
            return NULL;
    }
    return arg;
}

void SVAsm_arg_destroy(SVAsm_Arg *arg)
{
    if(!arg)
        return;
    switch(arg->type)
    {
        case SVASM_ARG_STRING:
        case SVASM_ARG_SYMBOL:
            FFREE(arg->value.text);
            break;
        case SVASM_ARG_LABEL:
            SVAsm_labelCall_destroy(arg->value.label);
            break;
        case SVASM_ARG_MACRO:
            SVAsm_macroCall_destroy(arg->value.macro);
            break;
        case SVASM_ARG_ARRAY:
            SVAsm_array_destroy(arg->value.array);
            break;
        default:
            break;
    }
    FFREE(arg);
}

SVAsm_Arg* SVAsm_arg_copy(SVAsm_Arg *arg)
{
    if(!arg)
        return NULL;
    SVAsm_Arg *copy = NULL;
    switch(arg->type)
    {
        case SVASM_ARG_NONE:
            copy = SVAsm_arg_create(arg->type, NULL);
            break;
        case SVASM_ARG_SYMBOL:
        case SVASM_ARG_STRING:
            copy = SVAsm_arg_create(arg->type, arg->value.text);
            break;
        case SVASM_ARG_NUMBER:
            copy = SVAsm_arg_create(arg->type, &(arg->value.number));
            break;
        case SVASM_ARG_REGISTER:
            copy = SVAsm_arg_create(arg->type, &(arg->value.reg));
            break;
        case SVASM_ARG_LABEL:
            copy = SVAsm_arg_create(arg->type, SVAsm_labelCall_copy(arg->value.label));
            break;
        case SVASM_ARG_MACRO:
            {
                SVAsm_MacroCall *macro = SVAsm_macroCall_copy(arg->value.macro);
                copy = SVAsm_arg_create(arg->type, macro);
            }
            break;
        case SVASM_ARG_ARRAY:
            {
                SVAsm_Array *array = SVAsm_array_copy(arg->value.array);
                copy = SVAsm_arg_create(arg->type, array);
            }
            break;
        default:
            break;
    }
    return copy;
}


char* SVAsm_arg_toString(SVAsm_Arg *arg, unsigned int base)
{
    if(!arg)
        return NULL;
    char *newString = NULL;
    switch(arg->type)
    {
        case SVASM_ARG_NONE:
            MMALLOC(newString, char, 4);
            strcpy(newString, "nil");
            break;
        case SVASM_ARG_NUMBER:
            {
                char buffer[100] = {'\0'};
                SVAsm_NumberType n = arg->value.number;
                bool negative = false;
                int r = 0;
                size_t len = 0;
                bool done = false;
                if(n < 0)
                    negative = true;
                while(!done)
                {
                    r = llabs(n % base);
                    n /= base;
                    buffer[len] = r < 10 ? (char)(r + '0') : (char)(r - 10 + 'A');
                    len++;
                    if(n == 0)
                        done = true;
                }
                if(negative)
                    buffer[len++] = '-';
                MMALLOC(newString, char, len + 1);
                for(size_t i = 0; i < len; i++)
                    newString[len - i - 1] = buffer[i];
                newString[len] = '\0';
            }
            break;
        case SVASM_ARG_STRING:
            MMALLOC(newString, char, strlen(arg->value.text) + 3);
            sprintf(newString, "\"%s\"", arg->value.text);
            break;
        case SVASM_ARG_LABEL:
            {
                MMALLOC(newString, char, strlen(arg->value.label->label) + 2);
                sprintf(newString, ":%s", arg->value.label->label);
                size_t len = strlen(newString);
                if(arg->value.label->offset)
                {
                    char *innerString = SVAsm_arg_toString(arg->value.label->offset, base);
                    size_t innerStringLen = strlen(innerString);
                    RREALLOC(newString, char, len + innerStringLen + 2);
                    sprintf(newString + len, "%c%s", arg->value.label->negative ? '-' : '+', innerString);
                    FFREE(innerString);
                }
            }
            break;
        case SVASM_ARG_REGISTER:
            MMALLOC(newString, char, strlen(SVAsm_registerValues[arg->value.reg]) + 2);
            sprintf(newString, "%%%s", SVAsm_registerValues[arg->value.reg]);
            break;
        case SVASM_ARG_ARRAY:
            {
                size_t len = 1;
                MMALLOC(newString, char, len + 1);
                strcpy(newString, "[");
                for(size_t i = 0; i < arg->value.array->argCount; i++)
                {
                    char *innerString = SVAsm_arg_toString(arg->value.array->args[i], base);
                    size_t innerStringLen = strlen(innerString);
                    RREALLOC(newString, char, len + innerStringLen + 1);
                    strcpy(newString + len, innerString);
                    len += innerStringLen;
                    FFREE(innerString);
                    if(i < arg->value.array->argCount - 1)
                    {
                        RREALLOC(newString, char, len + 3);
                        strcpy(newString + len, ", ");
                        len += 2;
                    }
                }
                RREALLOC(newString, char, len + 2);
                strcpy(newString + len, "]");
            }
            break;
        default:
            break;
    }
    return newString;
}

bool SVAsm_arg_truth(SVAsm_Arg *arg)
{
    if(!arg)
        return false;
    switch(arg->type)
    {
        case SVASM_ARG_NONE:
            return false;
            break;
        case SVASM_ARG_NUMBER:
            if(arg->value.number != 0)
                return true;
            return false;
            break;
        case SVASM_ARG_STRING:
            if(strlen(arg->value.text) > 0)
                return true;
            return false;
            break;
        case SVASM_ARG_ARRAY:
            if(arg->value.array->argCount > 0)
                return true;
            return false;
            break;
        case SVASM_ARG_LABEL:
        case SVASM_ARG_REGISTER:
            return true;
            break;
        default:
            return false;
            break;
    }
    return false;
}


bool SVAsm_arg_equal(SVAsm_Arg *arg1, SVAsm_Arg *arg2)
{
    if(!arg1 || !arg2)
        return false;
    if(arg1->type != arg2->type)
        return false;
    switch(arg1->type)
    {
        case SVASM_ARG_NONE:
            return true;
            break;
        case SVASM_ARG_NUMBER:
            return arg1->value.number == arg2->value.number;
            break;
        case SVASM_ARG_STRING:
            return strcmp(arg1->value.text, arg2->value.text) == 0;
            break;
        case SVASM_ARG_REGISTER:
            return arg1->value.reg == arg2->value.reg;
            break;
        case SVASM_ARG_LABEL:
            {
                if((arg1->value.label->offset != NULL) != (arg2->value.label->offset != NULL))
                    return false;
                bool labEq = strcmp(arg1->value.label->label, arg2->value.label->label) == 0;
                bool offEq = arg1->value.label->offset ? SVAsm_arg_equal(arg1->value.label->offset, arg2->value.label->offset) : true;
                bool negEq = arg1->value.label->offset ? arg1->value.label->negative == arg2->value.label->negative : true;
                return  labEq && offEq && negEq;
            }
            break;
        case SVASM_ARG_ARRAY:
            if(arg1->value.array->argCount != arg2->value.array->argCount)
                return false;
            for(size_t i = 0; i < arg1->value.array->argCount; i++)
            {
                if(!SVAsm_arg_equal(arg1->value.array->args[i], arg2->value.array->args[i]))
                    return false;
            }
            return true;
            break;
        default:
            return false;
            break;
    }
    return false;
}
