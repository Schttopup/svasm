/**
 * \file command.h
 * \brief Declares structures and functions related to commands.
 * \author Schttopup
 */

#ifndef SVASM_COMMAND_H
#define SVASM_COMMAND_H

#include "arg.h"


/**
 * \brief Command names.
 */
enum SVAsm_Command_
{
    SVASM_COMMAND_NONE,
    SVASM_COMMAND_ORIGIN,
    SVASM_COMMAND_EXPORT,
    SVASM_COMMAND_META,
    SVASM_COMMAND_INCLUDE,
    SVASM_COMMAND_BINARY,
    SVASM_COMMAND_LOAD,
    SVASM_COMMAND_BYTES,
    SVASM_COMMAND_SPACE,
    SVASM_COMMAND_STACK,
    SVASM_COMMAND_DEFINE,
    SVASM_COMMAND_SET,
    SVASM_COMMAND_ASSIGN,
    SVASM_COMMAND_DELETE,
    SVASM_COMMAND_LOCAL,
    SVASM_COMMAND_UNLOCAL,
    SVASM_COMMAND_CALL,
    SVASM_COMMAND_IF,
    SVASM_COMMAND_ELSE,
    SVASM_COMMAND_FOR,
    SVASM_COMMAND_WHILE,
    SVASM_COMMAND_MACRO,
    SVASM_COMMAND_BLOCK,
    SVASM_COMMAND_END,
    SVASM_COMMAND_RETURN,
    SVASM_COMMAND_MESSAGE,
    SVASM_COMMAND_WARNING,
    SVASM_COMMAND_ERROR,
    SVASM_COMMAND_LAST
};

/// Type name for command names.
typedef enum SVAsm_Command_ SVAsm_Command;

/// String values for command names.
extern char const * const SVAsm_commandValues[SVASM_COMMAND_LAST];




/**
 * \brief Command line structure.
 */
struct SVAsm_CommandLine_
{
    SVAsm_Command command;
    SVAsm_Arg **args;
    size_t argCount;
};

/// Type name for command line.
typedef struct SVAsm_CommandLine_ SVAsm_CommandLine;


/**
 * \brief Creates a new command line.
 * \param command The corresponding command
 * \return The created command line
 */
SVAsm_CommandLine* SVAsm_commandLine_create(SVAsm_Command command);

/**
 * \brief Destroys a command line.
 * \param line The command line to destroy
 */
void SVAsm_commandLine_destroy(SVAsm_CommandLine *line);

/**
 * \brief Adds an argument to a command line.
 * \param line The command line
 * \param arg The argument to add
 */
void SVAsm_commandLine_add(SVAsm_CommandLine *line, SVAsm_Arg *arg);

/**
 * Associates a command name to a string
 * \param text The string
 * \return The command name
 */
SVAsm_Command SVAsm_command_getCommand(char const *text);


bool SVAsm_command_isArgExpandable(SVAsm_Command command, size_t index);


#endif //SVASM_COMMAND_H
