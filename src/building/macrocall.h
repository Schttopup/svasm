/**
 * \file macrocall.h
 * \brief Declares structures and functions related to macro calls.
 * \author Schttopup
 */

#ifndef SVASM_MACROCALL_H
#define SVASM_MACROCALL_H

#include "arg.h"
#include "command.h"
#include "../translation/macro.h"
#include "../translation/builtins.h"


/**
 * Macro call.
 */
struct SVAsm_MacroCall_
{
    char *name;             //!< Macro name
    SVAsm_Arg **args;       //!< Macro arguments
    size_t argCount;        //!< Macro argument call count
};

/// Type name for macro calls
typedef struct SVAsm_MacroCall_ SVAsm_MacroCall;


struct SVAsm_Translator_;

/// Type name for built-in macros
typedef SVAsm_Arg* (*SVAsm_MacroTranslation)(struct SVAsm_Translator_*, SVAsm_MacroCall*);


/**
 * Creates a new macro call.
 * \param name The macro name
 * \return The created macro call
 */
SVAsm_MacroCall* SVAsm_macroCall_create(char const *name);

SVAsm_MacroCall* SVAsm_macroCall_copy(SVAsm_MacroCall *call);

/**
 * Destroys a macro call.
 * \param call The macro call to destroy
 */
void SVAsm_macroCall_destroy(SVAsm_MacroCall *call);

/**
 * Adds an argument to a macro call.
 * \param call The macro call
 * \param arg The argument to add
 */
void SVAsm_macroCall_add(SVAsm_MacroCall *call, SVAsm_Arg *arg);


#endif //SVASM_MACROCALL_H
