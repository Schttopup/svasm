/**
 * \file arg.h
 * \brief Declares structures and functions related to command and macro arguments.
 * \author Schttopup
 */

#ifndef SVASM_ARG_H
#define SVASM_ARG_H


#include "../parsing/register.h"
#include "../translation/labelcall.h"
#include "array.h"
#include <stdint.h>
#include "../parsing/token.h"


/**
 * Argument type.
 */
enum SVAsm_ArgType_
{
    SVASM_ARG_NONE,     //!< No data
    SVASM_ARG_NUMBER,   //!< Numeral data
    SVASM_ARG_SYMBOL,   //!< Symbol data
    SVASM_ARG_STRING,   //!< String data
    SVASM_ARG_MACRO,    //!< Macro call
    SVASM_ARG_LABEL,    //!< Label reference
    SVASM_ARG_REGISTER, //!< Register data
    SVASM_ARG_ARRAY,    //!< Arg array
    SVASM_ARG_LAST
};

/// Type name for argument type.
typedef enum SVAsm_ArgType_ SVAsm_ArgType;

extern char const *SVAsm_argTypeValues[SVASM_ARG_LAST];


struct SVAsm_MacroCall_;

/**
 * Command or macro argument.
 */
struct SVAsm_Arg_
{
    SVAsm_ArgType type;                 //!< Argument type
    union SVAsm_ArgValue_
    {
        SVAsm_NumberType number;        //!< Numeral value
        char *text;                     //!< Text value
        struct SVAsm_MacroCall_ *macro; //!< Macro call
        SVAsm_LabelCall *label;         //!< Label call
        SVAsm_Register reg;             //!< Register value
        SVAsm_Array *array;             //!< Arg array
    } value;
};

/// Type name for arguments.
typedef struct SVAsm_Arg_ SVAsm_Arg;


/**
 * Creates a new argument.
 * \param type Argument type
 * \param value Argument value
 * \return The created argument
 */
SVAsm_Arg* SVAsm_arg_create(SVAsm_ArgType type, void const *value);

/**
 * Destroys an argument.
 * \param arg The argument to destroy
 */
void SVAsm_arg_destroy(SVAsm_Arg *arg);

/**
 * Copies an argument.
 * \param arg The argument to copy
 * \return The created copy
 */
SVAsm_Arg* SVAsm_arg_copy(SVAsm_Arg *arg);

/**
 * Creates a string from an argument.
 * \param arg The argument to convert
 * \param base The base to use for number conversion
 * \return The created string
 */
char* SVAsm_arg_toString(SVAsm_Arg *arg, unsigned int base);

/**
 * Returns the truth value of an argument.
 * For numbers : true if != 0, false if == 0.
 * For strings and arrays : true if not empty, false if empty.
 * For label calls and registers : true.
 * \param arg The arg to check
 * \return The arg's truth value
 */
bool SVAsm_arg_truth(SVAsm_Arg *arg);

/**
 * Tests equality between two arguments.
 * Returns false if the two arguments have different types.
 * \param arg1 The first argument
 * \param arg2 The second argument
 * \return true if both arguments are equal, false otherwise
 */
bool SVAsm_arg_equal(SVAsm_Arg *arg1, SVAsm_Arg *arg2);


#endif //SVASM_ARG_H
