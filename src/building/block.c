/**
 * \file block.c
 * \brief Defines functions related to building blocks.
 * \author Schttopup
 */

#include "block.h"
#include <sdefs.h>
#include <string.h>


SVAsm_Block* SVAsm_block_create(SVAsm_BlockType type, void *value, unsigned int line, char const *filename, char const *filepath, unsigned int level)
{
    if(type != SVASM_BLOCK_NONE && !value)
        return NULL;
    SVAsm_Block *block = NULL;
    MMALLOC(block, SVAsm_Block, 1);
    block->type = type;
    switch(type)
    {
        case SVASM_BLOCK_INSTRUCTION:
            block->content.instruction = (SVAsm_Instruction*)value;
            break;
        case SVASM_BLOCK_LABEL:
            block->content.text = NULL;
            MMALLOC(block->content.text, char, strlen((char*)value) + 1);
            strcpy(block->content.text, (char*)value);
            break;
        case SVASM_BLOCK_COMMAND:
            block->content.command = (SVAsm_CommandLine*)value;
            break;
        case SVASM_BLOCK_MACRO:
            block->content.macro = (SVAsm_MacroCall*)value;
            break;
        default:
            break;
    }
    block->line = line;
    block->filename = filename;
    block->filepath = filepath;
    block->level = level;
    block->comment = NULL;
    return block;
}

void SVAsm_block_destroy(SVAsm_Block *block)
{
    if(!block)
        return;
    switch(block->type)
    {
        case SVASM_BLOCK_LABEL:
            FFREE(block->content.text);
            break;
        case SVASM_BLOCK_INSTRUCTION:
            SVAsm_instruction_destroy(block->content.instruction);
            break;
        case SVASM_BLOCK_COMMAND:
            SVAsm_commandLine_destroy(block->content.command);
            break;
        case SVASM_BLOCK_MACRO:
            SVAsm_macroCall_destroy(block->content.macro);
        default:
            break;
    }
    if(block->comment)
        FFREE(block->comment);
    FFREE(block);
}

void SVAsm_block_setComment(SVAsm_Block *block, char const *comment)
{
    if(!block || !comment)
        return;
    if(!block->comment)
    {
        MMALLOC(block->comment, char, strlen(comment) + 1);
    }
    else
        RREALLOC(block->comment, char, strlen(comment) + 1);
    strcpy(block->comment, comment);
}

SVAsm_BlockList* SVAsm_blockList_create(void)
{
    SVAsm_BlockList *list = NULL;
    MMALLOC(list, SVAsm_BlockList, 1);
    list->blockCount = 0;
    list->blocks = NULL;
    return list;
}

void SVAsm_blockList_destroy(SVAsm_BlockList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->blockCount; i++)
        SVAsm_block_destroy(list->blocks[i]);
    FFREE(list->blocks);
    FFREE(list);
}

void SVAsm_blockList_add(SVAsm_BlockList *list, SVAsm_Block *block)
{
    if(!list || !block)
        return;
    RREALLOC(list->blocks, SVAsm_Block*, list->blockCount + 1);
    list->blocks[list->blockCount] = block;
    list->blockCount++;
}

SVAsm_Block* SVAsm_blockList_get(SVAsm_BlockList *list, size_t index)
{
    if(!list)
        return NULL;
    if(index >= list->blockCount)
        return NULL;
    return list->blocks[index];
}

void SVAsm_blockList_insert(SVAsm_BlockList *list, SVAsm_BlockList *toAdd, size_t index)
{
    if(!list || !toAdd)
        return;
    if(index > list->blockCount)
        return;
    SVAsm_block_destroy(list->blocks[index]);
    size_t total = list->blockCount + toAdd->blockCount - 1;
    RREALLOC(list->blocks, SVAsm_Block*, total);
    for(size_t i = 0; i < list->blockCount - index - 1; i++)
        list->blocks[total - i - 1] = list->blocks[list->blockCount - i - 1];
    for(size_t i = 0; i < toAdd->blockCount; i++)
        list->blocks[index + i] = toAdd->blocks[i];
    list->blockCount += toAdd->blockCount - 1;
    FFREE(toAdd->blocks);
    FFREE(toAdd);
}
