/**
 * \file instruction.c
 * \brief Defines functions related to processor instructions.
 * \author Schttopup
 */

#include "instruction.h"
#include "arg.h"
#include <sdefs.h>
#include <string.h>


char const * const SVAsm_keywordValues[SVASM_KEYWORD_LAST] = {
        "",
        "nop",

        "ldi",
        "ld",
        "lda",
        "st",
        "sta",
        "mv",

        "not",
        "or",
        "and",
        "xor",
        "shl",
        "shr",
        "rol",
        "ror",

        "add",
        "sub",
        "addc",
        "subc",
        "sc",
        "cc",
        "inc",
        "dec",
        "push",
        "pop",

        "jmp",
        "jz",
        "jnz",
        "jc",
        "jnc",
        "jt",
        "jnt",
        "call",
        "ret",
        "brk",
        "sys",

        "ceq",
        "cneq",
        "clt",
        "cnlt",
        "cgt",
        "cngt"

};


SVAsm_Operand *SVAsm_operand_create(SVAsm_OperandType type, void *value)
{
    if(type == SVASM_OPERAND_NONE)
        return NULL;
    SVAsm_Operand *operand = NULL;
    MMALLOC(operand, SVAsm_Operand, 1);
    operand->type = type;
    switch(type)
    {
        case SVASM_OPERAND_NUMBER:
            operand->value.number = *((SVAsm_NumberType*)value);
            break;
        case SVASM_OPERAND_REGISTER:
            operand->value.reg = *((SVAsm_Register*)value);
            break;
        case SVASM_OPERAND_LABEL:
            operand->value.label = (SVAsm_LabelCall*)value;
            break;
        case SVASM_OPERAND_SYMBOL:
            MMALLOC(operand->value.text, char, strlen((char*)value) + 1);
            strcpy(operand->value.text, (char*)value);
            break;
        case SVASM_OPERAND_MACRO:
            operand->value.macro = (SVAsm_MacroCall*)value;
            break;
        default:
            FFREE(operand);
            return NULL;
    }
    return operand;
}

SVAsm_Operand *SVAsm_operand_copy(SVAsm_Operand *operand)
{
    if(!operand)
        return NULL;
    if(operand->type == SVASM_OPERAND_NONE)
        return NULL;
    SVAsm_Operand *newOperand = NULL;
    MMALLOC(newOperand, SVAsm_Operand, 1);
    newOperand->type = operand->type;
    switch(operand->type)
    {
        case SVASM_OPERAND_NUMBER:
            newOperand->value.number = operand->value.number;
            break;
        case SVASM_OPERAND_REGISTER:
            newOperand->value.reg = operand->value.reg;
            break;
        case SVASM_OPERAND_LABEL:
            newOperand->value.label = SVAsm_labelCall_copy(operand->value.label);
            break;
        case SVASM_OPERAND_SYMBOL:
            MMALLOC(newOperand->value.text, char, strlen(operand->value.text) + 1);
            strcpy(newOperand->value.text, operand->value.text);
            break;
        case SVASM_OPERAND_MACRO:
            newOperand->value.macro = SVAsm_macroCall_copy(operand->value.macro);
            break;
        default:
        FFREE(newOperand);
            return NULL;
    }
    return newOperand;
}

SVAsm_Operand *SVAsm_operand_fromArg(SVAsm_Arg *arg)
{
    if(!arg)
        return NULL;
    SVAsm_Operand *operand = NULL;
    switch(arg->type)
    {
        case SVASM_ARG_NUMBER:
            operand = SVAsm_operand_create(SVASM_OPERAND_NUMBER, &(arg->value.number));
            break;
        case SVASM_ARG_LABEL:
            operand = SVAsm_operand_create(SVASM_OPERAND_LABEL, SVAsm_labelCall_copy(arg->value.label));
            break;
        case SVASM_ARG_REGISTER:
            operand = SVAsm_operand_create(SVASM_OPERAND_REGISTER, &(arg->value.reg));
            break;
        case SVASM_ARG_SYMBOL:
            operand = SVAsm_operand_create(SVASM_OPERAND_SYMBOL, arg->value.text);
            break;
        case SVASM_ARG_MACRO:
            operand = SVAsm_operand_create(SVASM_OPERAND_MACRO, SVAsm_macroCall_copy(arg->value.macro));
            break;
        default:
            FFREE(operand);
            return NULL;
    }
    return operand;
}

void SVAsm_operand_destroy(SVAsm_Operand *operand)
{
    if(!operand)
        return;
    switch(operand->type)
    {
        case SVASM_OPERAND_SYMBOL:
            FFREE(operand->value.text);
            break;
        case SVASM_OPERAND_LABEL:
            SVAsm_labelCall_destroy(operand->value.label);
            break;
        case SVASM_OPERAND_MACRO:
            SVAsm_macroCall_destroy(operand->value.macro);
            break;
        default:
            break;
    }
    FFREE(operand);
}


SVAsm_Instruction *SVAsm_instruction_create(SVAsm_Keyword keyword)
{
    if(keyword == SVASM_KEYWORD_NONE)
        return NULL;
    SVAsm_Instruction *instruction = NULL;
    MMALLOC(instruction, SVAsm_Instruction, 1);
    instruction->keyword = keyword;
    instruction->operands = NULL;
    instruction->operandCount = 0;
    return instruction;
}

void SVAsm_instruction_destroy(SVAsm_Instruction *instruction)
{
    if(!instruction)
        return;
    for(size_t i = 0; i < instruction->operandCount; i++)
        SVAsm_operand_destroy(instruction->operands[i]);
    FFREE(instruction->operands);
    FFREE(instruction);
}

void SVAsm_instruction_addOperand(SVAsm_Instruction *instruction, SVAsm_Operand *operand)
{
    if(!instruction || !operand)
        return;
    RREALLOC(instruction->operands, SVAsm_Operand*, instruction->operandCount + 1);
    instruction->operands[instruction->operandCount] = operand;
    instruction->operandCount++;
}

SVAsm_Keyword SVAsm_instruction_getKeyword(char const *text)
{
    for(unsigned int i = 1; i < SVASM_KEYWORD_LAST; i++)
    {
        if(strcmp(text, SVAsm_keywordValues[i]) == 0)
            return (SVAsm_Keyword)i;
    }
    return SVASM_KEYWORD_NONE;
}

