/**
 * \file builder.c
 * \brief Defines functions related to building.
 * \author Schttopup
 */

#include "builder.h"
#include "block.h"
#include "../error.h"
#include <sdefs.h>
#include <stdbool.h>
#include <string.h>


#define SVASM_BUILDER_IMPROPERSYNTAX() {\
    SVAsm_error(builder->options, SVASM_WARNING, builder->filename, builder->filepath, builder->tokens->tokens[builder->index]->line,\
                "improper syntax");\
    SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);\
    }

#define SVASM_BUILDER_UNEXPECTEDTOKEN() {\
    SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath, builder->tokens->tokens[builder->index]->line,\
                "unexpected token");\
    SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);\
    }


SVAsm_Builder *SVAsm_builder_create(SVAsm_TokenList *tokens, char const *filename, char const *filepath, unsigned int level, SVAsm_Options *options)
{
    if(!tokens)
        return NULL;
    SVAsm_Builder *builder = NULL;
    MMALLOC(builder, SVAsm_Builder, 1);
    builder->blocks = SVAsm_blockList_create();
    builder->tokens = tokens;
    builder->filename = filename;
    builder->filepath = filepath;
    builder->level = level;
    builder->state = SVASM_BSTATE_BLANK;
    builder->stateEnd = true;
    builder->expectEOL = false;
    builder->error = false;
    builder->options = options;
    builder->index = 0;
    return builder;
}

void SVAsm_builder_destroy(SVAsm_Builder *builder)
{
    if(!builder)
        return;
    SVAsm_blockList_destroy(builder->blocks);
    FFREE(builder);
}

void SVAsm_builder_build(SVAsm_Builder *builder)
{
    if(!builder)
        return;
    while(builder->index < builder->tokens->tokenCount && !builder->error)
    {
        if(!builder->stateEnd)
            SVAsm_builder_buildState(builder);
        else
            SVAsm_builder_setState(builder);
    }
}

void SVAsm_builder_buildState(SVAsm_Builder *builder)
{
    if(!builder)
        return;
    switch(builder->state)
    {
        case SVASM_BSTATE_BLANK:
            if(builder->tokens->tokens[builder->index]->type == SVASM_TOKEN_ENDLINE)
            {
                builder->index++;
                builder->expectEOL = false;
            }
            else
                builder->stateEnd = true;
            break;
        case SVASM_BSTATE_COMMENT:
            SVAsm_builder_getComment(builder);
            builder->stateEnd = true;
            builder->expectEOL = true;
            break;
        case SVASM_BSTATE_LABEL:
            if(!SVAsm_builder_getLabel(builder))
            {
                SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                            builder->tokens->tokens[builder->index]->line, "invalid label");
                SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);
                builder->error = true;
            }
            builder->stateEnd = true;
            builder->expectEOL = false;
            break;
        case SVASM_BSTATE_INSTRUCTION:
            if(!SVAsm_builder_getInstruction(builder))
            {
                SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                            builder->tokens->tokens[builder->index]->line, "invalid instruction");
                SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);
                builder->error = true;
            }
            builder->stateEnd = true;
            builder->expectEOL = true;
            break;
        case SVASM_BSTATE_COMMAND:
            if(!SVAsm_builder_getCommand(builder))
            {
                SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                            builder->tokens->tokens[builder->index]->line, "invalid command");
                SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);
                builder->error = true;
            }
            builder->stateEnd = true;
            builder->expectEOL = true;
            break;
        case SVASM_BSTATE_MACRO:
            if(!SVAsm_builder_getMacro(builder))
            {
                SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                            builder->tokens->tokens[builder->index]->line, "invalid macro");
                SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);
                builder->error = true;
            }
            builder->stateEnd = true;
            builder->expectEOL = true;
            break;
    }
}

void SVAsm_builder_setState(SVAsm_Builder *builder)
{
    if(!builder)
        return;
    bool found = false;
    switch(builder->tokens->tokens[builder->index]->type)
    {
        case SVASM_TOKEN_ENDLINE:
            builder->state = SVASM_BSTATE_BLANK;
            found = true;
            break;
        case SVASM_TOKEN_COMMENT:
            builder->state = SVASM_BSTATE_COMMENT;
            found = true;
            break;
        case SVASM_TOKEN_SYMBOL:
            if(builder->expectEOL)
            {
                SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                            builder->tokens->tokens[builder->index]->line, "unexpected token");
                SVAsm_errorBuilder(builder->options, builder->tokens, builder->index);
                builder->error = true;
                break;
            }
            found = true;
            if(SVAsm_instruction_getKeyword(builder->tokens->tokens[builder->index]->value.text) != SVASM_KEYWORD_NONE)
                builder->state = SVASM_BSTATE_INSTRUCTION;
            else
            {
                if(builder->index < builder->tokens->tokenCount - 1)
                {
                    if(builder->tokens->tokens[builder->index + 1]->type == SVASM_TOKEN_SEPARATOR)
                    {
                        if(builder->tokens->tokens[builder->index + 1]->value.separator == SVASM_SEPARATOR_COLON)
                            builder->state = SVASM_BSTATE_LABEL;
                        else if(builder->tokens->tokens[builder->index + 1]->value.separator == SVASM_SEPARATOR_LPARENTHESIS)
                            builder->state = SVASM_BSTATE_MACRO;
                        else
                        {
                            SVASM_BUILDER_UNEXPECTEDTOKEN();
                            builder->error = true;
                            found = false;
                        }
                    }
                    else
                    {
                        SVASM_BUILDER_UNEXPECTEDTOKEN();
                        builder->error = true;
                        found = false;
                    }
                }
                else
                {
                    SVASM_BUILDER_UNEXPECTEDTOKEN();
                    builder->error = true;
                    found = false;
                }
            }
            break;
        case SVASM_TOKEN_SEPARATOR:
            if(builder->expectEOL)
            {
                SVASM_BUILDER_UNEXPECTEDTOKEN();
                builder->error = true;
                break;
            }
            if(builder->tokens->tokens[builder->index]->value.separator == SVASM_SEPARATOR_DOT)
            {
                builder->state = SVASM_BSTATE_COMMAND;
                found = true;
            }
            break;
        default:
            builder->state = SVASM_BSTATE_BLANK;
            break;
    }
    if(!found && !builder->error)
    {
        SVASM_BUILDER_UNEXPECTEDTOKEN();
        builder->error = true;
    }
    builder->stateEnd = false;
}


bool SVAsm_builder_getComment(SVAsm_Builder *builder)
{
    if(!builder)
        return false;
    char *comment = builder->tokens->tokens[builder->index]->value.text;
    if(builder->index == 0 || builder->tokens->tokens[builder->index - 1]->type == SVASM_TOKEN_ENDLINE)
    {
        SVAsm_Block *block = SVAsm_block_create(SVASM_BLOCK_NONE,
            NULL, builder->tokens->tokens[builder->index]->line,
            builder->filename, builder->filepath, builder->level);
        SVAsm_block_setComment(block, comment);
        SVAsm_blockList_add(builder->blocks, block);
    }
    else
        SVAsm_block_setComment(builder->blocks->blocks[builder->blocks->blockCount - 1], comment);
    builder->index++;
    return true;
}

bool SVAsm_builder_getLabel(SVAsm_Builder *builder)
{
    if(!builder)
        return false;
    if(builder->index >= builder->tokens->tokenCount - 1)
        return false;
    if(builder->tokens->tokens[builder->index + 1]->type != SVASM_TOKEN_SEPARATOR)
        return false;
    if(builder->tokens->tokens[builder->index + 1]->value.separator != SVASM_SEPARATOR_COLON)
        return false;
    SVAsm_Block *block = SVAsm_block_create(SVASM_BLOCK_LABEL,
            builder->tokens->tokens[builder->index]->value.text,
            builder->tokens->tokens[builder->index]->line,
            builder->filename, builder->filepath, builder->level);
    SVAsm_blockList_add(builder->blocks, block);
    builder->index += 2;
    return true;
}

bool SVAsm_builder_getInstruction(SVAsm_Builder *builder)
{
    if(!builder)
        return false;
    SVAsm_Instruction *instruction = SVAsm_instruction_create(SVAsm_instruction_getKeyword(builder->tokens->tokens[builder->index]->value.text));
    bool expectSomething = false;
    bool expectComma = false;
    size_t index = 1;
    size_t argStartIndex = index;
    char *comment = NULL;
    while(true)
    {
        if(builder->index + index >= builder->tokens->tokenCount)
            break;
        if(builder->tokens->tokens[builder->index + index]->type == SVASM_TOKEN_ENDLINE)
            break;
        if(builder->tokens->tokens[builder->index + index]->type == SVASM_TOKEN_COMMENT)
        {
            comment = builder->tokens->tokens[builder->index]->value.text;
            builder->index++;
            continue;
        }
        argStartIndex = index;
        SVAsm_Arg *arg = SVAsm_builder_getArg(builder, &index);
        if(!arg)
        {
            SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                        builder->tokens->tokens[builder->index + index]->line, "invalid syntax");
            SVAsm_errorBuilder(builder->options, builder->tokens, builder->index + argStartIndex);
            builder->error = true;
            break;
        }
        SVAsm_Operand *operand = SVAsm_operand_fromArg(arg);
        if(!operand)
        {
            SVAsm_error(builder->options, SVASM_ERROR, builder->filename, builder->filepath,
                        builder->tokens->tokens[builder->index + index]->line, "invalid operand type");
            SVAsm_errorBuilder(builder->options, builder->tokens, builder->index + argStartIndex);
            builder->error = true;
            break;
        }
        SVAsm_instruction_addOperand(instruction, operand);
        expectComma = true;
        expectSomething = false;
        if(builder->index + index >= builder->tokens->tokenCount)
        {
            if(expectSomething)
                SVASM_BUILDER_IMPROPERSYNTAX();
            break;
        }
        if((builder->tokens->tokens[builder->index + index]->type == SVASM_TOKEN_ENDLINE ||
                builder->tokens->tokens[builder->index + index]->type == SVASM_TOKEN_COMMENT) &&
                expectSomething)
            SVASM_BUILDER_IMPROPERSYNTAX();
        if(builder->tokens->tokens[builder->index + index]->type == SVASM_TOKEN_SEPARATOR &&
                builder->tokens->tokens[builder->index + index]->value.separator == SVASM_SEPARATOR_COMMA)
        {
            if(!expectComma)
                SVASM_BUILDER_IMPROPERSYNTAX();
            expectSomething = true;
            expectComma = false;
            if(builder->index + index + 1 >= builder->tokens->tokenCount)
                break;
            index++;
        }
        else
            break;
    }
    SVAsm_Block *block = SVAsm_block_create(SVASM_BLOCK_INSTRUCTION,
            instruction,
            builder->tokens->tokens[builder->index]->line,
            builder->filename, builder->filepath, builder->level);
    if(comment)
        SVAsm_block_setComment(block, comment);
    SVAsm_blockList_add(builder->blocks, block);
    builder->index += index;
    return true;
}

bool SVAsm_builder_getCommand(SVAsm_Builder *builder)
{
    if(!builder)
        return false;
    if(builder->index + 1 >= builder->tokens->tokenCount)
        return false;
    SVAsm_Command command = SVAsm_command_getCommand(builder->tokens->tokens[builder->index + 1]->value.text);
    if(command == SVASM_COMMAND_NONE)
        return false;
    SVAsm_CommandLine *line = SVAsm_commandLine_create(command);
    size_t index = 2;
    bool done = false;
    while(!done)
    {
        SVAsm_Arg *arg = SVAsm_builder_getArg(builder, &index);
        if(arg)
            SVAsm_commandLine_add(line, arg);
        else
            done = true;
        if(builder->index + index >= builder->tokens->tokenCount)
            done = true;
    }
    SVAsm_Block *block = SVAsm_block_create(SVASM_BLOCK_COMMAND,
            line,
            builder->tokens->tokens[builder->index]->line,
            builder->filename, builder->filepath, builder->level);
    SVAsm_blockList_add(builder->blocks, block);
    builder->index += index;
    return true;
}

bool SVAsm_builder_getMacro(SVAsm_Builder *builder)
{
    if(!builder)
        return false;
    size_t idx = 0;
    SVAsm_MacroCall *macro = SVAsm_builder_getMacroCall(builder, &idx);
    if(!macro)
        return false;
    SVAsm_Block *block = SVAsm_block_create(SVASM_BLOCK_MACRO, macro,
                                            builder->tokens->tokens[builder->index]->line,
                                            builder->filename, builder->filepath, builder->level);
    SVAsm_blockList_add(builder->blocks, block);
    builder->index += idx + 1;
    return true;
}

SVAsm_Arg* SVAsm_builder_getArg(SVAsm_Builder *builder, size_t *index)
{
    if(!builder || !index)
        return NULL;
    SVAsm_Arg *arg = NULL;
    size_t innerIndex = (*index);
    if(builder->index + innerIndex >= builder->tokens->tokenCount)
        return NULL;
    SVAsm_TokenType type = builder->tokens->tokens[builder->index + innerIndex]->type;
    switch(type)
    {
        case SVASM_TOKEN_NUMBER:
            arg = SVAsm_arg_create(SVASM_ARG_NUMBER,
                                   &(builder->tokens->tokens[builder->index + innerIndex]->value.number));
            innerIndex++;
            break;
        case SVASM_TOKEN_SYMBOL:
            if(builder->index + innerIndex + 1 < builder->tokens->tokenCount &&
               builder->tokens->tokens[builder->index + innerIndex + 1]->type == SVASM_TOKEN_SEPARATOR &&
               builder->tokens->tokens[builder->index + innerIndex + 1]->value.separator == SVASM_SEPARATOR_LPARENTHESIS)
            {
                SVAsm_MacroCall *innerCall = SVAsm_builder_getMacroCall(builder, &innerIndex);
                if(!innerCall)
                    break;
                arg = SVAsm_arg_create(SVASM_ARG_MACRO, innerCall);
            }
            else
            {
                arg = SVAsm_arg_create(SVASM_ARG_SYMBOL,
                                       builder->tokens->tokens[builder->index + innerIndex]->value.text);
            }
            innerIndex++;
            break;
        case SVASM_TOKEN_STRING:
            arg = SVAsm_arg_create(SVASM_ARG_STRING,
                                   builder->tokens->tokens[builder->index + innerIndex]->value.text);
            innerIndex++;
            break;
        case SVASM_TOKEN_REGISTER:
            arg = SVAsm_arg_create(SVASM_ARG_REGISTER, &(builder->tokens->tokens[builder->index + innerIndex]->value.reg));
            innerIndex++;
            break;
        case SVASM_TOKEN_SEPARATOR:
            if(builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_COLON &&
               builder->index + innerIndex + 1 < builder->tokens->tokenCount &&
               builder->tokens->tokens[builder->index + innerIndex + 1]->type == SVASM_TOKEN_SYMBOL)
            {
                innerIndex++;
                char *labelName = builder->tokens->tokens[builder->index + innerIndex]->value.text;
                innerIndex++;
                SVAsm_Arg *offset = NULL;
                bool negative = false;
                if(builder->tokens->tokens[builder->index + innerIndex]->type == SVASM_TOKEN_SEPARATOR &&
                        (builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_PLUS ||
                                builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_MINUS) &&
                        builder->index + innerIndex + 1 < builder->tokens->tokenCount)
                {
                    if(builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_MINUS)
                        negative = true;
                    innerIndex++;
                    offset = SVAsm_builder_getArg(builder, &innerIndex);
                }
                SVAsm_LabelCall *label = SVAsm_labelCall_create(labelName, negative, offset);
                arg = SVAsm_arg_create(SVASM_ARG_LABEL, label);
            }
            else if(builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_LBRACKET)
            {
                SVAsm_Array *array = SVAsm_builder_getArray(builder, &innerIndex);
                if(!array)
                    break;
                arg = SVAsm_arg_create(SVASM_ARG_ARRAY, array);
                innerIndex++;
            }
            else if(builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_MINUS &&
                    builder->index + innerIndex < builder->tokens->tokenCount - 1 &&
                    builder->tokens->tokens[builder->index + innerIndex + 1]->type == SVASM_TOKEN_NUMBER)
            {
                SVAsm_NumberType number = -(builder->tokens->tokens[builder->index + innerIndex + 1]->value.number);
                arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
                innerIndex += 2;
            }
            break;
        default:
            break;
    }
    if(arg)
        *index = innerIndex;
    return arg;
}

SVAsm_MacroCall* SVAsm_builder_getMacroCall(SVAsm_Builder *builder, size_t *index)
{
    if(!builder)
        return NULL;
    if(builder->tokens->tokens[builder->index + (*index)]->type != SVASM_TOKEN_SYMBOL ||
            builder->tokens->tokens[builder->index + (*index) + 1]->type != SVASM_TOKEN_SEPARATOR ||
            builder->tokens->tokens[builder->index + (*index) + 1]->value.separator != SVASM_SEPARATOR_LPARENTHESIS)
        return NULL;
    SVAsm_MacroCall *call = SVAsm_macroCall_create(builder->tokens->tokens[builder->index + (*index)]->value.text);
    bool expectSomething = false;
    bool expectComma = false;
    size_t innerIndex = (*index) + 2;
    bool done = false;
    while(!done)
    {
        SVAsm_Arg *arg = SVAsm_builder_getArg(builder, &innerIndex);
        if(arg)
        {
            SVAsm_macroCall_add(call, arg);
            expectSomething = false;
            expectComma = true;
        }
        if(builder->index + innerIndex >= builder->tokens->tokenCount)
            break;
        if(builder->tokens->tokens[builder->index + innerIndex]->type == SVASM_TOKEN_SEPARATOR &&
           builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_COMMA)
        {
            if(builder->index + innerIndex + 1 >= builder->tokens->tokenCount)
                break;
            if(!expectComma)
                SVASM_BUILDER_IMPROPERSYNTAX();
            expectSomething = true;
            expectComma = false;
            innerIndex++;
        }
        else if(builder->tokens->tokens[builder->index + innerIndex]->type == SVASM_TOKEN_SEPARATOR &&
                builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_RPARENTHESIS)
        {
            if(expectSomething)
                SVASM_BUILDER_IMPROPERSYNTAX();
            innerIndex++;
            done = true;
        }
        else
        {
            SVAsm_error(builder->options, SVASM_WARNING, builder->filename, builder->filepath,
                        builder->tokens->tokens[builder->index]->line, "improper syntax");
            SVAsm_errorBuilder(builder->options, builder->tokens, builder->index + innerIndex);
            done = true;
        }
    }
    *index = innerIndex - 1;
    return call;
}

SVAsm_Array* SVAsm_builder_getArray(SVAsm_Builder *builder, size_t *index)
{
    if(!builder)
        return NULL;
    if(builder->tokens->tokens[builder->index + (*index)]->type != SVASM_TOKEN_SEPARATOR ||
       builder->tokens->tokens[builder->index + (*index)]->value.separator != SVASM_SEPARATOR_LBRACKET)
        return NULL;
    SVAsm_Array *array = SVAsm_array_create();
    bool expectSomething = false;
    bool expectComma = false;
    size_t innerIndex = (*index) + 1;
    bool done = false;
    while(!done)
    {
        SVAsm_Arg *arg = SVAsm_builder_getArg(builder, &innerIndex);
        if(arg)
        {
            SVAsm_array_insert(array, array->argCount, arg);
            expectSomething = false;
            expectComma = true;
        }
        if(builder->index + innerIndex >= builder->tokens->tokenCount)
            break;
        if(builder->tokens->tokens[builder->index + innerIndex]->type == SVASM_TOKEN_SEPARATOR &&
           builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_COMMA)
        {
            if(builder->index + innerIndex + 1 >= builder->tokens->tokenCount)
                break;
            if(!expectComma)
                SVASM_BUILDER_IMPROPERSYNTAX();
            expectSomething = true;
            expectComma = false;
            innerIndex++;
        }
        else if(builder->tokens->tokens[builder->index + innerIndex]->type == SVASM_TOKEN_SEPARATOR &&
                builder->tokens->tokens[builder->index + innerIndex]->value.separator == SVASM_SEPARATOR_RBRACKET)
        {
            if(expectSomething)
                SVASM_BUILDER_IMPROPERSYNTAX();
            innerIndex++;
            done = true;
        }
        else
        {
            SVAsm_error(builder->options, SVASM_WARNING, builder->filename, builder->filepath,
                        builder->tokens->tokens[builder->index]->line, "improper syntax");
            SVAsm_errorBuilder(builder->options, builder->tokens, builder->index + innerIndex);
            done = true;
        }
    }
    *index = innerIndex - 1;
    return array;
}
