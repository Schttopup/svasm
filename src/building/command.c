/**
 * \file command.c
 * \brief Defines functions related to commands.
 * \author Schttopup
 */

#include "command.h"
#include "macrocall.h"
#include <sdefs.h>
#include <string.h>


char const * const SVAsm_commandValues[SVASM_COMMAND_LAST] = {
        "",
        "origin",
        "export",
        "meta",
        "include",
        "binary",
        "load",
        "bytes",
        "space",
        "stack",
        "define",
        "set",
        "assign",
        "delete",
        "local",
        "unlocal",
        "call",
        "if",
        "else",
        "for",
        "while",
        "macro",
        "block",
        "end",
        "return",
        "message",
        "warning",
        "error"
};


SVAsm_CommandLine* SVAsm_commandLine_create(SVAsm_Command command)
{
    if(command == SVASM_COMMAND_NONE)
        return NULL;
    SVAsm_CommandLine *line = NULL;
    MMALLOC(line, SVAsm_CommandLine, 1);
    line->command = command;
    line->argCount = 0;
    line->args = NULL;
    return line;
}

void SVAsm_commandLine_destroy(SVAsm_CommandLine *line)
{
    if(!line)
        return;
    for(size_t i = 0; i < line->argCount; i++)
        SVAsm_arg_destroy(line->args[i]);
    FFREE(line->args);
    FFREE(line);
}

void SVAsm_commandLine_add(SVAsm_CommandLine *line, SVAsm_Arg *arg)
{
    if(!line || !arg)
        return;
    RREALLOC(line->args, SVAsm_Arg*, line->argCount + 1);
    line->args[line->argCount] = arg;
    line->argCount++;
}

SVAsm_Command SVAsm_command_getCommand(char const *text)
{
    for(size_t i = 1; i < SVASM_COMMAND_LAST; i++)
    {
        if(strcmp(text, SVAsm_commandValues[i]) == 0)
            return (SVAsm_Command)i;
    }
    return SVASM_COMMAND_NONE;
}


bool SVAsm_command_isArgExpandable(SVAsm_Command command, size_t index)
{
    switch(command)
    {
        case SVASM_COMMAND_SET:
        case SVASM_COMMAND_DELETE:
        case SVASM_COMMAND_LOCAL:
        case SVASM_COMMAND_UNLOCAL:
        case SVASM_COMMAND_MACRO:
        case SVASM_COMMAND_BLOCK:
            if(index == 0)
                return false;
            break;
        case SVASM_COMMAND_LOAD:
        case SVASM_COMMAND_FOR:
            if(index == 1)
                return false;
            break;
        case SVASM_COMMAND_DEFINE:
            if(index < 2)
                return false;
            break;
        default:
            break;
    }
    return true;
}
