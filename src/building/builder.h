/**
 * \file builder.h
 * \brief Declares functions and structures related to building.
 * \author Schttopup
 */

#ifndef SVASM_BUILDER_H
#define SVASM_BUILDER_H

#include "block.h"
#include "../parsing/token.h"
#include "../option.h"
#include <sdefs.h>
#include <stdbool.h>


/**
 * \brief Builder states.
 */
enum SVAsm_BuilderState_
{
    SVASM_BSTATE_BLANK,
    SVASM_BSTATE_COMMENT,
    SVASM_BSTATE_LABEL,
    SVASM_BSTATE_INSTRUCTION,
    SVASM_BSTATE_COMMAND,
    SVASM_BSTATE_MACRO
};

/// Type name for builder states.
typedef enum SVAsm_BuilderState_ SVAsm_BuilderState;


/**
 * \brief Builder structure.
 * Contains data for building.
 */
struct SVAsm_Builder_
{
    SVAsm_TokenList *tokens;    //!< Token list
    char const *filename;       //!< File name
    char const *filepath;       //!< File path
    unsigned int level;         //!< Inclusion level
    size_t index;               //!< Token index
    SVAsm_BlockList *blocks;    //!< Block list
    SVAsm_BuilderState state;
    bool stateEnd;
    bool expectEOL;
    bool error;
    SVAsm_Options *options;
};

/// Type name for builder structure.
typedef struct SVAsm_Builder_ SVAsm_Builder;

SVAsm_Builder *SVAsm_builder_create(SVAsm_TokenList *tokens, char const *filename, char const *filepath, unsigned int level, SVAsm_Options *options);

void SVAsm_builder_destroy(SVAsm_Builder *builder);

/**
 * \brief Builds a block list from a token list.
 * \param tokens The token list
 * \return The built block list
 */
void SVAsm_builder_build(SVAsm_Builder *builder);

void SVAsm_builder_buildState(SVAsm_Builder *builder);

void SVAsm_builder_setState(SVAsm_Builder *builder);

/**
 * \brief Builds a comment from a builder
 * \param builder The builder
 * \return true if no error, false otherwise
 */
bool SVAsm_builder_getComment(SVAsm_Builder *builder);

/**
 * \brief Builds a label from a builder
 * \param builder The builder
 * \return true if no error, false otherwise
 */
bool SVAsm_builder_getLabel(SVAsm_Builder *builder);

/**
 * \brief Builds an instruction from a builder
 * \param builder The builder
 * \return true if no error, false otherwise
 */
bool SVAsm_builder_getInstruction(SVAsm_Builder *builder);

/**
 * \brief Builds a command from a builder
 * \param builder The builder
 * \return true if no error, false otherwise
 */
bool SVAsm_builder_getCommand(SVAsm_Builder *builder);

bool SVAsm_builder_getMacro(SVAsm_Builder *builder);

SVAsm_Arg* SVAsm_builder_getArg(SVAsm_Builder *builder, size_t *index);

SVAsm_MacroCall* SVAsm_builder_getMacroCall(SVAsm_Builder *builder, size_t *index);

SVAsm_Array* SVAsm_builder_getArray(SVAsm_Builder *builder, size_t *index);


#endif //SVASM_BUILDER_H
