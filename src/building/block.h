/**
 * \file block.h
 * \brief Declares structures and functions related to building blocks.
 * \author Schttopup
 */

#ifndef SVASM_BLOCK_H
#define SVASM_BLOCK_H

#include "instruction.h"
#include "command.h"


/**
 * \brief Parsing block types.
 */
enum SVAsm_BlockType_
{
    SVASM_BLOCK_NONE,
    SVASM_BLOCK_LABEL,
    SVASM_BLOCK_INSTRUCTION,
    SVASM_BLOCK_COMMAND,
    SVASM_BLOCK_MACRO
};

/// Type name for parsing block types.
typedef enum SVAsm_BlockType_ SVAsm_BlockType;


struct SVAsm_MacroCall_;

/**
 * \brief Parsing block.
 */
struct SVAsm_Block_
{
    SVAsm_BlockType type;               //!< Block type
    union SVAsm_BlockContent_           //!< Block content
    {
        char *text;                     //!< Text content
        SVAsm_Instruction *instruction; //!< Instruction content
        SVAsm_CommandLine *command;     //!< Command content
        struct SVAsm_MacroCall_ *macro;
    } content;
    unsigned int line;                  //!< Line number
    char const *filename;               //!< File name
    char const *filepath;               //!< File path
    unsigned int level;                 //!< Inclusion level
    char *comment;
};

/// Type name for block structure.
typedef struct SVAsm_Block_ SVAsm_Block;


/**
 * \brief Parsing block list.
 */
struct SVAsm_BlockList_
{
    SVAsm_Block **blocks;       //!< Block list
    size_t blockCount;    //!< Block callCount
};

/// Type name for parsing block list.
typedef struct SVAsm_BlockList_ SVAsm_BlockList;


/**
 * \brief Creates a block.
 * \param type The block type
 * \param value The block value
 * \param line The line number
 * \return The created block
 */
SVAsm_Block* SVAsm_block_create(SVAsm_BlockType type, void *value, unsigned int line, char const *filename, char const *filepath, unsigned int level);

/**
 * \brief Destroys a block.
 * \param block The block to destroy
 */
void SVAsm_block_destroy(SVAsm_Block *block);

void SVAsm_block_setComment(SVAsm_Block *block, char const *comment);

/**
 * \brief Creates a block list.
 * \return The created bloxk list
 */
SVAsm_BlockList* SVAsm_blockList_create(void);

/**
 * \brief Destroys a block list
 * \param list The block list to destroy
 */
void SVAsm_blockList_destroy(SVAsm_BlockList *list);

/**
 * Adds a block to a block list
 * \param list The block list
 * \param block The block to add
 */
void SVAsm_blockList_add(SVAsm_BlockList *list, SVAsm_Block *block);

/**
 * Gets a block from a block list
 * \param list The block list
 * \param index The index of the block
 * \return The requested block, or NULL if invalid index
 */
SVAsm_Block* SVAsm_blockList_get(SVAsm_BlockList *list, size_t index);

void SVAsm_blockList_insert(SVAsm_BlockList *list, SVAsm_BlockList *toAdd, size_t index);


#endif //SVASM_BLOCK_H
