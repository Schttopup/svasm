#include "app.h"
#include "error.h"


int main(int argc, char *argv[])
{
    int returnCode = EXIT_SUCCESS;

    SArg_OptionList *cmdOptions = SVAsm_app_prepareCommandlineOptions();
    SArg_ParameterList *cmdParams = SArg_parse(cmdOptions, argc - 1, argv + 1);
    SArg_parameterList_addDefaults(cmdParams, cmdOptions);
    if(!cmdParams)
    {
        SArg_optionList_printHelp(cmdOptions);
        SArg_optionList_destroy(cmdOptions);
        return EXIT_FAILURE;
    }
    SVAsm_Options *options = SVAsm_app_makeOptions(cmdParams, cmdOptions);
    if(!options)
    {
        SArg_optionList_printHelp(cmdOptions);
        SArg_parameterList_destroy(cmdParams);
        SArg_optionList_destroy(cmdOptions);
        return EXIT_FAILURE;
    }

    bool assembled = SVAsm_app_assemble(options);
    if(!assembled)
    {
        SVAsm_message(options, SVASM_INFO, "Assembly finished with errors, no output produced");
        returnCode = EXIT_FAILURE;
    }
    else
    {
        SVAsm_message(options, SVASM_INFO, "Assembly successful, file saved to %s", options->outputFileName);
    }

    SVAsm_options_destroy(options);
    SArg_parameterList_destroy(cmdParams);
    SArg_optionList_destroy(cmdOptions);

    return returnCode;
}
