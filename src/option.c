/**
 * \file option.c
 * \brief Defines functions related to program options.
 * \author Schttopup
 */

#include "option.h"
#include <sdefs.h>


char const * const SVAsm_options_warningNames[SVASM_OPTIONS_WARNING_COUNT] = {
        "error",
        "fatal",
        "math",
        "math-error",
        "unset-labels",
        "default-values-error",
        "shadow-variables",
        "undefined-delete",
        "register-overflow"
};

int const SVAsm_options_warningOffsets[SVASM_OPTIONS_WARNING_COUNT] = {
        offsetof(SVAsm_Options, warningError),
        offsetof(SVAsm_Options, fatalError),
        offsetof(SVAsm_Options, warnings.mathWarning),
        offsetof(SVAsm_Options, warnings.mathError),
        offsetof(SVAsm_Options, warnings.unsetLabels),
        offsetof(SVAsm_Options, warnings.defaultValuesError),
        offsetof(SVAsm_Options, warnings.shadowVariables),
        offsetof(SVAsm_Options, warnings.undefinedDelete),
        offsetof(SVAsm_Options, warnings.registerOverflowError)
};


SVAsm_Options* SVAsm_options_create(void)
{
    SVAsm_Options *options = NULL;
    MMALLOC(options, SVAsm_Options, 1);
    options->varDataStart = 0x8000;
    options->inputFileName = NULL;
    options->outputFileName = NULL;
    options->outputListing = false;
    options->outputColors = true;
    options->fullPaths = false;
    options->shortErrors = false;
    options->definedSymbols = NULL;
    options->definedSymbolCount = 0;

    options->warningError = false;
    options->fatalError = false;
    options->warnings.mathWarning = true;
    options->warnings.mathError = false;
    options->warnings.unsetLabels = true;
    options->warnings.defaultValuesError = false;
    options->warnings.shadowVariables = true;
    options->warnings.undefinedDelete = true;
    options->warnings.registerOverflowError = true;

    void *defaultAux = SVAsm_source_defCreate();
    SVAsm_source_defAdd(defaultAux, ".");
    options->source = SVAsm_source_create(&(SVAsm_source_defCallback), defaultAux);
    options->maxIncludeLevels = 16;
    options->maxMacroDepth = 256;
    options->maxTranslatorDepth = 16;
    return options;
}

void SVAsm_options_destroy(SVAsm_Options *options)
{
    if(!options)
        return;
    FFREE(options->inputFileName);
    FFREE(options->outputFileName);
    for(size_t i = 0; i < options->definedSymbolCount; i++)
        FFREE(options->definedSymbols[i]);
    FFREE(options->definedSymbols);
    SVAsm_source_defDestroy(options->source->aux);
    FFREE(options->source);
    FFREE(options);
}
