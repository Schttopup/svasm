/**
 * \file source.c
 * \brief Defines functions related to data sources.
 * \author Schttopup
 */

#include "source.h"
#include <sfile/sfile.h>
#include <sdefs.h>
#include <string.h>


SVAsm_Source *SVAsm_source_create(SVAsm_SourceCallback callback, void *aux)
{
    SVAsm_Source *source = NULL;
    MMALLOC(source, SVAsm_Source, 1);
    source->callback = callback;
    source->aux = aux;
    return source;
}

bool SVAsm_source_load(SVAsm_Source *source, char const *name, uint8_t **data, size_t *size, char **filename, char **filepath)
{
    if(!source)
        return false;
    if(!source->callback)
        return false;
    return (*source->callback)(name, source->aux, data, size, filename, filepath);
}


struct SVAsm_SourceDefaultAux_
{
    char **paths;
    size_t pathCount;
    char **filenames;
    size_t filenameCount;
};

typedef struct SVAsm_SourceDefaultAux_ SVAsm_SourceDefaultAux;


void* SVAsm_source_defCreate(void)
{
    SVAsm_SourceDefaultAux *aux = NULL;
    MMALLOC(aux, SVAsm_SourceDefaultAux, 1);
    aux->pathCount = 0;
    aux->paths = NULL;
    aux->filenameCount = 0;
    aux->filenames = NULL;
    return aux;
}

void SVAsm_source_defDestroy(void *aux)
{
    if(!aux)
        return;
    SVAsm_SourceDefaultAux *privateAux = aux;
    for(size_t i = 0; i < privateAux->pathCount; i++)
        FFREE(privateAux->paths[i]);
    FFREE(privateAux->paths);
    for(size_t i = 0; i < privateAux->filenameCount; i++)
        FFREE(privateAux->filenames[i]);
    FFREE(privateAux->filenames);
    FFREE(privateAux);
}

void SVAsm_source_defAdd(void *aux, char const *path)
{
    if(!aux || !path)
        return;
    SVAsm_SourceDefaultAux *privateAux = aux;
    RREALLOC(privateAux->paths, char*, privateAux->pathCount + 1);
    char *temp = SFile_path_absolute(path);
    privateAux->paths[privateAux->pathCount] = temp;
    privateAux->pathCount++;
}

bool SVAsm_source_defCallback(char const *name, void *aux, uint8_t **data, size_t *size, char **filename, char **filepath)
{
    if(!name || !aux || !data || !size ||!filename ||!filepath)
        return false;
    *data = NULL;
    *size = 0;
    SVAsm_SourceDefaultAux *defAux = (SVAsm_SourceDefaultAux*)aux;
    for(size_t i = 0; i < defAux->pathCount; i++)
    {
        char *fullPath = NULL;
        fullPath = SFile_path_concat(defAux->paths[i], name);

        if(SFile_loadRaw(fullPath, data, size))
        {
            FFREE(fullPath);
            *filename = NULL;
            for(size_t j = 0; j < defAux->filenameCount; j++)
            {
                if(strcmp(defAux->filenames[j], name) == 0)
                {
                    *filename = defAux->filenames[j];
                    break;
                }
            }
            if(!(*filename))
            {
                RREALLOC(defAux->filenames, char*, defAux->filenameCount + 1);
                defAux->filenames[defAux->filenameCount] = NULL;
                MMALLOC(defAux->filenames[defAux->filenameCount], char, strlen(name) + 1);
                strcpy(defAux->filenames[defAux->filenameCount], name);
                *filename = defAux->filenames[defAux->filenameCount];
                defAux->filenameCount++;
            }
            *filepath = defAux->paths[i];
            return true;
        }
        FFREE(fullPath);
    }
    return false;
}
