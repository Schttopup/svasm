/**
 * \file app.h
 * \brief Declares structures and functions related to the svasm application.
 * \author Schttopup
 */

#ifndef SVASM_APP_H
#define SVASM_APP_H

#include "option.h"
#include "translation/translator.h"
#include <sarg/sarg.h>

/**
 * Builds an option list for the svasm program.
 * \return The constructed option list
 */
SArg_OptionList* SVAsm_app_prepareCommandlineOptions(void);

/**
 * Fills the options structure with the svasm program parameters.
 * \param parameters The specified command line parameters
 * \param options The option list
 * \return The filled options
 */
SVAsm_Options* SVAsm_app_makeOptions(SArg_ParameterList *parameters, SArg_OptionList *options);

/**
 * Creates macros for each symbol defined via the command line parameters.
 * \param translator The main translator
 * \param options The program options
 */
void SVAsm_app_defineSymbols(SVAsm_Translator *translator, SVAsm_Options *options);

/**
 * Sets or clear warning flags.
 * \param options The program options
 * \param warningFlag The flag to set or clear (cleared flags are prefixed by "no-")
 * \return true if the flag exists, false otherwise
 */
bool SVAsm_app_setWarning(SVAsm_Options *options, char const *warningFlag);

/**
 * Processes the assembly source file and creates the binary output file.
 * \param options The program options
 * \return true if the assembly succeeded, false otherwise
 */
bool SVAsm_app_assemble(SVAsm_Options *options);

/**
 * Saves the output of the assembly in a SV binary file.
 * \param options The program options
 * \param translator The main translator
 * \return true if saving succeeded, false otherwise
 */
bool SVAsm_app_saveBinary(SVAsm_Options *options, SVAsm_Translator *translator);

#endif //SVASM_APP_H
