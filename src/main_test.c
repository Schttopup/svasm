#include "app.h"
#include "error.h"


int main(int argc, char *argv[])
{
    SVAsm_Options *options = SVAsm_options_create();
    bool assembled = SVAsm_app_assemble(options);
    return EXIT_SUCCESS;
}
