/**
 * \file error.c
 * \brief Defines functions related to error management.
 * \author Schttopup
 */

#include "error.h"
#include "translation/translator.h"
#include <sfile/sfile.h>
#include <sdefs.h>
#include <stdio.h>
#include <string.h>


void SVAsm_message(SVAsm_Options *options, SVAsm_ErrorType type, char const *format, ...)
{
    if(!options || !format)
        return;
    char const *color = NULL;
    char const *errorText = NULL;
    if(options->warningError && type == SVASM_WARNING)
        type = SVASM_ERROR;
    if(options->fatalError && type == SVASM_ERROR)
        type = SVASM_FATAL;
    switch(type)
    {
        case SVASM_FATAL:
            color = "91";
            errorText = "fatal";
            break;
        case SVASM_ERROR:
            color = "91";
            errorText = "error";
            break;
        case SVASM_WARNING:
            color = "93";
            errorText = "warning";
            break;
        case SVASM_INFO:
            color = "94";
            errorText = "info";
            break;
        case SVASM_BLANK:
            color = "0";
            errorText = "";
            break;
    }
    if(options->outputColors)
        printf("\x1B[0;%sm%-7s\033[0m ", color, errorText);
    else
        printf("%-7s ", errorText);
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
    printf("\n");
}

void SVAsm_error(SVAsm_Options *options, SVAsm_ErrorType type, char const *filename, char const *filepath, unsigned int line, char const *format, ...)
{
    if(!options || !format)
        return;
    char const *color = NULL;
    char const *errorText = NULL;
    if(options->warningError && type == SVASM_WARNING)
        type = SVASM_ERROR;
    if(options->fatalError && type == SVASM_ERROR)
        type = SVASM_FATAL;
    switch(type)
    {
        case SVASM_FATAL:
            color = "91";
            errorText = "fatal";
            break;
        case SVASM_ERROR:
            color = "91";
            errorText = "error";
            break;
        case SVASM_WARNING:
            color = "93";
            errorText = "warning";
            break;
        case SVASM_INFO:
            color = "94";
            errorText = "info";
            break;
        case SVASM_BLANK:
            color = "0";
            errorText = "";
            break;
    }
    char const *fullPath = filename;
    if(filepath && options->fullPaths)
        fullPath = SFile_path_concat(filepath, filename);
    if(options->outputColors)
        printf("\x1B[0;%sm%-7s\033[0m %s - %d : ", color, errorText, fullPath, line + 1);
    else
        printf("%-7s %s - %d : ", errorText, fullPath, line + 1);
    if(filepath && options->fullPaths)
        FFREE((void*)fullPath);
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
    printf("\n");
}


void SVAsm_errorParser(SVAsm_Options *options, char const *source, size_t column)
{
    if(!options || !source)
        return;
    char const *stringStart = source;
    for(; *stringStart == ' ' || *stringStart == '\t' || *stringStart == '\n'; stringStart++);
    printf("\t%s\n\t", stringStart);
    ssize_t whitespace = (column - 1) - (stringStart - source);
    if(whitespace < 0)
        whitespace = 0;
    for(ssize_t i = 0; i < whitespace; i++)
        printf(" ");
    printf("^\n");
}

void SVAsm_errorBuilder(SVAsm_Options *options, SVAsm_TokenList *tokens, size_t index)
{
    if(!options || !tokens)
        return;
    printf("\t");
    size_t start = 0;
    size_t end = 0;
    for(size_t i = index; i > 0; i--)
    {
        start = i;
        if(tokens->tokens[i]->line != tokens->tokens[index]->line)
        {
            start++;
            break;
        }
    }
    for(size_t i = index; i < tokens->tokenCount; i++)
    {
        end = i;
        if(tokens->tokens[i]->line != tokens->tokens[index]->line || tokens->tokens[i]->type == SVASM_TOKEN_ENDLINE)
        {
            end--;
            break;
        }
    }
    size_t before = 0;
    size_t after = 0;
    for(size_t i = start; i <= end; i++)
    {
        if(i > start)
            printf(" ");
        unsigned int count = 0;
        switch(tokens->tokens[i]->type)
        {
            case SVASM_TOKEN_STRING:
                printf("\"%s\"%n", tokens->tokens[i]->value.text, &count);
                break;
            case SVASM_TOKEN_SYMBOL:
                printf("%s%n", tokens->tokens[i]->value.text, &count);
                break;
            case SVASM_TOKEN_SEPARATOR:
                printf("%c%n", SVAsm_separatorValues[tokens->tokens[i]->value.separator], &count);
                break;
            case SVASM_TOKEN_NUMBER:
                printf("%lld%n", tokens->tokens[i]->value.number, &count);
                break;
            case SVASM_TOKEN_REGISTER:
                printf("%%%s%n", SVAsm_registerValues[tokens->tokens[i]->value.reg], &count);
                break;
            default:
                break;
        }
        if(i < index)
            before += count + 1;
        else if(i == index)
            after = count;
    }
    printf("\n\t");
    for(size_t i = 0; i < before; i++)
        printf(" ");
    printf("^");
    if(after > 0)
    {
        for(size_t i = 0; i < after - 1; i++)
            printf("-");
    }
    printf("\n");
}


static size_t printArg(SVAsm_Arg *arg, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end);
static size_t printArray(SVAsm_Array *array, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end);
static size_t printMacroCall(SVAsm_MacroCall *macro, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end);

static size_t printOperand(SVAsm_Operand *operand, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end)
{
    if(!operand || !currentIndex || !start || !end)
        return 0;
    size_t count = 0;
    int sCount = 0;
    bool isFullLength = false;
    if(*currentIndex == index)
    {
        isFullLength = true;
        *start = currentCount + count;
    }
    (*currentIndex)++;
    switch(operand->type)
    {
        case SVASM_OPERAND_NUMBER:
            printf("%lld%n", operand->value.number, &sCount);
            break;
        case SVASM_OPERAND_SYMBOL:
            printf("%s%n", operand->value.text, &sCount);
            break;
        case SVASM_OPERAND_LABEL:
            if(operand->value.label->offset)
                printf(":%s%c%d%n", operand->value.label->label, operand->value.label->negative ? '-' : '+',
                       (int)operand->value.label->offset->value.number, &sCount);
            else
                printf(":%s%n", operand->value.label->label, &sCount);
            sCount += printArg(operand->value.label->offset, index, currentCount, currentIndex, start, end);
            break;
        case SVASM_OPERAND_MACRO:
            sCount = printMacroCall(operand->value.macro, index, count, currentIndex, start, end);
            (*currentIndex)--;
            break;
        case SVASM_OPERAND_REGISTER:
            printf("%%%s%n", SVAsm_registerValues[operand->value.reg], &sCount);
            break;
        default:
            break;
    }
    count += sCount;
    if(isFullLength)
        *end = currentCount + count;
    return count;
}

static size_t printArg(SVAsm_Arg *arg, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end)
{
    if(!arg || !currentIndex || !start || !end)
        return 0;
    size_t count = 0;
    int sCount = 0;
    bool isFullLength = false;
    if(*currentIndex == index)
    {
        isFullLength = true;
        *start = currentCount + count;
    }
    (*currentIndex)++;
    switch(arg->type)
    {
        case SVASM_ARG_NONE:
            printf("nil%n", &sCount);
            break;
        case SVASM_ARG_NUMBER:
            printf("%lld%n", arg->value.number, &sCount);
            break;
        case SVASM_ARG_STRING:
            printf("\"%s\"%n", arg->value.text, &sCount);
            break;
        case SVASM_ARG_SYMBOL:
            printf("%s%n", arg->value.text, &sCount);
            break;
        case SVASM_ARG_LABEL:
            if(arg->value.label->offset == NULL)
                printf(":%s%n", arg->value.label->label, &sCount);
            else
            {
                printf(":%s%c%n", arg->value.label->label, arg->value.label->negative ? '-' : '+', &sCount);
                sCount += printArg(arg->value.label->offset, index, currentCount, currentIndex, start, end);
            }
            break;
        case SVASM_ARG_REGISTER:
            printf("%%%s%n", SVAsm_registerValues[arg->value.reg], &sCount);
            break;
        case SVASM_ARG_MACRO:
            sCount = printMacroCall(arg->value.macro, index, currentCount, currentIndex, start, end);
            (*currentIndex)--;
            break;
        case SVASM_ARG_ARRAY:
            sCount = printArray(arg->value.array, index, currentCount, currentIndex, start, end);
            (*currentIndex)--;
            break;
        default:
            break;
    }
    count += sCount;
    if(isFullLength)
        *end = currentCount + count;
    return count;
}

static size_t printMacroCall(SVAsm_MacroCall *macro, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end)
{
    if(!macro || !currentIndex || !start || !end)
        return 0;
    size_t count = 0;
    int sCount = 0;
    bool isFullLength = false;
    if(*currentIndex == index)
    {
        isFullLength = true;
        *start = currentCount + count;
    }
    (*currentIndex)++;
    printf("%s%n", macro->name, &sCount);
    if(*currentIndex == index)
    {
        *start = currentCount + count;
        *end = currentCount + count + sCount;
    }
    (*currentIndex)++;
    printf("(");
    count += sCount + 1;
    for(size_t i = 0; i < macro->argCount; i++)
    {
        sCount = printArg(macro->args[i], index, count, currentIndex, start, end);
        count += sCount;
        if(i < macro->argCount - 1)
        {
            printf(", ");
            count += 2;
        }
    }
    printf(")");
    count++;
    if(isFullLength)
        *end = currentCount + count;
    return count;
}

static size_t printArray(SVAsm_Array *array, size_t index, size_t currentCount, size_t *currentIndex, size_t *start, size_t *end)
{
    if(!array || !currentIndex || !start || !end)
        return 0;
    size_t count = 0;
    int sCount = 0;
    bool isFullLength = false;
    if(*currentIndex == index)
    {
        isFullLength = true;
        *start = currentCount + count;
    }
    (*currentIndex)++;
    printf("[");
    count += sCount + 1;
    for(size_t i = 0; i < array->argCount; i++)
    {
        sCount = printArg(array->args[i], index, count, currentIndex, start, end);
        count += sCount;
        if(i < array->argCount - 1)
        {
            printf(", ");
            count += 2;
        }
    }
    printf("]");
    count++;
    if(isFullLength)
        *end = currentCount + count;
    return count;
}

void SVAsm_errorTranslator(SVAsm_Options *options, SVAsm_Translator *translator, SVAsm_Block *block, size_t index)
{
    if(!options || !translator || !block)
        return;

    SVAsm_Block *localBlock = NULL;
    size_t localIndex = 0;
    bool firstItem = true;
    for(ssize_t iStack = translator->stack->itemCount - 1; iStack >= 0; iStack--)
    {
        size_t start = 0;
        size_t end = 0;
        size_t currentIndex = 0;
        size_t count = 0;
        int sCount = 0;
        bool isFullLength = false;
        if(firstItem)
        {
            localBlock = block;
            localIndex = index;
            firstItem = false;
        }
        else
        {
            if(translator->stack->items[iStack + 1]->type == SVASM_STACK_MACRO)
            {
                localBlock = SVAsm_blockList_get(translator->blocks, SVAsm_stackItem_getLocation(translator->stack->items[iStack]));
                localIndex = 0;
                SVAsm_error(options, SVASM_BLANK, localBlock->filename, localBlock->filepath, localBlock->line, "called by");
            }
            else
                continue;
        }
        printf("\t");
        switch(localBlock->type)
        {
            case SVASM_BLOCK_LABEL:
                printf("%s%n :", localBlock->content.text, &sCount);
                start = count;
                end = count + sCount;
                count += sCount;
                break;
            case SVASM_BLOCK_INSTRUCTION:
                {
                    if(currentIndex == localIndex)
                    {
                        isFullLength = true;
                        start = count;
                    }
                    currentIndex++;
                    printf("%s%n", SVAsm_keywordValues[localBlock->content.instruction->keyword], &sCount);
                    if(currentIndex == localIndex)
                    {
                        start = count;
                        end = count + sCount;
                    }
                    count += sCount;
                    currentIndex++;
                    for(size_t i = 0; i < localBlock->content.instruction->operandCount; i++)
                    {
                        if(i > 0)
                        {
                            printf(",");
                            count++;
                        }
                        printf(" ");
                        count++;
                        sCount = printOperand(localBlock->content.instruction->operands[i], localIndex, count, &currentIndex, &start, &end);
                        count += sCount;
                    }
                    if(isFullLength)
                        end = count;
                }
                break;
            case SVASM_BLOCK_COMMAND:
                {
                    if(currentIndex == localIndex)
                    {
                        isFullLength = true;
                        start = count;
                    }
                    currentIndex++;
                    printf(".%s%n", SVAsm_commandValues[localBlock->content.command->command], &sCount);
                    if(currentIndex == localIndex)
                    {
                        start = count;
                        end = count + sCount;
                    }
                    count += sCount;
                    currentIndex++;
                    for(size_t i = 0; i < localBlock->content.command->argCount; i++)
                    {
                        printf(" ");
                        count++;
                        sCount = printArg(localBlock->content.command->args[i], localIndex, count, &currentIndex, &start, &end);
                        count += sCount;
                    }
                    if(isFullLength)
                        end = count;
                }
                break;
            case SVASM_BLOCK_MACRO:
                sCount = printMacroCall(localBlock->content.macro, localIndex, count, &currentIndex, &start, &end);
                count += sCount;
                break;
            default:
                break;
        }
        printf("\n\t");
        for(size_t i = 0; i < start; i++)
            printf(" ");
        printf("^");
        if(end > 0)
        {
            for(size_t i = 0; i < end - start - 1; i++)
                printf("-");
        }
        printf("\n");
        if(options->shortErrors)
            break;
    }
}

void SVAsm_errorMacro(SVAsm_Options *options, SVAsm_MacroCall *macro, size_t index)
{
    if(!options || !macro)
        return;
    size_t start = 0;
    size_t end = 0;
    size_t currentIndex = 0;
    printf("\t");
    printMacroCall(macro, index, 0, &currentIndex, &start, &end);
    printf("\n\t");
    for(size_t i = 0; i < start; i++)
        printf(" ");
    printf("^");
    if(end > 0)
    {
        for(size_t i = 0; i < end - start - 1; i++)
            printf("-");
    }
    printf("\n");
}

void SVAsm_errorMacroStack(SVAsm_Options *options, SVAsm_MacroCallStack *stack)
{
    if(!options || !stack)
        return;
    for(ssize_t i = stack->itemCount - 1; i >= 0; i--)
    {
        SVAsm_MacroCallStackItem *item = stack->items[i];
        char const *filename = "built-in macro";
        char const *filepath = NULL;
        char const *macroName = NULL;
        int macroLine = 0;
        size_t index = 0;
        size_t argCount = 0;
        bool hasChild = false;
        size_t childIndex = 0;
        switch(item->type)
        {
            case SVASM_MACROCALLSTACKITEM_LINE:
            case SVASM_MACROCALLSTACKITEM_BUILTIN:
                argCount = item->origin.call->argCount;
                index = SVAsm_error_getMacroParam(item->origin.call, item->index);
                if(item->index >= argCount)
                    index = 0;
                if(item->type == SVASM_MACROCALLSTACKITEM_LINE)
                {
                    filename = item->value.line->macro->filename;
                    filepath = item->value.line->macro->filepath;
                    macroLine = item->value.line->macro->line;
                }
                else
                    macroName = item->origin.call->name;
                SVAsm_error(options, SVASM_BLANK, filename, filepath,
                            macroLine, "in macro \'%s\'", macroName);
                SVAsm_errorMacro(options, item->origin.call, index);
                // TODO count idx, show only root item
                break;
            case SVASM_MACROCALLSTACKITEM_ARRAY:
                argCount = item->origin.array->argCount;
                index = SVAsm_error_getArrayParam(item->origin.array, item->index);
                if(item->index >= argCount)
                    index = 0;
                SVAsm_error(options, SVASM_BLANK, "array expansion", NULL, 0, "in array");
                SVAsm_errorArray(options, item->origin.array, index);
                break;
            case SVASM_MACROCALLSTACKITEM_LABEL:
                SVAsm_error(options, SVASM_BLANK, "label expansion", NULL, 0, "in label");
                SVAsm_errorLabel(options, item->origin.label);
                break;
            case SVASM_MACROCALLSTACKITEM_BLOCK:
                {
                    index = SVAsm_stackItem_getLocation(SVAsm_stack_top(item->value.block.translator->stack));
                    SVAsm_Block *block = item->value.block.translator->blocks->blocks[index];
                    SVAsm_error(options, SVASM_BLANK, block->filename, block->filepath, block->line,
                                "in macro \'%s\'", item->origin.call->name);
                    SVAsm_errorTranslator(options, item->value.block.translator, block, SVAsm_error_getParam(block, childIndex + 1));
                }
                break;
            case SVASM_MACROCALLSTACKITEM_BLOCKPARAMS:
            case SVASM_MACROCALLSTACKITEM_BLOCKMACRO:
                // TODO count idx
                break;
            case SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND:
                childIndex = item->index;
                hasChild = true;
                break;
            default:
                break;
        }
        if(options->shortErrors && !hasChild)
            break;
    }
}

void SVAsm_errorArray(SVAsm_Options *options, SVAsm_Array *array, size_t index)
{
    if(!options || !array)
        return;
    size_t start = 0;
    size_t end = 0;
    size_t currentIndex = 0;
    printf("\t");
    printArray(array, index, 0, &currentIndex, &start, &end);
    printf("\n\t");
    for(size_t i = 0; i < start; i++)
        printf(" ");
    printf("^");
    if(end > 0)
    {
        for(size_t i = 0; i < end - start - 1; i++)
            printf("-");
    }
    printf("\n");
}

void SVAsm_errorLabel(SVAsm_Options *options, SVAsm_LabelCall *label)
{
    if(!options || !label)
        return;
    size_t start = 0;
    size_t end = 0;
    int sCount = 0;
    size_t currentIndex = 0;
    printf("\t");
    printf(":%s%n", label->label, &sCount);
    if(label->offset)
    {
        printf("%c", label->negative ? '-' : '+');
        sCount++;
        printArg(label->offset, 0, sCount, &currentIndex, &start, &end);
    }
    printf("\n\t");
    for(size_t i = 0; i < start; i++)
        printf(" ");
    printf("^");
    if(end > 0)
    {
        for(size_t i = 0; i < end - start - 1; i++)
            printf("-");
    }
    printf("\n");
}


size_t SVAsm_error_getLine(char const *source, size_t index, char **sourceLine)
{
    if(!source || !sourceLine)
        return 0;
    size_t start = 0;
    size_t end = 0;
    for(size_t i = index; i > 0; i--)
    {
        start = i + 1;
        if(source[i] == '\n')
            break;
    }
    for(size_t i = index; source[i]; i++)
    {
        end = i - 1;
        if(source[i] == '\n')
            break;
    }
    size_t length = end - start + 1;
    MMALLOC(*sourceLine, char, length + 1);
    strncpy(*sourceLine, source + start, length);
    (*sourceLine)[length] = '\0';
    return index - start;
}

size_t SVAsm_error_macroSize(SVAsm_MacroCall *macro)
{
    if(!macro)
        return 0;
    size_t size = 2;
    for(size_t i = 0; i < macro->argCount; i++)
    {
        if(macro->args[i]->type == SVASM_ARG_MACRO)
            size += SVAsm_error_macroSize(macro->args[i]->value.macro);
        else if(macro->args[i]->type == SVASM_ARG_ARRAY)
            size += SVAsm_error_arraySize(macro->args[i]->value.array);
        else
            size++;
    }
    return size;
}

size_t SVAsm_error_arraySize(SVAsm_Array *array)
{
    if(!array)
        return 0;
    size_t size = 1;
    for(size_t i = 0; i < array->argCount; i++)
    {
        if(array->args[i]->type == SVASM_ARG_MACRO)
            size += SVAsm_error_macroSize(array->args[i]->value.macro);
        else if(array->args[i]->type == SVASM_ARG_ARRAY)
            size += SVAsm_error_arraySize(array->args[i]->value.array);
        else
            size++;
    }
    return size;
}

size_t SVAsm_error_getParam(SVAsm_Block *block, size_t index)
{
    if(!block)
        return 0;
    size_t count = 0;
    switch(block->type)
    {
        case SVASM_BLOCK_INSTRUCTION:
            count = 2;
            for(size_t i = 0; i < block->content.instruction->operandCount; i++)
            {
                if(i == index)
                    return count;
                if(block->content.instruction->operands[i]->type == SVASM_OPERAND_MACRO)
                    count += SVAsm_error_macroSize(block->content.instruction->operands[i]->value.macro);
                else
                    count++;
            }
            break;
        case SVASM_BLOCK_COMMAND:
            count = 2;
            for(size_t i = 0; i < block->content.command->argCount; i++)
            {
                if(i == index)
                    return count;
                if(block->content.command->args[i]->type == SVASM_ARG_MACRO)
                    count += SVAsm_error_macroSize(block->content.command->args[i]->value.macro);
                else if(block->content.command->args[i]->type == SVASM_ARG_ARRAY)
                    count += SVAsm_error_arraySize(block->content.command->args[i]->value.array);
                else
                    count++;
            }
            break;
        default:
            break;
    }
    return 0;
}

size_t SVAsm_error_getMacroParam(SVAsm_MacroCall *macro, size_t index)
{
    if(!macro)
        return 0;
    size_t count = 2;
    for(size_t i = 0; i < macro->argCount; i++)
    {
        if(i == index)
            return count;
        if(macro->args[i]->type == SVASM_ARG_MACRO)
            count += SVAsm_error_macroSize(macro->args[i]->value.macro);
        else if(macro->args[i]->type == SVASM_ARG_ARRAY)
            count += SVAsm_error_arraySize(macro->args[i]->value.array);
        else
            count++;
    }
    return 0;
}

size_t SVAsm_error_getArrayParam(SVAsm_Array *array, size_t index)
{

    if(!array)
        return 0;
    size_t count = 1;
    for(size_t i = 0; i < array->argCount; i++)
    {
        if(i == index)
            return count;
        if(array->args[i]->type == SVASM_ARG_MACRO)
            count += SVAsm_error_macroSize(array->args[i]->value.macro);
        else if(array->args[i]->type == SVASM_ARG_ARRAY)
            count += SVAsm_error_arraySize(array->args[i]->value.array);
        else
            count++;
    }
    return 0;
}
