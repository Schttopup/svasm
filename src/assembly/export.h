/**
 * \file export.h
 * \brief Declares structures and functions related to address exports.
 * \author Schttopup
 */

#ifndef SVASM_EXPORT_H
#define SVASM_EXPORT_H

#include <stdint.h>


/**
 * Address export.
 */
struct SVAsm_Export_
{
    char *name;         //!< Export name
    uint16_t address;   //!< Export address
};

/// Type name for address exports.
typedef struct SVAsm_Export_ SVAsm_Export;

/**
 * Address export list.
 */
struct SVAsm_ExportList_
{
    SVAsm_Export **exports;     //!< Address exports
    size_t exportCount;         //!< Address export callCount
};

/// Type name for address export lists.
typedef struct SVAsm_ExportList_ SVAsm_ExportList;


/**
 * Creates a new export.
 * \param name The export name
 * \param address The export address
 * \return The created export
 */
SVAsm_Export* SVAsm_export_create(char const *name, uint16_t address);

/**
 * Destroys an export.
 * \param export The export to destroy
 */
void SVAsm_export_destroy(SVAsm_Export *export);

/**
 * Creates a new export list.
 * \return The created export list
 */
SVAsm_ExportList* SVAsm_exportList_create(void);

/**
 * Destroys an export list.
 * \param list The export list to destroy
 */
void SVAsm_exportList_destroy(SVAsm_ExportList *list);

/**
 * Adds an export to an export list. If the list already contains an export with the same name, the old export is replaced.
 * \param list The export list in which to set the export
 * \param export The export to set
 */
void SVAsm_exportList_set(SVAsm_ExportList *list, SVAsm_Export *export);


#endif //SVASM_EXPORT_H
