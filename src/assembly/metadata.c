#include "metadata.h"
#include <sdefs.h>
#include <string.h>


SVAsm_Metadata* SVAsm_metadata_create(void)
{
    SVAsm_Metadata *metadata = NULL;
    MMALLOC(metadata, SVAsm_Metadata, 1);
    metadata->keys = NULL;
    metadata->values = NULL;
    metadata->count = 0;
    return metadata;
}

void SVAsm_metadata_destroy(SVAsm_Metadata *metadata)
{
    if(!metadata)
        return;
    for(size_t i = 0; i < metadata->count; i++)
    {
        FFREE(metadata->keys[i]);
        FFREE(metadata->values[i]);
    }
    FFREE(metadata->keys);
    FFREE(metadata->values);
    FFREE(metadata);
}

void SVAsm_metadata_set(SVAsm_Metadata *metadata, char const *key, char const *value)
{
    if(!metadata || !key || !value)
        return;
    for(size_t i = 0; i < metadata->count; i++)
    {
        if(strcmp(metadata->keys[i], key) == 0)
        {
            RREALLOC(metadata->values[i], char, strlen(value) + 1);
            strcpy(metadata->values[i], value);
            return;
        }
    }
    RREALLOC(metadata->keys, char*, metadata->count + 1);
    RREALLOC(metadata->values, char*, metadata->count + 1);
    MMALLOC(metadata->keys[metadata->count], char, strlen(key) + 1);
    strcpy(metadata->keys[metadata->count], key);
    MMALLOC(metadata->values[metadata->count], char, strlen(value) + 1);
    strcpy(metadata->values[metadata->count], value);
    metadata->count++;
}

char const * SVAsm_metadata_get(SVAsm_Metadata *metadata, char const *key)
{
    if(!metadata || !key)
        return NULL;
    for(size_t i = 0; i < metadata->count; i++)
    {
        if(strcmp(metadata->keys[i], key) == 0)
            return metadata->values[i];
    }
    return NULL;
}
