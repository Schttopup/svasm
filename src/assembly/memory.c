/**
 * \file memory.c
 * \brief Defines functions related to memory representation objects.
 * \author Schttopup
 */

#include "memory.h"
#include <sdefs.h>
#include <stdbool.h>


SVAsm_MemoryObject* SVAsm_memoryObject_create(uint16_t varDataStart)
{
    SVAsm_MemoryObject *memory = NULL;
    MMALLOC(memory, SVAsm_MemoryObject, 1);
    memory->varDataStart = varDataStart;
    memory->constData = NULL;
    MMALLOC(memory->constData, uint8_t, varDataStart);
    memory->allocConstData = NULL;
    memory->allocVarData = NULL;
    unsigned int constAllocSize = varDataStart / 8;
    unsigned int varAllocSize = (0x10000 - varDataStart) / 8;
    if (varDataStart % 8)
    {
        constAllocSize++;
        varAllocSize++;
    }
    MMALLOC(memory->allocConstData, uint8_t, constAllocSize);
    MMALLOC(memory->allocVarData, uint8_t, varAllocSize);
    for(unsigned int i = 0; i < varDataStart; i++)
        memory->constData[i] = 0;
    for(unsigned int i = 0; i < constAllocSize; i++)
        memory->allocConstData[i] = 0;
    for(unsigned int i = 0; i < varAllocSize; i++)
        memory->allocVarData[i] = 0;
    memory->constDataSize = 0;
    memory->varDataSize = 0;
    memory->stackStart = memory->varDataStart;
    memory->stackSize = 0;
    return memory;
}

void SVAsm_memoryObject_destroy(SVAsm_MemoryObject *memory)
{
    if(!memory)
        return;
    FFREE(memory->constData);
    FFREE(memory->allocConstData);
    FFREE(memory->allocVarData);
    FFREE(memory);
}

bool SVAsm_memoryObject_bounds(SVAsm_MemoryObject *memory, uint16_t address, uint16_t size)
{
    if(!memory)
        return false;
    if(address < memory->varDataStart)
    {
        if(address + size > memory->varDataStart)
            return false;
        return true;
    }
    else
    {
        if(address + size > 0xFFFF)
            return false;
        return true;
    }
}

bool SVAsm_memoryObject_check(SVAsm_MemoryObject *memory, uint16_t address, uint16_t size)
{
    if(!memory)
        return false;
    if(address < memory->varDataStart)
    {
        if(address + size > memory->varDataStart)
            return false;
        for(unsigned int i = 0; i < size; i++)
        {
            if((memory->allocConstData[(address + i) / 8] >> ((address + i) % 8)) & 1)
                return false;
        }
        return true;
    }
    else
    {
        if(address + size > 0xFFFF)
            return false;
        for(unsigned int i = 0; i < size; i++)
        {
            if((memory->allocVarData[(address - memory->varDataStart + i) / 8] >> ((memory->varDataStart + i) % 8)) & 1)
                return false;
        }
        return true;
    }
}

void SVAsm_memoryObject_fill(SVAsm_MemoryObject *memory, uint16_t address, uint8_t *data, uint16_t size)
{
    if(!memory)
        return;
    if(address < memory->varDataStart)
    {
        for(unsigned int i = 0; i < (unsigned int)(address + size < memory->varDataStart ? size : memory->varDataStart - address); i++)
        {
            if(data)
                memory->constData[address + i] = data[i];
            memory->allocConstData[(address + i) / 8] |= 1 << ((address + i) % 8);
        }
        unsigned int dataEnd = MMIN(address + size, memory->varDataStart);
        if(dataEnd > memory->constDataSize)
            memory->constDataSize = dataEnd;
    }
    else
    {
        for(unsigned int i = 0; i < (unsigned int)(address + size < 0x10000 ? size : 0x10000 - address); i++)
            memory->allocVarData[(address - memory->varDataStart + i) / 8] |= 1 << ((address - memory->varDataStart + i) % 8);
        unsigned int dataEnd = MMIN(address + size, 0x10000) - memory->varDataStart;
        if(dataEnd > memory->varDataSize)
            memory->varDataSize = dataEnd;
    }
}
