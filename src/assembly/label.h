/**
 * \file label.h
 * \brief Declares structures and functions related to code labels.
 * \author Schttopup
 */

#ifndef SVASM_LABEL_H
#define SVASM_LABEL_H

#include <stdint.h>
#include <stdbool.h>


/**
 * \brief Code label.
 */
struct SVAsm_Label_
{
    char *name;             //!< Label name
    uint16_t address;       //!< Label definition address
    uint16_t *locations;    //!< Label call addresses
    int32_t *offsets;       //!< Label call offsets
    size_t callCount;       //!< Label call count
    bool assigned;          //!< Definition address assigned
};

/// Type name for code label.
typedef struct SVAsm_Label_ SVAsm_Label;


/**
 * Code label list.
 */
struct SVAsm_LabelList_
{
    SVAsm_Label **labels;
    size_t labelCount;
};

/// Type name for code label list.
typedef struct SVAsm_LabelList_ SVAsm_LabelList;


/**
 * \brief Creates a label.
 * \param name The label name
 * \return The created label
 */
SVAsm_Label* SVAsm_label_create(char const *name);

/**
 * \brief Destroys a label.
 * \param label The label to destroy
 */
void SVAsm_label_destroy(SVAsm_Label *label);

/**
 * \brief Sets the address of a label.
 * Assigns the address only if the label wasn't assigned yet.
 * \param label The label
 * \param address The address to assign
 */
void SVAsm_label_setAddress(SVAsm_Label *label, uint16_t address);

/**
 * \brief Adds a call location to a label.
 * Adds the location only if it hasn't been added before.
 * \param label the label
 * \param location The location of the call to add
 * \param offset The offset of the call to add
 */
void SVAsm_label_addCall(SVAsm_Label *label, uint16_t location, int32_t offset);

/**
 * \brief Creates a new label list.
 * \return The created list
 */
SVAsm_LabelList* SVAsm_labelList_create();

/**
 * \brief Destroys a label list.
 * \param list The list to destroy
 */
void SVAsm_labelList_destroy(SVAsm_LabelList *list);

/**
 * \brief Adds a label to a label list.
 * \param list The list
 * \param label The label to add
 */
void SVAsm_labelList_add(SVAsm_LabelList *list, SVAsm_Label *label);

/**
 * \brief Gets the label corresponding to a name.
 * \param list The label list
 * \param name The label name
 * \return The corresponding label, or NULL if no label was found
 */
SVAsm_Label* SVAsm_labelList_get(SVAsm_LabelList *list, char const *name);


#endif //SVASM_LABEL_H
