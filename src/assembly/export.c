/**
 * \file export.c
 * \brief Defines functions related to address exports.
 * \author Schttopup
 */

#include "export.h"
#include <sdefs.h>
#include <string.h>


SVAsm_Export* SVAsm_export_create(char const *name, uint16_t address)
{
    if(!name)
        return NULL;
    SVAsm_Export *export = NULL;
    MMALLOC(export, SVAsm_Export, 1);
    export->name = NULL;
    MMALLOC(export->name, char, strlen(name) + 1);
    strcpy(export->name, name);
    export->address = address;
    return export;
}

void SVAsm_export_destroy(SVAsm_Export *export)
{
    if(!export)
        return;
    FFREE(export->name);
    FFREE(export);
}

SVAsm_ExportList* SVAsm_exportList_create(void)
{
    SVAsm_ExportList *list = NULL;
    MMALLOC(list, SVAsm_ExportList, 1);
    list->exportCount = 0;
    list->exports = NULL;
    return list;
}

void SVAsm_exportList_destroy(SVAsm_ExportList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->exportCount; i++)
        SVAsm_export_destroy(list->exports[i]);
    FFREE(list->exports);
    FFREE(list);
}

void SVAsm_exportList_set(SVAsm_ExportList *list, SVAsm_Export *export)
{
    if(!list || !export)
        return;
    for(size_t i = 0; i < list->exportCount; i++)
    {
        if(strcmp(list->exports[i]->name, export->name) == 0)
        {
            SVAsm_export_destroy(list->exports[i]);
            list->exports[i] = export;
            return;
        }
    }
    RREALLOC(list->exports, SVAsm_Export*, list->exportCount + 1);
    list->exports[list->exportCount] = export;
    list->exportCount++;
}
