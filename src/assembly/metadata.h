#ifndef SVASM_METADATA_H
#define SVASM_METADATA_H

#include <stdlib.h>


struct SVAsm_Metadata_
{
    char **keys;
    char **values;
    size_t count;
};

typedef struct SVAsm_Metadata_ SVAsm_Metadata;


SVAsm_Metadata* SVAsm_metadata_create(void);

void SVAsm_metadata_destroy(SVAsm_Metadata *metadata);

void SVAsm_metadata_set(SVAsm_Metadata *metadata, char const *key, char const *value);

char const * SVAsm_metadata_get(SVAsm_Metadata *metadata, char const *key);


#endif //SVASM_METADATA_H
