/**
 * \file memory.h
 * \brief Declares structures and functions related to memory representation objects.
 * \author Schttopup
 */

#ifndef SVASM_MEMORY_H
#define SVASM_MEMORY_H

#include <stdint.h>
#include <stdbool.h>


/**
 * Memory representation object.
 */
struct SVAsm_MemoryObject_
{
    uint8_t *constData;         //!< Constant data
    uint8_t *allocConstData;    //!< Constant data allocation table
    uint8_t *allocVarData;      //!< Variable data allocation table
    uint16_t varDataStart;      //!< Variable data start address
    uint16_t constDataSize;     //!< Constant data segment size
    uint16_t varDataSize;       //!< Variable data segment size
    uint16_t stackStart;        //!< Stack start address
    uint16_t stackSize;         //!< Stack size
};

/// Type name for memory representation objects.
typedef struct SVAsm_MemoryObject_ SVAsm_MemoryObject;


/**
 * \brief Creates a new memory object.
 * \return The created memory object
 */
SVAsm_MemoryObject* SVAsm_memoryObject_create(uint16_t constDataSize);

/**
 * \brief Destroys a memory object.
 * \param memory The memory object to destroy
 */
void SVAsm_memoryObject_destroy(SVAsm_MemoryObject *memory);

/**
 * \brief Checks if a memory space is within bounds.
 * \param memory The memory object
 * \param address The address of the memory space
 * \param size The size of the memory space
 * \return true if the memory space is in bounds, false otherwise.
 */
bool SVAsm_memoryObject_bounds(SVAsm_MemoryObject *memory, uint16_t address, uint16_t size);

/**
 * \brief Checks if a memory space is free.
 * \param memory The memory object
 * \param address The address of the memory space
 * \param size The size of the memory space
 * \return true if the memory space is free, false otherwise or if the space is out of bounds.
 */
bool SVAsm_memoryObject_check(SVAsm_MemoryObject *memory, uint16_t address, uint16_t size);

/**
 * \brief Fills a memory space with data.
 * \param memory The memory object
 * \param address The address of the memory space
 * \param data The data to use
 * \param size The size of the data
 */
void SVAsm_memoryObject_fill(SVAsm_MemoryObject *memory, uint16_t address, uint8_t *data, uint16_t size);


#endif //SVASM_MEMORY_H
