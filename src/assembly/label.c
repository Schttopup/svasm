/**
 * \file label.c
 * \brief Defines functions related to code labels.
 * \author Schttopup
 */

#include "label.h"
#include <sdefs.h>
#include <string.h>


SVAsm_Label* SVAsm_label_create(char const *name)
{
    if(!name || !strlen(name))
        return NULL;
    SVAsm_Label *label = NULL;
    MMALLOC(label, SVAsm_Label, 1);
    label->name = NULL;
    MMALLOC(label->name, char, strlen(name) + 1);
    strcpy(label->name, name);
    label->address = 0;
    label->assigned = false;
    label->locations = NULL;
    label->offsets = NULL;
    label->callCount = 0;
    return label;
}

void SVAsm_label_destroy(SVAsm_Label *label)
{
    if(!label)
        return;
    FFREE(label->locations);
    FFREE(label->offsets);
    FFREE(label->name);
    FFREE(label);
}

void SVAsm_label_setAddress(SVAsm_Label *label, uint16_t address)
{
    if(!label)
        return;
    if(!label->assigned)
    {
        label->address = address;
        label->assigned = true;
    }
}

void SVAsm_label_addCall(SVAsm_Label *label, uint16_t location, int32_t offset)
{
    if(!label)
        return;
    bool found = false;
    for(size_t i = 0; i < label->callCount; i++)
    {
        if(label->locations[i] == location)
        {
            found = true;
            break;
        }
    }
    if(!found)
    {
        RREALLOC(label->locations, uint16_t, label->callCount + 1);
        label->locations[label->callCount] = location;
        RREALLOC(label->offsets, int32_t, label->callCount + 1);
        label->offsets[label->callCount] = offset;
        label->callCount++;
    }
}

SVAsm_LabelList* SVAsm_labelList_create()
{
    SVAsm_LabelList *list = NULL;
    MMALLOC(list, SVAsm_LabelList, 1);
    list->labels = NULL;
    list->labelCount = 0;
    return list;
}

void SVAsm_labelList_destroy(SVAsm_LabelList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->labelCount; i++)
        SVAsm_label_destroy(list->labels[i]);
    FFREE(list->labels);
    FFREE(list);
}

void SVAsm_labelList_add(SVAsm_LabelList *list, SVAsm_Label *label)
{
    if(!list || !label)
        return;
    RREALLOC(list->labels, SVAsm_Label*, list->labelCount + 1);
    list->labels[list->labelCount] = label;
    list->labelCount++;
}

SVAsm_Label* SVAsm_labelList_get(SVAsm_LabelList *list, char const *name)
{
    if(!list || !name)
        return NULL;
    for(size_t i = 0; i < list->labelCount; i++)
    {
        if(strcmp(list->labels[i]->name, name) == 0)
            return list->labels[i];
    }
    return NULL;
}
