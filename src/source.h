/**
 * \file source.h
 * \brief Declares structures and functions related to data sources.
 * \author Schttopup
 */

#ifndef SVASM_SOURCE_H
#define SVASM_SOURCE_H

#include <stdint.h>
#include <stdbool.h>


/**
 * Data source loader callback.
 * Loads a source given a name and auxiliary data.
 * data and size are to be filled by the callback.
 * \param name The name of the source to load
 * \param aux Auxiliary data
 * \param data Pointer to an unallocated buffer
 * \param size Pointer to the size of the buffer
 * \return true if the source was loaded, false otherwise
 */
typedef bool (*SVAsm_SourceCallback)(char const *name, void *aux, uint8_t **data, size_t *size, char **filename, char **filepath);

/**
 * Data source.
 */
struct SVAsm_Source_
{
    SVAsm_SourceCallback callback;      //!< Loader callback
    void *aux;                          //!< Auxiliary data
};

/// Type name for data sources.
typedef struct SVAsm_Source_ SVAsm_Source;


/**
 * Creates a data source.
 * \param callback The callback to use
 * \param aux Auxiliary data
 * \return The created data source
 */
SVAsm_Source *SVAsm_source_create(SVAsm_SourceCallback callback, void *aux);

/**
 * Loads a file from a source.
 * \param source The source
 * \param name The name of the file to load
 * \param data Pointer to an unallocated buffer
 * \param size Pointer to the size of the buffer
 * \param filename Pointer to the loaded file name
 * \param filepath Pointer to the loaded file path
 * \return true if the source was loaded, false otherwise
 */
bool SVAsm_source_load(SVAsm_Source *source, char const *name, uint8_t **data, size_t *size, char **filename, char **filepath);


/**
 * Creates a default data source auxiliary data object.
 * \return The created object
 */
void* SVAsm_source_defCreate(void);

/**
 * Destroys a default data source auxiliary data object.
 * \param aux The auxiliary data object
 */
void SVAsm_source_defDestroy(void *aux);

/**
 * Adds a path to a default data source auxiliary data object.
 * \param aux The auxiliary data object
 * \param path The path to add
 */
void SVAsm_source_defAdd(void *aux, char const *path);

/**
 * Default data source loader call back.
 * Searches sequentially in a list of paths for a file with a given name.
 * \param name The name of the file to load
 * \param aux Auxiliary data
 * \param data Pointer to an unallocated buffer
 * \param size Pointer to the size of the buffer
 * \param filename Pointer to the loaded file name
 * \param filepath Pointer to the loaded file path
 * \return true if the source was loaded, false otherwise
 */
bool SVAsm_source_defCallback(char const *name, void *aux, uint8_t **data, size_t *size, char **filename, char **filepath);

#endif //SVASM_SOURCE_H
