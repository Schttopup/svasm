/**
 * \file app.c
 * \brief Defines functions related to the svasm application.
 * \author Schttopup
 */

#include "app.h"
#include "parsing/parser.h"
#include "parsing/token.h"
#include "building/builder.h"
#include "building/block.h"
#include "translation/translator.h"
#include "error.h"
#include <sdefs.h>
#include <string.h>
#include <sfile/sfile.h>
#include <sfile/ssf.h>


SArg_OptionList* SVAsm_app_prepareCommandlineOptions(void)
{
    SArg_OptionList *list = SArg_optionList_create("svasm");

    SArg_Option *inputFileOption = SArg_option_create('i', "input");
    SArg_option_setFlags(inputFileOption, SARG_OPTION_REQUIRED | SARG_OPTION_ARGUMENTS);
    SArg_option_setHelp(inputFileOption, "Input SV Assembly source");
    SArg_optionList_addOption(list, inputFileOption);

    SArg_Option *outputFileOption = SArg_option_create('o', "output");
    SArg_option_setFlags(outputFileOption, SARG_OPTION_ARGUMENTS);
    SArg_option_setDefault(outputFileOption, "out.svb");
    SArg_option_setHelp(outputFileOption, "Output SV Binary file");
    SArg_optionList_addOption(list, outputFileOption);

    SArg_Option *varStartOption = SArg_option_create('\0', "var-start");
    SArg_option_setFlags(varStartOption, SARG_OPTION_ARGUMENTS);
    SArg_option_setDefault(varStartOption, "8000");
    SArg_option_setHelp(varStartOption, "Variable data start address");
    SArg_optionList_addOption(list, varStartOption);

    SArg_Option *outputListingOption = SArg_option_create('l', "list");
    SArg_option_setHelp(outputListingOption, "Output assembly listing");
    SArg_optionList_addOption(list, outputListingOption);

    SArg_Option *defineOption = SArg_option_create('D', "define");
    SArg_option_setFlags(defineOption, SARG_OPTION_ARGUMENTS | SARG_OPTION_MULTIPLE);
    SArg_option_setHelp(defineOption, "Define variable");
    SArg_optionList_addOption(list, defineOption);

    SArg_Option *warningOption = SArg_option_create('W', NULL);
    SArg_option_setFlags(warningOption, SARG_OPTION_ARGUMENTS | SARG_OPTION_MULTIPLE);
    SArg_option_setHelp(warningOption, "Set warning option");
    SArg_optionList_addOption(list, warningOption);

    SArg_Option *libraryOption = SArg_option_create('I', "include-path");
    SArg_option_setFlags(libraryOption, SARG_OPTION_ARGUMENTS | SARG_OPTION_MULTIPLE);
    SArg_option_setHelp(libraryOption, "Add source include path");
    SArg_optionList_addOption(list, libraryOption);

    SArg_Option *fullPathsOption = SArg_option_create('\0', "full-paths");
    SArg_option_setHelp(fullPathsOption, "Full file paths in output");
    SArg_optionList_addOption(list, fullPathsOption);

    SArg_Option *shortErrorsOption = SArg_option_create('\0', "short-errors");
    SArg_option_setHelp(shortErrorsOption, "Short errors in output");
    SArg_optionList_addOption(list, shortErrorsOption);

    SArg_Option *noColorOption = SArg_option_create('\0', "no-color");
    SArg_option_setHelp(noColorOption, "No colors in output");
    SArg_optionList_addOption(list, noColorOption);

    return list;
}

SVAsm_Options* SVAsm_app_makeOptions(SArg_ParameterList *parameters, SArg_OptionList *options)
{
    if(!parameters || !options)
        return NULL;
    SVAsm_Options *asmOptions = SVAsm_options_create();
    SArg_Parameter *inputFileParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'i', NULL));
    SArg_Parameter *outputFileParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'o', NULL));
    SArg_Parameter *listingParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'l', NULL));
    SArg_Parameter *symbolParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'D', NULL));
    SArg_Parameter *warningParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'W', NULL));
    SArg_Parameter *libraryParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, 'I', NULL));
    SArg_Parameter *varStartParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, '\0', "var-start"));
    SArg_Parameter *fullPathsParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, '\0', "full-paths"));
    SArg_Parameter *shortErrorsParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, '\0', "short-errors"));
    SArg_Parameter *noColorParam = SArg_parameterList_getParameter(parameters, SArg_optionList_getOption(options, '\0', "no-color"));

    if(!inputFileParam || !outputFileParam || !varStartParam)
        goto error;

    if(inputFileParam->valueCount < 1 || strlen(inputFileParam->values[0]) < 1)
        goto error;
    char *inputFileName = SFile_path_absolute(inputFileParam->values[0]);
    if(!inputFileName)
        goto error;
    asmOptions->inputFileName = inputFileName;

    if(outputFileParam->valueCount < 1 || strlen(outputFileParam->values[0]) < 1)
        goto error;
    char *outputFileName = SFile_path_absolute(outputFileParam->values[0]);
    if(!outputFileName)
        goto error;
    asmOptions->outputFileName = outputFileName;

    if(varStartParam->valueCount < 1)
        goto error;
    long parsedVarStart = strtol(varStartParam->values[0], NULL, 16);
    if(parsedVarStart < 1 || parsedVarStart > 0xFFFF)
        goto error;
    asmOptions->varDataStart = (uint16_t)parsedVarStart;

    if(listingParam)
        asmOptions->outputListing = true;
    if(fullPathsParam)
        asmOptions->fullPaths = true;
    if(shortErrorsParam)
        asmOptions->shortErrors = true;
    if(noColorParam)
        asmOptions->outputColors = false;

    if(symbolParam)
    {
        MMALLOC(asmOptions->definedSymbols, char*, symbolParam->valueCount);
        for(size_t i = 0; i < symbolParam->valueCount; i++)
        {
            MMALLOC(asmOptions->definedSymbols[i], char, strlen(symbolParam->values[i]) + 1);
            strcpy(asmOptions->definedSymbols[i], symbolParam->values[i]);
        }
        asmOptions->definedSymbolCount = symbolParam->valueCount;
    }

    if(warningParam)
    {
        for(size_t i = 0; i < warningParam->valueCount; i++)
        {
            if(!SVAsm_app_setWarning(asmOptions, warningParam->values[i]))
            {
                SVAsm_message(asmOptions, SVASM_FATAL, "Invalid warning flag : %s", warningParam->values[i]);
                goto error;
            }
        }
    }

    if(libraryParam)
    {
        for(size_t i = 0; i < libraryParam->valueCount; i++)
        {
            char *fullPath = SFile_path_absolute(libraryParam->values[i]);
            if(SFile_path_exists(fullPath) && SFile_path_isDir(fullPath))
            {
                SVAsm_source_defAdd(asmOptions->source->aux, fullPath);
                FFREE(fullPath);
            }
            else
            {
                SVAsm_message(asmOptions, SVASM_FATAL, "Invalid source directory : %s", fullPath);
                FFREE(fullPath);
                goto error;
            }
        }
    }

    return asmOptions;

    error:
        SVAsm_options_destroy(asmOptions);
        return NULL;
}

void SVAsm_app_defineSymbols(SVAsm_Translator *translator, SVAsm_Options *options)
{
    if(!translator || !options)
        return;
    for(size_t i = 0; i < options->definedSymbolCount; i++)
    {
        char *eq = NULL;
        for(size_t j = 0; options->definedSymbols[i][j]; j++)
        {
            if(options->definedSymbols[i][j] == '=')
            {
                eq = options->definedSymbols[i] + j;
                break;
            }
        }
        if(eq)
        {
            char *name = NULL;
            size_t len = eq - options->definedSymbols[i];
            MMALLOC(name, char, len + 1);
            strncpy(name, options->definedSymbols[i], len);
            name[len] = '\0';
            SVAsm_Macro *macro = SVAsm_macro_create(name, SVASM_MACRO_CONSTANT);
            FFREE(name);
            char *macroValue = NULL;
            MMALLOC(macroValue, char, strlen(eq + 1) + 1);
            strcpy(macroValue, eq + 1);
            SVAsm_Arg *macroArg = SVAsm_arg_create(SVASM_ARG_STRING, macroValue);
            SVAsm_macro_setConstant(macro, macroArg);
            SVAsm_arg_destroy(macroArg);
            FFREE(macroValue);
            SVAsm_macroList_add(translator->macros, macro);
        }
        else
        {
            SVAsm_Macro *macro = SVAsm_macro_create(options->definedSymbols[i], SVASM_MACRO_CONSTANT);
            SVAsm_Arg *macroArg = SVAsm_arg_create(SVASM_ARG_NUMBER, &(SVAsm_NumberType){1});
            SVAsm_macro_setConstant(macro, macroArg);
            SVAsm_arg_destroy(macroArg);
            SVAsm_macroList_add(translator->macros, macro);
        }
    }
}

bool SVAsm_app_setWarning(SVAsm_Options *options, char const *warningFlag)
{
    if(!options || !warningFlag)
        return false;
    bool newValue = true;
    char const *flagName = warningFlag;
    bool flagFound = false;
    if(strncmp(warningFlag, "no-", 3) == 0)
    {
        newValue = false;
        flagName += 3;
    }
    for(size_t i = 0; i < SVASM_OPTIONS_WARNING_COUNT; i++)
    {
        if(strcmp(flagName, SVAsm_options_warningNames[i]) == 0)
        {
            *(bool*)(((void*)options) + SVAsm_options_warningOffsets[i]) = newValue;
            flagFound = true;
            break;
        }
    }
    return flagFound;
}

bool SVAsm_app_assemble(SVAsm_Options *options)
{
    if(!options)
        return false;
    bool returnVal = true;

    uint8_t *sourceText;
    size_t sourceSize;
    bool read = SFile_loadRaw(options->inputFileName, &sourceText, &sourceSize);
    if(!read)
    {
        SVAsm_message(options, SVASM_FATAL, "Unable to read file <%s>", options->inputFileName);
        return false;
    }
    if(!sourceText)
    {
        SVAsm_message(options, SVASM_WARNING, "Empty source file <%s>", options->inputFileName);
        return true;
    }
    RREALLOC(sourceText, uint8_t, sourceSize + 1);
    sourceText[sourceSize] = '\0';

    char *sourceFileName = SFile_path_fileName(options->inputFileName);
    char *sourceFilePath = SFile_path_basePath(options->inputFileName);
    SVAsm_TokenList *list = SVAsm_parser_parse((char*)sourceText, sourceFileName, sourceFilePath, options);
    if(!list)
    {
        returnVal = false;
        goto parse_error;
    }

    SVAsm_Builder *builder = SVAsm_builder_create(list, sourceFileName, sourceFilePath, 0, options);
    SVAsm_builder_build(builder);
    if(builder->error)
    {
        returnVal = false;
        goto build_error;
    }
    SVAsm_BlockList *blocks = builder->blocks;
    if(blocks->blockCount == 0)
    {
        SVAsm_message(options, SVASM_WARNING, "Empty source file <%s>", options->inputFileName);
        goto build_error;
    }

    SVAsm_Translator *translator = SVAsm_translator_create(blocks, options);
    translator->baseFilename = sourceFileName;
    translator->baseFilepath = sourceFilePath;
    SVAsm_translator_setSource(translator, options->source);
    SVAsm_app_defineSymbols(translator, options);

    SVAsm_translator_translate(translator);
    if(translator->error)
    {
        returnVal = false;
        goto translate_error;
    }

    SVAsm_app_saveBinary(options, translator);

    translate_error:
        SVAsm_translator_destroy(translator);
    build_error:
        SVAsm_builder_destroy(builder);
        SVAsm_tokenList_destroy(list);
    parse_error:
        FFREE(sourceText);
        FFREE(sourceFileName);
        FFREE(sourceFilePath);
    return returnVal;
}


static void SVAsm_app_saveBE16(uint8_t *dest, uint16_t src)
{
    if(!dest)
        return;
    dest[0] = (src & 0xFF00) >> 8;
    dest[1] = src & 0x00FF;
}

bool SVAsm_app_saveBinary(SVAsm_Options *options, SVAsm_Translator *translator)
{
    if(!options || !translator)
        return false;
    SFile_SSFTree *outputTree = SFile_ssfTree_create("SVB ");

    uint8_t *headerData = NULL;
    MMALLOC(headerData, uint8_t, 10);
    SVAsm_app_saveBE16(headerData, translator->memory->constDataSize);
    SVAsm_app_saveBE16(headerData + 2, translator->memory->varDataSize);
    SVAsm_app_saveBE16(headerData + 4, translator->memory->varDataStart);
    SVAsm_app_saveBE16(headerData + 6, translator->memory->stackStart);
    SVAsm_app_saveBE16(headerData + 8, translator->memory->stackSize);
    SFile_SSFChunk *headerChunk = SFile_ssfChunk_create("HEAD", headerData, 10);
    FFREE(headerData);

    size_t constDataSize = translator->memory->constDataSize;
    SFile_SSFChunk *dataChunk = SFile_ssfChunk_create("DATA", translator->memory->constData, constDataSize);

    uint8_t *exportData = NULL;
    size_t exportDataSize = 0;
    for(size_t i = 0; i < translator->exports->exportCount; i++)
    {
        size_t sizeToAdd = strlen(translator->exports->exports[i]->name) + 3;
        RREALLOC(exportData, uint8_t, exportDataSize + sizeToAdd);
        exportData[exportDataSize] = (translator->exports->exports[i]->address & 0xFF00) >> 8;
        exportData[exportDataSize + 1] = translator->exports->exports[i]->address & 0x00FF;
        strcpy((char*)(exportData + exportDataSize + 2), translator->exports->exports[i]->name);
        exportData[exportDataSize + sizeToAdd - 1] = 0;
        exportDataSize += sizeToAdd;
    }
    SFile_SSFChunk *exportChunk = SFile_ssfChunk_create("EXPT", exportData, exportDataSize);
    FFREE(exportData);

    uint8_t *metaData = NULL;
    size_t metaDataSize = 0;
    for(size_t i = 0; i < translator->metadata->count; i++)
    {
        size_t keySize = strlen(translator->metadata->keys[i]) + 1;
        size_t valueSize = strlen(translator->metadata->values[i]) + 1;
        RREALLOC(metaData, uint8_t, metaDataSize + keySize + valueSize);
        strcpy((char*)metaData + metaDataSize, translator->metadata->keys[i]);
        strcpy((char*)metaData + metaDataSize + keySize, translator->metadata->values[i]);
        metaDataSize += keySize + valueSize;
    }
    SFile_SSFChunk *metaChunk = SFile_ssfChunk_create("META", metaData, metaDataSize);
    FFREE(metaData);

    SFile_ssfTree_add(outputTree, headerChunk);
    SFile_ssfTree_add(outputTree, dataChunk);
    SFile_ssfTree_add(outputTree, exportChunk);
    SFile_ssfTree_add(outputTree, metaChunk);
    uint8_t *outputRaw = NULL;
    size_t outputRawSize = 0;
    SFile_ssfTree_encode(outputTree, &outputRaw, &outputRawSize);
    bool saveOk = SFile_saveRaw(options->outputFileName, outputRaw, outputRawSize);
    if(!saveOk)
        SVAsm_message(options, SVASM_FATAL, "Unable to save file <%s>", options->outputFileName);
    SFile_ssfTree_destroy(outputTree);
    FFREE(outputRaw);
    return saveOk;
}
