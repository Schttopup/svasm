/**
 * \file parser.c
 * \brief Defines functions related to parsing.
 * \author Schttopup
 */

#include "parser.h"
#include "token.h"
#include "register.h"
#include "../error.h"
#include <sdefs.h>
#include <stdbool.h>
#include <string.h>


SVAsm_TokenList *SVAsm_parser_parse(char const *source, char const *filename, char const *filepath, SVAsm_Options *options)
{
    if(!source)
        return NULL;
    SVAsm_ParserState state = SVASM_PSTATE_BLANK;
    SVAsm_Parser *parser = NULL;
    MMALLOC(parser, SVAsm_Parser, 1);
    parser->source = source;
    parser->filename = filename;
    parser->filepath = filepath;
    parser->index = 0;
    parser->line = 0;
    parser->tokens = SVAsm_tokenList_create();
    parser->options = options;
    bool stateEnd = true;
    bool stop = false;
    while(*(source + parser->index) != '\0' && !stop)
    {
        if(!stateEnd)
        {
            switch(state)
            {
                case SVASM_PSTATE_BLANK:
                    if(SVAsm_parser_isWhitespace(*(parser->source + parser->index)))
                        parser->index++;
                    else
                        stateEnd = true;
                    break;
                case SVASM_PSTATE_SEPARATOR:
                    if(!SVAsm_parser_getSeparator(parser))
                    {
                        char *sourceLine = NULL;
                        unsigned int column = SVAsm_error_getLine(parser->source, parser->index + 1, &sourceLine);
                        SVAsm_error(parser->options, SVASM_ERROR, filename, filepath, parser->line, "syntax error");
                        SVAsm_errorParser(parser->options, sourceLine, column);
                        FFREE(sourceLine);
                        stop = true;
                    }
                    stateEnd = true;
                    break;
                case SVASM_PSTATE_SYMBOL:
                    if(!SVAsm_parser_getSymbol(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case SVASM_PSTATE_STRING:
                    if(!SVAsm_parser_getString(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case SVASM_PSTATE_COMMENT:
                    if(!SVAsm_parser_getComment(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case SVASM_PSTATE_NUMBER:
                    if(!SVAsm_parser_getNumber(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case SVASM_PSTATE_REGISTER:
                    if(!SVAsm_parser_getRegister(parser))
                        stop = true;
                    stateEnd = true;
                    break;
            }
        }
        else
        {
            char c = *(parser->source + parser->index);
            if(SVAsm_parser_isWhitespace(c))
                state = SVASM_PSTATE_BLANK;
            else if(SVAsm_parser_isCharacter(c))
                state = SVASM_PSTATE_SYMBOL;
            else if(SVAsm_parser_isNumber(c) || c == '$' || c == '&')
                state = SVASM_PSTATE_NUMBER;
            else if(c == '%')
                state = SVASM_PSTATE_REGISTER;
            else if(c == ';')
                state = SVASM_PSTATE_COMMENT;
            else if(c == '\"')
            {
                state = SVASM_PSTATE_STRING;
                parser->index++;
            }
            else
                state = SVASM_PSTATE_SEPARATOR;
            stateEnd = false;
        }
    }
    if(stop)
    {
        SVAsm_tokenList_destroy(parser->tokens);
        parser->tokens = NULL;
    }
    SVAsm_TokenList *list = parser->tokens;
    FFREE(parser);
    return list;
}


bool SVAsm_parser_getSeparator(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    SVAsm_SeparatorType sepType = SVAsm_token_getSeparator(*(parser->source + parser->index));
    if(sepType == SVASM_SEPARATOR_NONE)
    {
        if(*(parser->source + parser->index) == '\n')
        {
            SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_ENDLINE, NULL, parser->line);
            SVAsm_tokenList_add(parser->tokens, token);
            parser->line++;
            parser->index++;
        }
        else if(*(parser->source + parser->index) == '\r')
            parser->index++;
        else
            return false;
    }
    else
    {
        SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_SEPARATOR, &sepType, parser->line);
        SVAsm_tokenList_add(parser->tokens, token);
        parser->index++;
    }
    return true;
}

bool SVAsm_parser_getSymbol(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    size_t endIndex = parser->index;
    while(SVAsm_parser_isCharacter(*(parser->source + endIndex)) ||
          SVAsm_parser_isNumber(*(parser->source + endIndex)))
        endIndex++;
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index + 1));
    for(size_t i = 0; i < (endIndex - parser->index); i++)
        tempStr[i] = *(parser->source + parser->index + i);
    tempStr[endIndex - parser->index] = '\0';
    SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_SYMBOL, tempStr, parser->line);
    FFREE(tempStr);
    SVAsm_tokenList_add(parser->tokens, token);
    parser->index = endIndex;
    return true;
}

bool SVAsm_parser_getString(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    size_t endIndex = parser->index;
    bool backslashCount = false;
    while(true)
    {
        if(*(parser->source + endIndex) == '\0')
            break;
        if(*(parser->source + endIndex) == '\n' ||
           *(parser->source + endIndex) == '\r')
        {
            parser->line++;
            break;
        }
        if(*(parser->source + endIndex) == '\\')
            backslashCount = !backslashCount;
        else if(*(parser->source + endIndex) == '\"')
        {
            if(!backslashCount)
                break;
        }
        else
            backslashCount = false;
        endIndex++;
    }
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index + 1));
    for(size_t i = 0; i < (endIndex - parser->index); i++)
        tempStr[i] = *(parser->source + parser->index + i);
    tempStr[endIndex - parser->index] = '\0';
    char *escapedStr = SVAsm_parser_escapeString(tempStr);
    SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_STRING, escapedStr, parser->line);
    FFREE(tempStr);
    FFREE(escapedStr);
    SVAsm_tokenList_add(parser->tokens, token);
    parser->index = (*(parser->source + endIndex) == '\"') ? endIndex + 1 : endIndex;
    return true;
}

bool SVAsm_parser_getComment(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    size_t endIndex = parser->index;
    while(*(parser->source + endIndex) != '\0' &&
          *(parser->source + endIndex) != '\n' &&
          *(parser->source + endIndex) != '\r')
        endIndex++;
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index));
    for(size_t i = 1; i < (endIndex - parser->index); i++)
        tempStr[i - 1] = *(parser->source + parser->index + i);
    tempStr[endIndex - parser->index - 1] = '\0';
    SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_COMMENT, tempStr, parser->line);
    FFREE(tempStr);
    SVAsm_tokenList_add(parser->tokens, token);
    if(*(parser->source + endIndex) != '\0' &&
            (*(parser->source + endIndex) == '\n' ||
             *(parser->source + endIndex) == '\r') &&
            *(parser->source + endIndex) != *(parser->source + endIndex - 1))
        endIndex++;
    parser->index = endIndex;
    return true;
}

bool SVAsm_parser_getNumber(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    int number = 0;
    size_t endIndex = parser->index;
    if(*(parser->source + endIndex) == '$')
    {
        endIndex++;
        while(SVAsm_parser_isHexNumber(*(parser->source + endIndex)))
            endIndex++;
        number = SVAsm_parser_parseNumber(parser->source + parser->index + 1, endIndex - parser->index - 1, 16);
    }
    else if(*(parser->source + endIndex) == '&')
    {
        endIndex++;
        while(SVAsm_parser_isBinNumber(*(parser->source + endIndex)))
            endIndex++;
        number = SVAsm_parser_parseNumber(parser->source + parser->index + 1, endIndex - parser->index - 1, 2);
    }
    else
    {
        while(SVAsm_parser_isNumber(*(parser->source + endIndex)))
            endIndex++;
        number = SVAsm_parser_parseNumber(parser->source + parser->index, endIndex - parser->index, 10);
    }
    SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_NUMBER, &number, parser->line);
    SVAsm_tokenList_add(parser->tokens, token);
    parser->index = endIndex;
    return true;
}

bool SVAsm_parser_getRegister(SVAsm_Parser *parser)
{
    if(!parser)
        return false;
    size_t endIndex = parser->index + 1;
    while(SVAsm_parser_isCharacter(*(parser->source + endIndex)))
        endIndex++;
    char *tempStr = NULL;
    size_t len = endIndex - parser->index - 1;
    MMALLOC(tempStr, char, len + 1);
    strncpy(tempStr, parser->source + parser->index + 1, len);
    tempStr[len] = '\0';
    SVAsm_Register reg = SVAsm_register_parse(tempStr);
    FFREE(tempStr);
    if(reg != SVASM_REGISTER_NONE)
    {
        SVAsm_Token *token = SVAsm_token_create(SVASM_TOKEN_REGISTER, &reg, parser->line);
        SVAsm_tokenList_add(parser->tokens, token);
        parser->index = endIndex;
    }
    else
    {
        char *sourceLine = NULL;
        unsigned int column = SVAsm_error_getLine(parser->source, parser->index + 1, &sourceLine);
        SVAsm_error(parser->options, SVASM_ERROR, parser->filename, parser->filepath, parser->line, "invalid register");
        SVAsm_errorParser(parser->options, sourceLine, column);
        FFREE(sourceLine);
        return false;
    }
    return true;
}


bool SVAsm_parser_isWhitespace(char c)
{
    if(c == ' ' || c == '\t')
        return true;
    return false;
}

bool SVAsm_parser_isCharacter(char c)
{
    if(('A' <= c && c <= 'Z') ||
       ('a' <= c && c <= 'z') ||
       c == '_')
        return true;
    return false;
}

bool SVAsm_parser_isNumber(char c)
{
    if('0' <= c && c <= '9')
        return true;
    return false;
}

bool SVAsm_parser_isHexNumber(char c)
{
    if(('0' <= c && c <= '9') ||
       ('A' <= c && c <= 'F') ||
       ('a' <= c && c <= 'f'))
        return true;
    return false;
}

bool SVAsm_parser_isBinNumber(char c)
{
    if(c == '0' || c == '1')
        return true;
    return false;
}

unsigned int SVAsm_parser_parseNumber(char const *str, size_t length, unsigned int base)
{
    if(!str || !length || !base)
        return 0;
    unsigned int val = 0;
    unsigned int result = 0;
    unsigned int exp = 1;
    for(int i = length - 1; i >= 0; i--)
    {
        if('0' <= str[i] && str[i] <= '9')
            val = (unsigned int) str[i] - '0';
        else if('a' <= str[i] && str[i] <= 'z')
            val = (unsigned int) str[i] - 'a' + 10;
        else if('A' <= str[i] && str[i] <= 'Z')
            val = (unsigned int) str[i] - 'A' + 10;
        else
            return 0;
        if(val > base)
            return 0;
        result += exp * val;
        exp *= base;
    }
    return result;
}

char* SVAsm_parser_escapeString(char const *str)
{
    if(!str)
        return NULL;
    char *newString = NULL;
    MMALLOC(newString, char, strlen(str) + 1);
    size_t i = 0;
    size_t index = 0;
    while(str[i])
    {
        if(str[i] == '\\')
        {
            if(!str[i + 1])
                break;
            switch(str[i + 1])
            {
                case '\\':
                    newString[index] = '\\';
                    i++;
                    break;
                case '\"':
                    newString[index] = '\"';
                    i++;
                    break;
                case '\'':
                    newString[index] = '\'';
                    i++;
                    break;
                case 'n':
                    newString[index] = '\n';
                    i++;
                    break;
                case 'r':
                    newString[index] = '\r';
                    i++;
                    break;
                case 't':
                    newString[index] = '\t';
                    i++;
                    break;
                case 'x':
                    if(!str[i + 2] || !str[i + 3])
                        break;
                    if(SVAsm_parser_isHexNumber(str[i + 2]) && SVAsm_parser_isHexNumber(str[i + 3]))
                    {
                        unsigned int n = SVAsm_parser_parseNumber(str + i + 2, 2, 16);
                        newString[index] = n;
                        i += 3;
                    }
                    break;
                default:
                    break;
            }
        }
        else
            newString[index] = str[i];
        i++;
        index++;
    }
    newString[index] = str[i];
    return newString;
}
