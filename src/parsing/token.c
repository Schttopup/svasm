/**
 * \file token.c
 * \brief Defines functions related to parsing tokens.
 * \author Schttopup
 */

#include "token.h"
#include <sdefs.h>
#include <string.h>
#include <stdint.h>


char const SVAsm_separatorValues[SVASM_SEPARATOR_LAST] = {
    '\0',
    '.',
    ',',
    ':',
    '(',
    ')',
    '[',
    ']',
    '+',
    '-'
};

SVAsm_Token* SVAsm_token_create(SVAsm_TokenType type, void *value, unsigned int line)
{
    if(type > SVASM_TOKEN_LAST || type == SVASM_TOKEN_NONE)
        return NULL;
    SVAsm_Token *token = NULL;
    MMALLOC(token, SVAsm_Token, 1);
    token->type = type;
    token->line = line;
    switch(token->type)
    {
        case SVASM_TOKEN_SYMBOL:
        case SVASM_TOKEN_STRING:
        case SVASM_TOKEN_COMMENT:
            MMALLOC(token->value.text, char, strlen((char*)value) + 1);
            strcpy(token->value.text, (char*)value);
            break;
        case SVASM_TOKEN_NUMBER:
            token->value.number = *(int32_t*)value;
            break;
        case SVASM_TOKEN_SEPARATOR:
            token->value.separator = *(SVAsm_SeparatorType*)value;
            break;
        case SVASM_TOKEN_REGISTER:
            token->value.reg = *(SVAsm_Register*)value;
            break;
        case SVASM_TOKEN_ENDLINE:
            break;
        default:
            FFREE(token);
            return NULL;
    }
    return token;
}

void SVAsm_token_destroy(SVAsm_Token *token)
{
    if(!token)
        return;
    if(token->type == SVASM_TOKEN_SYMBOL ||
            token->type == SVASM_TOKEN_STRING ||
            token->type == SVASM_TOKEN_COMMENT)
        FFREE(token->value.text);
    FFREE(token);
}

SVAsm_SeparatorType SVAsm_token_getSeparator(char text)
{
    for(unsigned int i = 1; i < SVASM_SEPARATOR_LAST; i++)
    {
        if(text == SVAsm_separatorValues[i])
            return (SVAsm_SeparatorType)i;
    }
    return SVASM_SEPARATOR_NONE;
}

SVAsm_TokenList* SVAsm_tokenList_create(void)
{
    SVAsm_TokenList *list = NULL;
    MMALLOC(list, SVAsm_TokenList, 1);
    list->tokenCount = 0;
    list->tokens = NULL;
    return list;
}

void SVAsm_tokenList_destroy(SVAsm_TokenList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->tokenCount; i++)
        SVAsm_token_destroy(list->tokens[i]);
    FFREE(list->tokens);
    FFREE(list);
}

void SVAsm_tokenList_add(SVAsm_TokenList *list, SVAsm_Token *token)
{
    if(!list)
        return;
    RREALLOC(list->tokens, SVAsm_Token*, list->tokenCount + 1);
    list->tokens[list->tokenCount] = token;
    list->tokenCount++;
}

SVAsm_Token* SVAsm_tokenList_get(SVAsm_TokenList *list, size_t index)
{
    if(!list)
        return NULL;
    if(index >= list->tokenCount)
        return NULL;
    return list->tokens[index];
}
