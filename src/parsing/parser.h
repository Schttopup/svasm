/**
 * \file parser.h
 * \brief Declares structures and functions related to parsing.
 * \author Schttopup
 */

#ifndef SVASM_PARSER_H
#define SVASM_PARSER_H

#include "token.h"
#include "../option.h"
#include <stdbool.h>


/**
 * \brief Parser states.
 */
enum SVAsm_ParserState_
{
    SVASM_PSTATE_BLANK,         //!< White space
    SVASM_PSTATE_SEPARATOR,     //!< Separator
    SVASM_PSTATE_SYMBOL,        //!< Symbol
    SVASM_PSTATE_STRING,        //!< String
    SVASM_PSTATE_NUMBER,        //!< Number
    SVASM_PSTATE_REGISTER,      //!< Register
    SVASM_PSTATE_COMMENT        //!< Line comment
};

/// Type name for parser states.
typedef enum SVAsm_ParserState_ SVAsm_ParserState;

/**
 * \brief Parser structure.
 * Contains data for parsing.
 */
struct SVAsm_Parser_
{
    SVAsm_Options *options;
    char const *source;         //!< Source code
    char const *filename;       //!< Source file name
    char const *filepath;       //!< Source file path
    size_t index;         //!< Character index
    unsigned int line;          //!< Line number
    SVAsm_TokenList *tokens;    //!< Token list
};

/// Type name for parser structure.
typedef struct SVAsm_Parser_ SVAsm_Parser;

/**
 * \brief Parses a source and builds the token list.
 * \param source The source to parse
 * \return The built token list
 */
SVAsm_TokenList* SVAsm_parser_parse(char const *source, char const *filename, char const *filepath, SVAsm_Options *options);

/**
 * \brief Parses a separator from a parser.
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getSeparator(SVAsm_Parser *parser);

/**
 * \brief Parses a text from a parser
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getSymbol(SVAsm_Parser *parser);

/**
 * \brief Parses a string from a parser
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getString(SVAsm_Parser *parser);

/**
 * \brief Parses a comment from a parser
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getComment(SVAsm_Parser *parser);

/**
 * \brief Parses a number from a parser
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getNumber(SVAsm_Parser *parser);

/**
 * \brief Parses a register from a parser
 * \param parser The parser
 * \return true if no error, false otherwise
 */
bool SVAsm_parser_getRegister(SVAsm_Parser *parser);

/**
 * \brief Checks if a character is considered whitespace.
 * \param c The character to check
 * \return true if the character is whitespace, false otherwise
 */
bool SVAsm_parser_isWhitespace(char c);

/**
 * Checks if a character is alphabetic or underscore.
 * \param c The character to check
 * \return true if the character is alphabetic or underscore, false otherwise
 */
bool SVAsm_parser_isCharacter(char c);

/**
 * \brief Checks if a character is a decimal digit.
 * \param c The character to check
 * \return true if the character is a deimal digit, false otherwise
 */
bool SVAsm_parser_isNumber(char c);

/**
 * \brief Checks if a character is a a hexadecimal digit, false otherwise
 * \param c The character to check
 * \return true if the character is a hexadecimal digit, false otherwise
 */
bool SVAsm_parser_isHexNumber(char c);

/**
 * \brief Checks if a character is a binary digit.
 * \param c The character to check
 * \return true if the character is a binary digit, false otherwise
 */
bool SVAsm_parser_isBinNumber(char c);

/**
 * \brief Parses a string to a number in a given base.
 * \param str The string to parse
 * \param length The length of the string
 * \param base The base of the number
 * \return The parsed number, or 0 if invalid character.
 */
unsigned int SVAsm_parser_parseNumber(char const *str, size_t length, unsigned int base);

/**
 * Converts escape codes in a string to special characters.
 * \param str The string to escape
 * \return The escaped string
 */
char* SVAsm_parser_escapeString(char const *str);


#endif //SVASM_PARSER_H
