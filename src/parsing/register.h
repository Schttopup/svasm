/**
 * \file register.h
 * \brief Declares structures and functions related to processor registers.
 * \author Schttopup
 */

#ifndef SVASM_REGISTER_H
#define SVASM_REGISTER_H


/**
 * \brief Processor registers.
 */
enum SVAsm_Register_
{
    SVASM_REGISTER_NONE,
    SVASM_REGISTER_A,
    SVASM_REGISTER_B,
    SVASM_REGISTER_C,
    SVASM_REGISTER_D,
    SVASM_REGISTER_W,
    SVASM_REGISTER_X,
    SVASM_REGISTER_Y,
    SVASM_REGISTER_Z,
    SVASM_REGISTER_AL,
    SVASM_REGISTER_AH,
    SVASM_REGISTER_BL,
    SVASM_REGISTER_BH,
    SVASM_REGISTER_CL,
    SVASM_REGISTER_CH,
    SVASM_REGISTER_DL,
    SVASM_REGISTER_DH,
    SVASM_REGISTER_WL,
    SVASM_REGISTER_WH,
    SVASM_REGISTER_XL,
    SVASM_REGISTER_XH,
    SVASM_REGISTER_YL,
    SVASM_REGISTER_YH,
    SVASM_REGISTER_ZL,
    SVASM_REGISTER_ZH,
    SVASM_REGISTER_PC,
    SVASM_REGISTER_SP,
    SVASM_REGISTER_LAST
};

/// Type names for processor registers.
typedef enum SVAsm_Register_ SVAsm_Register;

/// String values for registers.
extern char const * const SVAsm_registerValues[SVASM_REGISTER_LAST];


/**
 * \brief Associates a register value to a string.
 * \param text The string
 * \return The register
 */
SVAsm_Register SVAsm_register_parse(char const *text);


#endif //SVASM_REGISTER_H
