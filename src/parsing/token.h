/**
 * \file token.h
 * \brief Declares structures and functions related to parsing tokens.
 * \author Schttopup
 */

#ifndef SVASM_TOKEN_H
#define SVASM_TOKEN_H

#include "register.h"
#include <stdint.h>


typedef int64_t SVAsm_NumberType;


/**
 * \brief Token types.
 */
enum SVAsm_TokenType_
{
    SVASM_TOKEN_NONE,
    SVASM_TOKEN_ENDLINE,
    SVASM_TOKEN_SYMBOL,
    SVASM_TOKEN_SEPARATOR,
    SVASM_TOKEN_STRING,
    SVASM_TOKEN_NUMBER,
    SVASM_TOKEN_REGISTER,
    SVASM_TOKEN_COMMENT,
    SVASM_TOKEN_LAST
};

/// Type name for token types.
typedef enum SVAsm_TokenType_ SVAsm_TokenType;


/**
 * \brief Separator token types.
 */
enum SVAsm_SeparatorType_
{
    SVASM_SEPARATOR_NONE,
    SVASM_SEPARATOR_DOT,
    SVASM_SEPARATOR_COMMA,
    SVASM_SEPARATOR_COLON,
    SVASM_SEPARATOR_LPARENTHESIS,
    SVASM_SEPARATOR_RPARENTHESIS,
    SVASM_SEPARATOR_LBRACKET,
    SVASM_SEPARATOR_RBRACKET,
    SVASM_SEPARATOR_PLUS,
    SVASM_SEPARATOR_MINUS,
    SVASM_SEPARATOR_LAST
};

/// Type name for separator types.
typedef enum SVAsm_SeparatorType_ SVAsm_SeparatorType;

/// Character values for separators.
extern char const SVAsm_separatorValues[SVASM_SEPARATOR_LAST];


/**
 * \brief Parsing token.
 */
struct SVAsm_Token_
{
    SVAsm_TokenType type;                //!< Token type
    union SVAsm_TokenValue_                   //!< Token value
    {
        char *text;                      //!< Text value
        SVAsm_SeparatorType separator;   //!< Separator value
        SVAsm_NumberType number;         //!< Numeral value
        SVAsm_Register reg;              //!< Register value
    } value;
    unsigned int line;                   //!< Line number
};

/// Type name for parsing token.
typedef struct SVAsm_Token_ SVAsm_Token;


/**
 * \brief Token list.
 */
struct SVAsm_TokenList_
{
    SVAsm_Token **tokens;
    size_t tokenCount;
};

/// Type name for token list.
typedef struct SVAsm_TokenList_ SVAsm_TokenList;


/**
 * \brief Creates a token.
 * \param type The token type
 * \param value The token value
 * \param line The line number
 * \return The created token
 */
SVAsm_Token* SVAsm_token_create(SVAsm_TokenType type, void *value, unsigned int line);

/**
 * \brief Destroys a token.
 * \param token The token to destroy
 */
void SVAsm_token_destroy(SVAsm_Token *token);

/**
 * \brief Associates a separator value to a character.
 * \param text The character
 * \return The separator
 */
SVAsm_SeparatorType SVAsm_token_getSeparator(char text);

/**
 * \brief Creates a token list.
 * \return The created token list
 */
SVAsm_TokenList* SVAsm_tokenList_create(void);

/**
 * \brief Destroys a token list.
 * \param list The list to destroy
 */
void SVAsm_tokenList_destroy(SVAsm_TokenList *list);

/**
 * \brief Adds a token to a token list.
 * \param list The token list
 * \param token The token to add
 */
void SVAsm_tokenList_add(SVAsm_TokenList *list, SVAsm_Token *token);

/**
 * \brief Gets a token from a token list.
 * \param list The token list
 * \param index The index of the token
 * \return The requested token, or NULL if invlid index
 */
SVAsm_Token* SVAsm_tokenList_get(SVAsm_TokenList *list, size_t index);


#endif //SVASM_TOKEN_H
