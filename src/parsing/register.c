/**
 * \file register.c
 * \brief Defines functions related to processor registers.
 * \author Schttopup
 */

#include "register.h"
#include <string.h>


char const * const SVAsm_registerValues[SVASM_REGISTER_LAST] = {
        "\0",
        "a",
        "b",
        "c",
        "d",
        "w",
        "x",
        "y",
        "z",
        "al",
        "ah",
        "bl",
        "bh",
        "cl",
        "ch",
        "dl",
        "dh",
        "wl",
        "wh",
        "xl",
        "xh",
        "yl",
        "yh",
        "zl",
        "zh",
        "pc",
        "sp"
};


SVAsm_Register SVAsm_register_parse(char const *text)
{
    for(unsigned int i = 1; i < SVASM_REGISTER_LAST; i++)
    {
        if(strcmp(text, SVAsm_registerValues[i]) == 0)
            return (SVAsm_Register)i;
    }
    return SVASM_REGISTER_NONE;
}
