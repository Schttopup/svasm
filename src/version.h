/**
 * \file version.h
 * \brief Defines macros for version strings.
 * \author Schttopup
 */

#ifndef SVASM_VERSION_H
#define SVASM_VERSION_H


#define SVASM_PROGRAM "svasm"
#define SVASM_VERSION "0.1"


#endif //SVASM_VERSION_H
