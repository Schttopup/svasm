/**
 * \file option.h
 * \brief Declares structures and functions related to program options.
 * \author Schttopup
 */

#ifndef SVASM_OPTION_H
#define SVASM_OPTION_H

#include "source.h"
#include <stdint.h>
#include <stdbool.h>


#define SVASM_OPTIONS_WARNING_COUNT 9
extern char const * const SVAsm_options_warningNames[SVASM_OPTIONS_WARNING_COUNT];
extern int const SVAsm_options_warningOffsets[SVASM_OPTIONS_WARNING_COUNT];


/**
 * Program options.
 */
struct SVAsm_Options_
{
    uint16_t varDataStart;              //!< Start address of the variable segment
    char *inputFileName;                //!< File name of the assembly source file
    char *outputFileName;               //!< File name of the binary output file
    bool outputListing;                 //!< Listing file output
    bool outputColors;                  //!< Colors in console output
    bool fullPaths;                     //!< Full file paths in console output
    bool shortErrors;                   //!< No stack trace in error output
    char **definedSymbols;              //!< List of command line-defined symbols
    size_t definedSymbolCount;          //!< Number of command line-defined symbols
    bool warningError;                  //!< All warnings issue errors
    bool fatalError;                    //!< All errors are fatal
    struct SVAsm_WarningOptions_
    {
        bool mathWarning;               //!< Invalid math issues a warning
        bool mathError;                 //!< Invalid math warnings are errors
        bool unsetLabels;               //!< Labels called but not defined
        bool defaultValuesError;        //!< Default value substitution is an error
        bool shadowVariables;           //!< Shadowed builtin variables & macros TODO
        bool undefinedDelete;           //!< Undefined macro .delete TODO
        bool registerOverflowError;     //!< Register overflow is an error
    } warnings;
    SVAsm_Source *source;               //!< File source
    unsigned int maxIncludeLevels;      //!< Maximum depth for file inclusion
    unsigned int maxMacroDepth;         //!< Maximum recursion in macro expansion
    unsigned int maxTranslatorDepth;    //!< Maximum recursion in function macros
};

/// Type name for program options.
typedef struct SVAsm_Options_ SVAsm_Options;


/**
 * Creates program options.
 * \return The created program options
 */
SVAsm_Options* SVAsm_options_create(void);

/**
 * Destroy program options.
 * \param options The program options to destroy
 */
void SVAsm_options_destroy(SVAsm_Options *options);


#endif //SVASM_OPTION_H
