/**
 * \file error.h
 * \brief Declares structures and functions related to error management.
 * \author Schttopup
 */

#ifndef SVASM_ERROR_H
#define SVASM_ERROR_H


#include "option.h"
#include "building/block.h"
#include "translation/macrocallstack.h"
#include <stdarg.h>
#include <stdbool.h>


/**
 * Error level.
 */
enum SVAsm_ErrorType_
{
    SVASM_BLANK,    //!< No level
    SVASM_INFO,     //!< Information message
    SVASM_WARNING,  //!< Warning message
    SVASM_ERROR,    //!< Error message
    SVASM_FATAL     //!< Fatal error message
};

/// Type name for error levels.
typedef enum SVAsm_ErrorType_ SVAsm_ErrorType;


/**
 * Prints a message in the console.
 * \param options The program options
 * \param type The error level
 * \param format The printf-like format string
 * \param ... The printf-like format parameters
 */
void SVAsm_message(SVAsm_Options *options, SVAsm_ErrorType type, char const *format, ...);

/**
 * Prints an error message in the console.
 * \param options The program options
 * \param type The error level
 * \param filename The file name of the error
 * \param filepath The file path of the error
 * \param line The line of the error
 * \param format The printf-like format string
 * \param ... The printf-like format parameters
 */
void SVAsm_error(SVAsm_Options *options, SVAsm_ErrorType type, char const *filename, char const *filepath, unsigned int line, char const *format, ...);

/**
 * Prints details of a parsing error in the console.
 * \param options The program options
 * \param source The source line
 * \param column The character column
 */
void SVAsm_errorParser(SVAsm_Options *options, char const *source, size_t column);

/**
 * Prints details of a building error in the console.
 * \param options The program options
 * \param tokens The tokenized source file
 * \param index The token index
 */
void SVAsm_errorBuilder(SVAsm_Options *options, SVAsm_TokenList *tokens, size_t index);

/**
 * Prints details of a translation error in the console.
 * \param options The program options
 * \param translator The translator in use
 * \param block The affected block
 * \param index The index of the error in the block
 */
void SVAsm_errorTranslator(SVAsm_Options *options, struct SVAsm_Translator_ *translator, SVAsm_Block *block, size_t index);

/**
 * Prints details of a macro call error in the console.
 * \param options The program options
 * \param macro The affected macro call
 * \param index The index of the error in the macro call
 */
void SVAsm_errorMacro(SVAsm_Options *options, SVAsm_MacroCall *macro, size_t index);

/**
 * Prints a macro call stack trace in the console.
 * \param options The program options
 * \param stack The macro call stack
 */
void SVAsm_errorMacroStack(SVAsm_Options *options, SVAsm_MacroCallStack *stack);

/**
 * Prints details of an array error in the console.
 * \param options The program options
 * \param array The affected array
 * \param index The index of the error in the array
 */
void SVAsm_errorArray(SVAsm_Options *options, SVAsm_Array *array, size_t index);

/**
 * Prints details of a label error in the console.
 * \param options The program options
 * \param label The affected label
 */
void SVAsm_errorLabel(SVAsm_Options *options, SVAsm_LabelCall *label);

/**
 * Extracts a line from the source file corresponding to a character.
 * \param source The source file
 * \param index The character index
 * \param sourceLine The extracted source line. It must be freed by the user.
 * \return The column of the affected character in the source line
 */
size_t SVAsm_error_getLine(char const *source, size_t index, char **sourceLine);

/**
 * Gets the length of a macro call in characters.
 * \param macro The macro call
 * \return The length in characters
 */
size_t SVAsm_error_macroSize(SVAsm_MacroCall *macro);

/**
 * Gets the length of an array in characters.
 * \param array The array
 * \return The length in characters
 */
size_t SVAsm_error_arraySize(SVAsm_Array *array);

/**
 * Gets the full index of a block parameter for use in SVAsm_errorTranslator.
 * The full index includes recursive parameters of macro calls, arrays and labels.
 * \param block The block to get the index from
 * \param index The index of the parameter
 * \return The full index
 */
size_t SVAsm_error_getParam(SVAsm_Block *block, size_t index);

/**
 * Gets the full index of a macro parameter for use in SVAsm_errorTranslator.
 * The full index includes recursive parameters of macro calls, arrays and labels.
 * \param macro The macro call to get the index from
 * \param index The index of the parameter
 * \return The full index
 */
size_t SVAsm_error_getMacroParam(SVAsm_MacroCall *macro, size_t index);

/**
 * Gets the full index of an array parameter for use in SVAsm_errorTranslator.
 * The full index includes recursive parameters of macro calls, arrays and labels.
 * \param array The array to get the index from
 * \param index The index of the parameter
 * \return The full index
 */
size_t SVAsm_error_getArrayParam(SVAsm_Array *array, size_t index);


#endif //SVASM_ERROR_H
