/**
 * \file translator_var.c
 * \brief Defines functions related to svasm variable macros.
 * \author Schttopup
 */

#include "translator_var.h"
#include "translator.h"
#include "../version.h"
#include <time.h>
#include <string.h>
#include <stdio.h>


SVAsm_BuiltinVariableTranslation const SVAsm_BuiltinVariableTranslations[SVASM_BUILTINVAR_LAST] = {
    NULL,
    SVAsm_translator_builtinAddress,
    SVAsm_translator_builtinVarStart,
    SVAsm_translator_builtinVarSize,
    SVAsm_translator_builtinConstSize,
    SVAsm_translator_builtinStackStart,
    SVAsm_translator_builtinStackSize,
    SVAsm_translator_builtinCounter,
    SVAsm_translator_builtinNil,
    SVAsm_translator_builtinFile,
    SVAsm_translator_builtinPath,
    SVAsm_translator_builtinBaseFile,
    SVAsm_translator_builtinBasePath,
    SVAsm_translator_builtinLine,
    SVAsm_translator_builtinLevel,
    SVAsm_translator_builtinTime,
    SVAsm_translator_builtinAsmPrg,
    SVAsm_translator_builtinAsmVer
};


SVAsm_BuiltinVariable SVAsm_translator_getBuiltinVariable(char const *symbol)
{
    if(!symbol)
        return SVASM_BUILTINVAR_NONE;
    for(size_t i = 0; i < SVASM_BUILTINVAR_LAST; i++)
    {
        if(strcmp(symbol, SVAsm_BuiltinVariableNames[i]) == 0)
            return (SVAsm_BuiltinVariable)i;
    }
    return SVASM_BUILTINVAR_NONE;
}


SVAsm_Arg* SVAsm_translator_builtinAddress(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType address = translator->location;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &address);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinVarStart(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType varStart = translator->memory->varDataStart;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &varStart);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinVarSize(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType varSize = translator->memory->varDataSize;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &varSize);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinConstSize(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType constSize = translator->memory->constDataSize;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &constSize);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinStackStart(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType stackStart = translator->memory->stackStart;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &stackStart);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinStackSize(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_NumberType stackSize = translator->memory->stackSize;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &stackSize);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinCounter(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &(translator->counter));
    translator->counter++;
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinNil(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NONE, NULL);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinFile(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    char const *filename = translator->blocks->blocks[index]->filename;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, filename);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinPath(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    char const *filepath = translator->blocks->blocks[index]->filepath;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, filepath);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinBaseFile(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    char const *baseFilename = translator->baseFilename;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, baseFilename);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinBasePath(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    char const *baseFilepath = translator->baseFilepath;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, baseFilepath);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinLine(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_NumberType line = translator->blocks->blocks[index]->line;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &line);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinLevel(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_NumberType level = translator->blocks->blocks[index]->level;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &level);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinTime(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    time_t timeNow = time(NULL);
    struct tm dateLocal = *localtime(&timeNow);
    struct tm dateUtc = *gmtime(&timeNow);
    char buf[SVASM_TIME_MAX_LENGTH];
    strftime(buf, SVASM_TIME_MAX_LENGTH, "%Y-%m-%dT%H:%M:%S", &dateLocal);
    int hours = dateLocal.tm_hour - dateUtc.tm_hour;
    int minutes = dateLocal.tm_min - dateUtc.tm_min;
    if(dateLocal.tm_yday != dateUtc.tm_yday)
        hours += 24 * (dateLocal.tm_yday != dateUtc.tm_yday);
    if(minutes < 0)
    {
        minutes += 60;
        hours -= 1;
    }
    sprintf(buf + strlen(buf), "%+.2d:%.2d", hours, minutes);
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, buf);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinAsmPrg(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    char *asm_program = SVASM_PROGRAM;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, asm_program);
    return arg;
}

SVAsm_Arg* SVAsm_translator_builtinAsmVer(struct SVAsm_Translator_ *translator)
{
    if(!translator)
        return NULL;
    char *asm_version = SVASM_VERSION;
    SVAsm_Arg *arg = SVAsm_arg_create(SVASM_ARG_STRING, asm_version);
    return arg;
}
