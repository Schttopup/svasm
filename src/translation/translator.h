/**
 * \file translator.h
 * \brief Declares structures and functions related to code translation.
 * \author Schttopup
 */

#ifndef SVASM_TRANSLATOR_H
#define SVASM_TRANSLATOR_H

#include "macro.h"
#include "stack.h"
#include "../building/block.h"
#include "../assembly/label.h"
#include "../assembly/memory.h"
#include "../building/instruction.h"
#include "../assembly/export.h"
#include "../assembly/metadata.h"
#include "../source.h"
#include "../option.h"


/**
 * Code translator.
 */
struct SVAsm_Translator_
{
    SVAsm_BlockList *blocks;        //!< Block list
    SVAsm_LabelList *labels;        //!< Label list
    SVAsm_MemoryObject *memory;     //!< Memory representation
    uint16_t location;              //!< Active memory location
    SVAsm_ExportList *exports;      //!< Memory export list
    SVAsm_Metadata *metadata;       //!< Program metadata
    SVAsm_MacroList *macros;        //!< Macro list
    SVAsm_Stack *stack;             //!< Preprocessor stack
    bool stop;                      //!< Stopping condition
    bool error;                     //!< Error level
    bool warning;                   //!< Warning level
    SVAsm_Source *source;           //!< Data source
    SVAsm_Options *options;         //!< Program options
    char const *baseFilename;       //!< Input source file name
    char const *baseFilepath;       //!< Inpur source file path
    SVAsm_NumberType counter;       //!< _counter built-in macro value
    bool nested;                    //!< Nested translator
    SVAsm_Arg *returnValue;         //!< Nested translator return value
};

/// Type name for code translators.
typedef struct SVAsm_Translator_ SVAsm_Translator;


/**
 * \brief Creates a new translator.
 * \param blocks The block list to use
 * \param memory The memory object to use
 * \return The created translator
 */
SVAsm_Translator* SVAsm_translator_create(SVAsm_BlockList *blocks, SVAsm_Options *options);

/**
 * \brief Destroys a translator.
 * \param translator The translator to destroy
 */
void SVAsm_translator_destroy(SVAsm_Translator *translator);

/**
 * Creates a nested translator.
 * \param translator The translator to copy
 * \return The nested translator
 */
SVAsm_Translator* SVAsm_translator_createNested(SVAsm_Translator *translator);

/**
 * Sets a data source for the translator.
 * Data source memory management (destruction) is to be taken care of by the user.
 * \param translator The translator
 * \param source The data source to use
 */
void SVAsm_translator_setSource(SVAsm_Translator *translator, SVAsm_Source *source);


/**
 * Performs the translation from a block list.
 * \param translator The translator to use
 */
void SVAsm_translator_translate(SVAsm_Translator *translator);

/**
 * \brief Translates a label.
 * \param translator The translator.
 * \return true if the translation was successful, false otherwise.
 */
bool SVAsm_translator_translateLabel(SVAsm_Translator *translator);

/**
 * \brief Translates an instruction.
 * \param translator The translator.
 * \return true if the translation was successful, false otherwise.
 */
bool SVAsm_translator_translateInstruction(SVAsm_Translator *translator);

/**
 * \brief Translates a command.
 * \param translator The translator.
 * \return true if the translation was successful, false otherwise.
 */
bool SVAsm_translator_translateCommand(SVAsm_Translator *translator);

/**
 * \brief Translates a macro call.
 * \param translator The translator.
 * \return true if the translation was successful, false otherwise.
 */
bool SVAsm_translator_translateMacroCall(SVAsm_Translator *translator);


#endif //SVASM_TRANSLATOR_H
