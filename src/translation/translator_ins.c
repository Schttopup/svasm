/**
 * \file translator_ins.c
 * \brief Defines functions related to code translation for instructions.
 * \author Schttopup
 */

#include "translator_ins.h"
#include "translator.h"
#include "../error.h"
#include "macro_defs.h"
#include <stdbool.h>


SVAsm_InstructionTranslation const SVAsm_instructionTranslations[] = {
        {.keyword = SVASM_KEYWORD_NOP, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOp, .opcode = 0x00},
        {.keyword = SVASM_KEYWORD_LDI, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NUMBER}, .function = SVAsm_translator_translateInsLDI, .opcode = 0x02},
        {.keyword = SVASM_KEYWORD_LDI, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_LABEL}, .function = SVAsm_translator_translateInsLDI, .opcode = 0x02},
        {.keyword = SVASM_KEYWORD_LD, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NUMBER}, .function = SVAsm_translator_translateInsOpRA, .opcode = 0x04},
        {.keyword = SVASM_KEYWORD_LD, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_LABEL}, .function = SVAsm_translator_translateInsOpRA, .opcode = 0x04},
        {.keyword = SVASM_KEYWORD_LD, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsLdRR, .opcode = 0x06},
        {.keyword = SVASM_KEYWORD_LDA, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsLdRR, .opcode = 0x08},
        {.keyword = SVASM_KEYWORD_ST, .operandCount = 2, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpAR, .opcode = 0x0A},
        {.keyword = SVASM_KEYWORD_ST, .operandCount = 2, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpAR, .opcode = 0x0A},
        {.keyword = SVASM_KEYWORD_ST, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsStRR, .opcode = 0x0C},
        {.keyword = SVASM_KEYWORD_STA, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsStRR, .opcode = 0x0E},
        {.keyword = SVASM_KEYWORD_MV, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsMV, .opcode = 0x10},

        {.keyword = SVASM_KEYWORD_NOT, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x20},
        {.keyword = SVASM_KEYWORD_OR, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x22},
        {.keyword = SVASM_KEYWORD_AND, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x24},
        {.keyword = SVASM_KEYWORD_XOR, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x26},
        {.keyword = SVASM_KEYWORD_SHL, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x28},
        {.keyword = SVASM_KEYWORD_SHR, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x2A},
        {.keyword = SVASM_KEYWORD_ROL, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x2C},
        {.keyword = SVASM_KEYWORD_ROR, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x2E},
        {.keyword = SVASM_KEYWORD_ADD, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x30},
        {.keyword = SVASM_KEYWORD_SUB, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x32},
        {.keyword = SVASM_KEYWORD_ADDC, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x34},
        {.keyword = SVASM_KEYWORD_SUBC, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x36},
        {.keyword = SVASM_KEYWORD_SC, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOp, .opcode = 0x38},
        {.keyword = SVASM_KEYWORD_CC, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOp, .opcode = 0x39},
        {.keyword = SVASM_KEYWORD_INC, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x3A},
        {.keyword = SVASM_KEYWORD_DEC, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x3C},
        {.keyword = SVASM_KEYWORD_PUSH, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x40},
        {.keyword = SVASM_KEYWORD_POP, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOpR, .opcode = 0x42},

        {.keyword = SVASM_KEYWORD_JMP, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x60},
        {.keyword = SVASM_KEYWORD_JMP, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x60},
        {.keyword = SVASM_KEYWORD_JMP, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x61},
        {.keyword = SVASM_KEYWORD_JZ, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x62},
        {.keyword = SVASM_KEYWORD_JZ, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x62},
        {.keyword = SVASM_KEYWORD_JZ, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x63},
        {.keyword = SVASM_KEYWORD_JNZ, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x64},
        {.keyword = SVASM_KEYWORD_JNZ, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x64},
        {.keyword = SVASM_KEYWORD_JNZ, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x65},
        {.keyword = SVASM_KEYWORD_JC, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x66},
        {.keyword = SVASM_KEYWORD_JC, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x66},
        {.keyword = SVASM_KEYWORD_JC, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x67},
        {.keyword = SVASM_KEYWORD_JNC, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x68},
        {.keyword = SVASM_KEYWORD_JNC, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x68},
        {.keyword = SVASM_KEYWORD_JNC, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x69},
        {.keyword = SVASM_KEYWORD_JT, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x6A},
        {.keyword = SVASM_KEYWORD_JT, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x6A},
        {.keyword = SVASM_KEYWORD_JT, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x6B},
        {.keyword = SVASM_KEYWORD_JNT, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x6C},
        {.keyword = SVASM_KEYWORD_JNT, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x6C},
        {.keyword = SVASM_KEYWORD_JNT, .operandCount = 1, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpR, .opcode = 0x6D},
        {.keyword = SVASM_KEYWORD_CALL, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x70},
        {.keyword = SVASM_KEYWORD_CALL, .operandCount = 1, .operands = {SVASM_OPERAND_LABEL, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsJumpA, .opcode = 0x70},
        {.keyword = SVASM_KEYWORD_RET, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOp, .opcode = 0x71},
        {.keyword = SVASM_KEYWORD_BRK, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsOp, .opcode = 0x72},
        {.keyword = SVASM_KEYWORD_SYS, .operandCount = 1, .operands = {SVASM_OPERAND_NUMBER, SVASM_OPERAND_NONE}, .function = SVAsm_translator_translateInsSYS, .opcode = 0x73},

        {.keyword = SVASM_KEYWORD_CEQ, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x74},
        {.keyword = SVASM_KEYWORD_CNEQ, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x76},
        {.keyword = SVASM_KEYWORD_CLT, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x78},
        {.keyword = SVASM_KEYWORD_CNLT, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x7A},
        {.keyword = SVASM_KEYWORD_CGT, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x7C},
        {.keyword = SVASM_KEYWORD_CNGT, .operandCount = 2, .operands = {SVASM_OPERAND_REGISTER, SVASM_OPERAND_REGISTER}, .function = SVAsm_translator_translateInsOpRR, .opcode = 0x7E},

        {.keyword = SVASM_KEYWORD_LAST, .operandCount = 0, .operands = {SVASM_OPERAND_NONE, SVASM_OPERAND_NONE}, .function = NULL, 0x00}
};


static bool SVAsm_translator_registerLabel(struct SVAsm_Translator_ *translator, SVAsm_LabelCall *labelCall, uint16_t location)
{
    if(!translator || !labelCall)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_NumberType offset = 0;
    if(labelCall->offset)
    {
        if(labelCall->offset->type != SVASM_ARG_NUMBER)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "label offset must be a number");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_ERROR();
        }
        offset = labelCall->offset->value.number;
        if(offset <= -0x10000 || offset >= 0x10000)
        {
            SVAsm_error(translator->options, translator->options->warnings.defaultValuesError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "label offset out of range (%s0x%04X), defaulting to 0", (offset < 0 ? "-" : ""), (offset < 0 ? -offset : offset));
            offset = 0;
            SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.defaultValuesError);
        }
    }
    SVAsm_Label *label = SVAsm_labelList_get(translator->labels, labelCall->label);
    if(!label)
    {
        label = SVAsm_label_create(labelCall->label);
        SVAsm_labelList_add(translator->labels, label);
    }
    if(labelCall->negative)
        offset = -offset;
    SVAsm_label_addCall(label, (uint16_t)location, (int32_t)offset);
    return true;
}


bool SVAsm_translator_translateInsOpRA(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 4))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[4] = {0};
    SVAsm_Register reg = instruction->operands[0]->value.reg;
    uint8_t regBits;
    if(SVASM_REGISTER_AL <= reg && reg <= SVASM_REGISTER_ZH)
    {
        bytes[0] = (uint8_t)(opcode);
        regBits = reg - SVASM_REGISTER_AL;
        bytes[3] = regBits;
    }
    else if(SVASM_REGISTER_A <= reg && reg <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode + 1);
        regBits = (reg - SVASM_REGISTER_A) << 1;
        bytes[3] = regBits;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    if(instruction->operands[1]->type == SVASM_OPERAND_LABEL)
    {
        bool labelOk = SVAsm_translator_registerLabel(translator, instruction->operands[1]->value.label,
                                                     translator->location + 1);
        if(!labelOk)
            return false;
    }
    else
    {
        SVAsm_NumberType number = instruction->operands[1]->value.number;
        if(number > 0xFFFF)
        {
            SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "value %d overflows address, truncating to %d", number, number & 0xFFFF);
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
            SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
        }
        bytes[1] = (uint8_t)(number >> 8 & 0xFF);
        bytes[2] = (uint8_t)(number & 0xFF);
    }
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 4);
    translator->location += 4;
    return true;
}

bool SVAsm_translator_translateInsOpAR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    uint8_t bytes[4] = {0};
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 4))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    if(instruction->operands[0]->type == SVASM_OPERAND_LABEL)
    {
        bool labelOk = SVAsm_translator_registerLabel(translator, instruction->operands[0]->value.label,
                                                      translator->location + 1);
        if(!labelOk)
            return false;
    }
    else
    {
        SVAsm_NumberType number = instruction->operands[0]->value.number;
        if(number > 0xFFFF)
        {
            SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "value %d overflows address, truncating to %d", number, number & 0xFFFF);
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
        }
        bytes[1] = (uint8_t)(number >> 8 & 0xFF);
        bytes[2] = (uint8_t)(number & 0xFF);
    }
    SVAsm_Register reg = instruction->operands[1]->value.reg;
    uint8_t regBits;
    if(SVASM_REGISTER_AL <= reg && reg <= SVASM_REGISTER_ZH)
    {
        bytes[0] = (uint8_t)(opcode);
        regBits = reg - SVASM_REGISTER_AL;
        bytes[3] = regBits;
    }
    else if(SVASM_REGISTER_A <= reg && reg <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode + 1);
        regBits = (reg - SVASM_REGISTER_A) << 1;
        bytes[3] = regBits;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 4);
    translator->location += 4;
    return true;
}

bool SVAsm_translator_translateInsOpRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg1 = instruction->operands[0]->value.reg;
    SVAsm_Register reg2 = instruction->operands[1]->value.reg;
    uint8_t reg1Bits;
    uint8_t reg2Bits;
    if((SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z) &&
       (SVASM_REGISTER_A <= reg2 && reg2 <= SVASM_REGISTER_Z))
    {
        bytes[0] = (uint8_t)(opcode + 1);
        reg1Bits = (reg1 - SVASM_REGISTER_A) << 1;
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else if((SVASM_REGISTER_AL <= reg1 && reg1 <= SVASM_REGISTER_ZH) &&
            (SVASM_REGISTER_AL <= reg2 && reg2 <= SVASM_REGISTER_ZH))
    {
        bytes[0] = (uint8_t)(opcode);
        reg1Bits = reg1 - SVASM_REGISTER_AL;
        reg2Bits = reg2 - SVASM_REGISTER_AL;
    }
    else
    {
        if(reg1 == SVASM_REGISTER_PC || reg1 == SVASM_REGISTER_SP)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        }
        else if(reg2 == SVASM_REGISTER_PC || reg2 == SVASM_REGISTER_SP)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
        }
        else
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "mismatched registers");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], 0);
        }
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(reg2Bits << 4 | reg1Bits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsOpR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg = instruction->operands[0]->value.reg;
    uint8_t regBits;
    if(SVASM_REGISTER_A <= reg && reg <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode + 1);
        regBits = (reg - SVASM_REGISTER_A) << 1;
    }
    else if(SVASM_REGISTER_AL <= reg && reg <= SVASM_REGISTER_ZH)
    {
        bytes[0] = (uint8_t)(opcode);
        regBits = reg - SVASM_REGISTER_AL;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(regBits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsOp(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 1))
    {
        SVASM_TRANSLATOR_INVALIDWRITE()
        return false;
    }
    uint8_t bytes = (uint8_t)(opcode);
    SVAsm_memoryObject_fill(translator->memory, translator->location, &bytes, 1);
    translator->location += 1;
    return true;
}

bool SVAsm_translator_translateInsLdRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg1 = instruction->operands[0]->value.reg;
    SVAsm_Register reg2 = instruction->operands[1]->value.reg;
    uint8_t reg1Bits;
    uint8_t reg2Bits;
    if(SVASM_REGISTER_AL <= reg1 && reg1 <= SVASM_REGISTER_ZH)
    {
        bytes[0] = (uint8_t)(opcode);
        reg1Bits = reg1 - SVASM_REGISTER_AL;
    }
    else if(SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode + 1);
        reg1Bits = (reg1 - SVASM_REGISTER_A) << 1;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    if(SVASM_REGISTER_A <= reg2 && reg2 <= SVASM_REGISTER_Z)
    {
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(reg2Bits << 4 | reg1Bits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsStRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg1 = instruction->operands[0]->value.reg;
    SVAsm_Register reg2 = instruction->operands[1]->value.reg;
    uint8_t reg1Bits;
    uint8_t reg2Bits;
    if(SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode);
        reg1Bits = (reg1 - SVASM_REGISTER_AL) << 1;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    if(SVASM_REGISTER_AL <= reg2 && reg2 <= SVASM_REGISTER_ZH)
    {
        bytes[0] = (uint8_t)(opcode);
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else if(SVASM_REGISTER_A <= reg2 && reg2 <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode + 1);
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(reg2Bits << 4 | reg1Bits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsJumpA(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 3))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[3] = {0};
    bytes[0] = opcode;
    if(instruction->operands[0]->type == SVASM_OPERAND_LABEL)
    {
        bool labelOk = SVAsm_translator_registerLabel(translator, instruction->operands[0]->value.label,
                                                      translator->location + 1);
        if(!labelOk)
            return false;
    }
    else
    {
        SVAsm_NumberType number = instruction->operands[0]->value.number;
        if(number > 0xFFFF)
        {
            SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "value %d overflows address, truncating to %d", number, number & 0xFFFF);
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
            SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
        }
        bytes[1] = (uint8_t)(number >> 8 & 0xFF);
        bytes[2] = (uint8_t)(number & 0xFF);
    }
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 3);
    translator->location += 3;
    return true;
}

bool SVAsm_translator_translateInsJumpR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg = instruction->operands[0]->value.reg;
    uint8_t regBits;
    bytes[0] = opcode;
    if(SVASM_REGISTER_A <= reg && reg <= SVASM_REGISTER_Z)
    {
        bytes[0] = (uint8_t)(opcode);
        regBits = (reg - SVASM_REGISTER_A) << 1;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(regBits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsLDI(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_Register reg = instruction->operands[0]->value.reg;
    uint8_t regBits;
    if(SVASM_REGISTER_A <= reg && reg <= SVASM_REGISTER_Z)
    {
        if(!SVAsm_memoryObject_check(translator->memory, translator->location, 4))
        {
            SVASM_TRANSLATOR_INVALIDWRITE();
            SVASM_TRANSLATOR_ERROR();
        }
        uint8_t bytes[4] = {0};
        bytes[0] = (uint8_t)(opcode + 1);
        regBits = (reg - SVASM_REGISTER_A) << 1;
        bytes[3] = regBits;
        if(instruction->operands[1]->type == SVASM_OPERAND_LABEL)
        {
            bool labelOk = SVAsm_translator_registerLabel(translator, instruction->operands[1]->value.label,
                                                          translator->location + 1);
            if(!labelOk)
                return false;
        }
        else
        {
            SVAsm_NumberType number = instruction->operands[1]->value.number;
            if(number > 0xFFFF)
            {
                SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                            block->filename, block->filepath, block->line,
                            "value %d overflows register, truncating to %d", number, number & 0xFFFF);
                SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
                SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
            }
            bytes[1] = (uint8_t)(number >> 8 & 0xFF);
            bytes[2] = (uint8_t)(number & 0xFF);
        }
        SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 4);
        translator->location += 4;
    }
    else if(SVASM_REGISTER_AL <= reg && reg <= SVASM_REGISTER_ZH)
    {
        if(!SVAsm_memoryObject_check(translator->memory, translator->location, 3))
        {
            SVASM_TRANSLATOR_INVALIDWRITE();
            SVASM_TRANSLATOR_ERROR();
        }
        if(instruction->operands[1]->type == SVASM_OPERAND_LABEL)
        {
            SVAsm_error(translator->options,  SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_ERROR();
        }
        uint8_t bytes[3] = {0};
        bytes[0] = (uint8_t)(opcode);
        regBits = reg - SVASM_REGISTER_AL;
        bytes[2] = regBits;
        SVAsm_NumberType number = instruction->operands[1]->value.number;
        bytes[1] = (uint8_t)(number & 0xFF);
        if(number > 0xFF)
        {
            SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "value %d overflows register, truncating to %d", number, number & 0xFF);
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
            SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
        }
        SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 3);
        translator->location += 3;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    return true;
}

bool SVAsm_translator_translateInsMV(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    SVAsm_Register reg1 = instruction->operands[0]->value.reg;
    SVAsm_Register reg2 = instruction->operands[1]->value.reg;
    uint8_t reg1Bits;
    uint8_t reg2Bits;
    if((SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z) &&
            reg2 == SVASM_REGISTER_SP)
    {
        bytes[0] = (uint8_t)(opcode + 3);
        reg1Bits = 0;
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else if((SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z) &&
            reg2 == SVASM_REGISTER_PC)
    {
        bytes[0] = (uint8_t)(opcode + 2);
        reg1Bits = 0;
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else if((SVASM_REGISTER_A <= reg1 && reg1 <= SVASM_REGISTER_Z) &&
            (SVASM_REGISTER_A <= reg2 && reg2 <= SVASM_REGISTER_Z))
    {
        bytes[0] = (uint8_t)(opcode + 1);
        reg1Bits = (reg1 - SVASM_REGISTER_A) << 1;
        reg2Bits = (reg2 - SVASM_REGISTER_A) << 1;
    }
    else if((SVASM_REGISTER_AL <= reg1 && reg1 <= SVASM_REGISTER_ZH) &&
            (SVASM_REGISTER_AL <= reg2 && reg2 <= SVASM_REGISTER_ZH))
    {
        bytes[0] = (uint8_t)(opcode);
        reg1Bits = reg1 - SVASM_REGISTER_AL;
        reg2Bits = reg2 - SVASM_REGISTER_AL;
    }
    else
    {
        if(reg1 == SVASM_REGISTER_PC || reg1 == SVASM_REGISTER_SP)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "invalid register");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 0));
        }
        else
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line, "mismatched registers");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], 0);
        }
        SVASM_TRANSLATOR_ERROR();
    }
    bytes[1] = (uint8_t)(reg2Bits << 4 | reg1Bits);
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}

bool SVAsm_translator_translateInsSYS(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode)
{
    if(!translator || !instruction)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, 2))
    {
        SVASM_TRANSLATOR_INVALIDWRITE();
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t bytes[2] = {0};
    bytes[0] = (uint8_t)(opcode);
    SVAsm_NumberType number = instruction->operands[0]->value.number;
    bytes[1] = (uint8_t)(number & 0xFF);
    if(number > 0xFF)
    {
        SVAsm_error(translator->options, translator->options->warnings.registerOverflowError ? SVASM_ERROR : SVASM_WARNING,
                    block->filename, block->filepath, block->line,
                    "value %d overflows system call value, truncating to %d", number, number & 0xFF);
        SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[index], SVAsm_error_getParam(block, 1));
        SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.registerOverflowError);
    }
    SVAsm_memoryObject_fill(translator->memory, translator->location, bytes, 2);
    translator->location += 2;
    return true;
}
