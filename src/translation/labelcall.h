/**
 * \file labelcall.h
 * \brief Declares structures and functions related to label calls.
 * \author Schttopup
 */

#ifndef SVASM_LABELCALL_H
#define SVASM_LABELCALL_H

#include "../parsing/token.h"
#include <stdbool.h>


struct SVAsm_Arg_;


/**
 * Label call.
 */
struct SVAsm_LabelCall_
{
    char *label;                //!< Label name
    bool negative;              //!< Negative offset
    struct SVAsm_Arg_ *offset;  //!< Offset value
};

/// Type name for label calls.
typedef struct SVAsm_LabelCall_ SVAsm_LabelCall;


/**
 * Creates a label call.
 * \param label The label name
 * \param negative Negative offset
 * \param offset Offset value or NULL
 * \return The created label call
 */
SVAsm_LabelCall* SVAsm_labelCall_create(char const *label, bool negative, struct SVAsm_Arg_ *offset);

/**
 * Destroys a label call.
 * \param label The label call to destroy
 */
void SVAsm_labelCall_destroy(SVAsm_LabelCall *label);

/**
 * Copies a label call.
 * \param label The label call to copy
 * \return The copied label call
 */
SVAsm_LabelCall* SVAsm_labelCall_copy(SVAsm_LabelCall *label);


#endif //SVASM_LABELCALL_H
