/**
 * \file translator.c
 * \brief Defines functions related to code translation.
 * \author Schttopup
 */

#include "translator.h"
#include "expansion.h"
#include "translator_ins.h"
#include "translator_cmd.h"
#include "translator_mac.h"
#include "../error.h"
#include "macro_defs.h"
#include <sdefs.h>
#include <string.h>
#include <stdbool.h>


SVAsm_Translator* SVAsm_translator_create(SVAsm_BlockList *blocks, SVAsm_Options *options)
{
    if(!blocks)
        return NULL;
    SVAsm_Translator *translator = NULL;
    MMALLOC(translator, SVAsm_Translator, 1);
    if(!options)
        options = SVAsm_options_create();
    translator->options = options;
    translator->blocks = blocks;
    translator->memory = SVAsm_memoryObject_create(options->varDataStart);
    translator->location = 0;
    translator->labels = SVAsm_labelList_create();
    translator->exports = SVAsm_exportList_create();
    translator->metadata = SVAsm_metadata_create();
    translator->macros = SVAsm_macroList_create();
    translator->stack = SVAsm_stack_create();
    SVAsm_stack_push(translator->stack, SVAsm_stackItem_create(SVASM_STACK_ROOT, SVAsm_stackItemRoot_create(0)));
    translator->stop = false;
    translator->error = false;
    translator->warning = false;
    translator->source = NULL;
    translator->baseFilename = NULL;
    translator->baseFilepath = NULL;
    translator->counter = 0;
    translator->nested = false;
    translator->returnValue = NULL;
    return translator;
}

void SVAsm_translator_destroy(SVAsm_Translator *translator)
{
    if(!translator)
        return;
    SVAsm_memoryObject_destroy(translator->memory);
    SVAsm_labelList_destroy(translator->labels);
    SVAsm_exportList_destroy(translator->exports);
    SVAsm_metadata_destroy(translator->metadata);
    SVAsm_stack_destroy(translator->stack);
    if(!translator->nested)
        SVAsm_macroList_destroy(translator->macros);
    SVAsm_arg_destroy(translator->returnValue);
    FFREE(translator);
}

SVAsm_Translator* SVAsm_translator_createNested(SVAsm_Translator *translator)
{
    if(!translator)
        return NULL;
    SVAsm_Translator *newTranslator = NULL;
    MMALLOC(newTranslator, SVAsm_Translator, 1);
    newTranslator->options = translator->options;
    newTranslator->blocks = translator->blocks;
    newTranslator->memory = NULL;
    newTranslator->location = 0;
    newTranslator->labels = NULL;
    newTranslator->exports = NULL;
    newTranslator->metadata = NULL;
    newTranslator->macros = translator->macros;
    newTranslator->stack = SVAsm_stack_create();
    newTranslator->stop = false;
    newTranslator->error = false;
    newTranslator->warning = false;
    newTranslator->source = NULL;
    newTranslator->baseFilename = translator->baseFilename;
    newTranslator->baseFilepath = translator->baseFilepath;
    newTranslator->counter = translator->counter;
    newTranslator->nested = true;
    newTranslator->returnValue = NULL;
    return newTranslator;
}

void SVAsm_translator_setSource(SVAsm_Translator *translator, SVAsm_Source *source)
{
    if(!translator)
        return;
    translator->source = source;
}


void SVAsm_translator_translate(SVAsm_Translator *translator)
{
    if(!translator)
        return;
    while(!translator->stop)
    {
        size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
        switch(translator->blocks->blocks[index]->type)
        {
            case SVASM_BLOCK_LABEL:
                if(!SVAsm_translator_translateLabel(translator))
                    translator->error = true;
                break;
            case SVASM_BLOCK_INSTRUCTION:
                if(!SVAsm_translator_translateInstruction(translator))
                    translator->error = true;
                break;
            case SVASM_BLOCK_COMMAND:
                if(!SVAsm_translator_translateCommand(translator))
                    translator->error = true;
                break;
            case SVASM_BLOCK_MACRO:
                if(!SVAsm_translator_translateMacroCall(translator))
                    translator->error = true;
            default:
                break;
        }
        index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
        index++;
        if(index >= translator->blocks->blockCount)
            translator->stop = true;
        SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), index);
    }
    for(size_t i = 0; i < translator->labels->labelCount; i++)
    {
        SVAsm_Label *label = translator->labels->labels[i];
        if(!label->assigned)
        {
            if(translator->options->warnings.unsetLabels)
            {
                SVAsm_message(translator->options, SVASM_WARNING, "Label \"%s\" is not assigned, defaulting to 0x0000", label->name);
                if(translator->options->warningError)
                    translator->error = true;
            }
            continue;
        }
        uint16_t address = label->address;
        for(size_t j = 0; j < label->callCount; j++)
        {
            int32_t fullAddress = address + label->offsets[j];
            if(fullAddress < 0 || fullAddress >= 0x10000)
            {
                SVAsm_message(translator->options, translator->options->warnings.defaultValuesError ? SVASM_ERROR : SVASM_WARNING,
                              "Label \"%s\" is out of addressable space (%s0x%04X), defaulting to 0x0000",
                              label->name, (fullAddress < 0 ? "-" : ""), (fullAddress < 0 ? -fullAddress : fullAddress));
                if(translator->options->warnings.defaultValuesError)
                    translator->error = true;
                fullAddress = 0;
            }
            uint8_t bytes[2] = {(uint8_t)((fullAddress >> 8) & 0xFF), (uint8_t)(fullAddress & 0xFF)};
            SVAsm_memoryObject_fill(translator->memory, label->locations[j], bytes, 2);
        }
    }
}

bool SVAsm_translator_translateLabel(SVAsm_Translator *translator)
{
    if(!translator)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    char *name = block->content.text;
    SVAsm_Label *label = SVAsm_labelList_get(translator->labels, name);
    if(label)
    {
        if(label->assigned)
        {
            SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                        "reassigning label \'%s\'", label->name);
            SVAsm_errorTranslator(translator->options, translator, block, 0);
            SVASM_TRANSLATOR_WARNING();
        }
        else
            SVAsm_label_setAddress(label, translator->location);
    }
    else
    {
        label = SVAsm_label_create(name);
        SVAsm_label_setAddress(label, translator->location);
        SVAsm_labelList_add(translator->labels, label);
    }
    return true;
}

bool SVAsm_translator_translateInstruction(SVAsm_Translator *translator)
{
    if(!translator)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    bool ret = false;
    bool done = false;
    size_t translationIndex = 0;
    SVAsm_Instruction *instruction = block->content.instruction;
    SVAsm_Instruction *expandedInstruction = SVAsm_instruction_create(instruction->keyword);
    for(size_t i = 0; i < instruction->operandCount; i++)
    {
        SVAsm_Operand *expandedOperand = SVAsm_expansion_expandOperand(translator, instruction->operands[i]);
        if(!expandedOperand)
        {
            SVAsm_error(translator->options, SVASM_BLANK, block->filename, block->filepath, block->line,
                        "in instruction \'%s\' :", SVAsm_keywordValues[instruction->keyword]);
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, i));
            SVAsm_instruction_destroy(expandedInstruction);
            SVASM_TRANSLATOR_ERROR();
        }
        SVAsm_instruction_addOperand(expandedInstruction, expandedOperand);
    }
    while(!done)
    {
        if(SVAsm_instructionTranslations[translationIndex].keyword == expandedInstruction->keyword &&
                SVAsm_instructionTranslations[translationIndex].operandCount == expandedInstruction->operandCount)
        {
            bool correctOperands = true;
            for(size_t i = 0; i < expandedInstruction->operandCount; i++)
            {
                if(expandedInstruction->operands[i]->type != SVAsm_instructionTranslations[translationIndex].operands[i])
                {
                    correctOperands = false;
                    break;
                }
            }
            if(correctOperands)
            {
                ret = (*SVAsm_instructionTranslations[translationIndex].function)(translator, expandedInstruction, SVAsm_instructionTranslations[translationIndex].opcode);
                done = true;
            }
        }
        if(SVAsm_instructionTranslations[translationIndex].keyword == SVASM_KEYWORD_LAST)
            break;
        translationIndex++;
    }
    SVAsm_instruction_destroy(expandedInstruction);
    if(!done)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "invalid instruction");
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    return ret;
}

bool SVAsm_translator_translateCommand(SVAsm_Translator *translator)
{
    if(!translator)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_CommandLine *command = block->content.command;
    SVAsm_CommandLine *expandedCommand = SVAsm_commandLine_create(command->command);
    for(size_t i = 0; i < command->argCount; i++)
    {
        if(SVAsm_command_isArgExpandable(command->command, i))
        {
            SVAsm_Arg *expandedArg = SVAsm_expansion_expandArg(translator, command->args[i]);
            if(!expandedArg)
            {
                SVAsm_error(translator->options, SVASM_BLANK, block->filename, block->filepath, block->line,
                    "in command \'.%s\'", SVAsm_commandValues[command->command]);
                SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, i));
                SVAsm_commandLine_destroy(expandedCommand);
                SVASM_TRANSLATOR_ERROR();
            }
            SVAsm_commandLine_add(expandedCommand, expandedArg);
        }
        else
            SVAsm_commandLine_add(expandedCommand, SVAsm_arg_copy(command->args[i]));
    }
    bool ret = SVAsm_commandTranslations[command->command](translator, expandedCommand);
    SVAsm_commandLine_destroy(expandedCommand);
    return ret;
}

bool SVAsm_translator_translateMacroCall(SVAsm_Translator *translator)
{
    if(!translator)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_MacroCall *call = block->content.macro;
    SVAsm_Macro *macro = NULL;
    for(ssize_t i = translator->stack->itemCount - 1; i >= 0; i--)
    {
        macro = SVAsm_macroList_get(translator->stack->items[i]->locals, call->name);
        if(macro || translator->stack->items[i]->type == SVASM_STACK_MACRO)
            break;
    }
    if(!macro)
        macro = SVAsm_macroList_get(translator->macros, call->name);
    if(!macro)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", call->name);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    if(macro->type != SVASM_MACRO_FUNCTION)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' called as function macro", macro->name);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    if(call->argCount != macro->value.function->argCount)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' requires %d parameters, %d supplied",
                    macro->name, (int)macro->value.function->argCount, call->argCount);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_StackItemMacro *itemMacro = SVAsm_stackItemMacro_create(macro);
    for(size_t i = 0; i < call->argCount; i++)
    {
        itemMacro->args[i] = SVAsm_expansion_expandArg(translator, call->args[i]);
        if(!itemMacro->args[i])
        {
            SVAsm_error(translator->options, SVASM_BLANK, block->filename, block->filepath, block->line,
                        "in macro call");
            SVAsm_errorTranslator(translator->options, translator, block, i + 2);
            SVAsm_stackItemMacro_destroy(itemMacro);
            SVASM_TRANSLATOR_ERROR();
        }
    }
    SVAsm_stack_push(translator->stack, SVAsm_stackItem_create(SVASM_STACK_MACRO, itemMacro));
    if(translator->stack->itemCount >= translator->options->maxTranslatorDepth)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "maximum translator depth reached (%d)", translator->options->maxTranslatorDepth);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    return true;
}

