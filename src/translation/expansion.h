/**
 * \file expansion.h
 * \brief Declares structures and functions related to macro call expansion.
 * \author Schttopup
 */

#ifndef SVASM_EXPANSION_H
#define SVASM_EXPANSION_H


#include "translator.h"
#include "macrocallstack.h"
#include <stdbool.h>


/**
 * Processes a macro call element
 * \param translator The translator
 * \param stack The macro call stack
 * \return true if the element was processed, false otherwise
 */
bool SVAsm_expansion_processMacroElement(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack);

/**
 * Processes a macro call.
 * \param translator The translator
 * \param macro The macro call to process
 * \return The return argument of the macro call, of NULL if something went wrong
 */
SVAsm_Arg* SVAsm_expansion_processMacro(SVAsm_Translator *translator, SVAsm_MacroCall *macro);

/**
 * Calls a built-in macro.
 * \param translator The translator
 * \param macro The macro call stack item
 * \return The return argument of the macro call, of NULL if something went wrong
 */
SVAsm_Arg* SVAsm_expansion_callMacro(SVAsm_Translator *translator, SVAsm_MacroCallStackItem *macro);

/**
 * Creates a macro call stack item from a macro call.
 * \param translator The translator
 * \param macro The macro call
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getMacroStackItem(SVAsm_Translator *translator, SVAsm_MacroCall *macro);

/**
 * Creates a macro call stack item from an array.
 * \param translator The translator
 * \param array The array
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getArrayStackItem(SVAsm_Translator *translator, SVAsm_Array *array);

/**
 * Creates a macro call stack item from a label.
 * \param translator The translator
 * \param label The label
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getLabelStackItem(SVAsm_Translator *translator, SVAsm_LabelCall *label);

/**
 * Creates a macro call stack item from a block macro.
 * \param translator The translator
 * \param nestedTranslator The nested translator
 * \param parent The parent stack item
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_MacroCallStackItem *parent);

/**
 * Creates a macro call stack item from a command.
 * \param translator The translator
 * \param nestedTranslator The nested translator
 * \param command The command
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockCommandStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_CommandLine *command);

/**
 * Creates a macro call stack item from a function macro.
 * \param translator The translator
 * \param nestedTranslator The nested translator
 * \param macro The function macro call
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockMacroStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_MacroCall *macro);

/**
 * Expands a symbol to its corresponding argument.
 * \param translator The translator
 * \param stack The macro call stack, or NULL if called outside of a macro call
 * \param symbol The symbol to expand
 * \return The expanded symbol, or an argument containing that symbol if none was found
 */
SVAsm_Arg* SVAsm_expansion_expand(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, char const *symbol);

/**
 * Expands the offset part of a label.
 * \param translator The translator
 * \param stack The macro call stack
 * \param call The label call to expand
 * \return The expanded label
 */
SVAsm_Arg* SVAsm_expansion_expandLabel(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_LabelCall *call);

/**
 * Expands all the arguments of a macro call into a new macro call.
 * \param translator The translator
 * \param stack The macro call stack, or NULL if called outside of a macro call
 * \param call The macro call to expand
 * \return The expanded macro call
 */
SVAsm_MacroCall* SVAsm_expansion_expandMacro(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_MacroCall *call);

/**
 * Expand all the arguments of an array into a new array.
 * \param translator The translator
 * \param stack The macro call stack
 * \param array The array to expand
 * \return The expanded array
 */
SVAsm_Array* SVAsm_expansion_expandArray(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_Array *array);

/**
 * Processes or expands an argument to its final value.
 * \param translator The translator
 * \param arg The argument to expand
 * \return The final argument
 */
SVAsm_Arg* SVAsm_expansion_expandArg(SVAsm_Translator *translator, SVAsm_Arg *arg);

/**
 * Processes or expands an operand to its final value.
 * \param translator The translator
 * \param operand The operand to expand
 * \return The final operand
 */
SVAsm_Operand* SVAsm_expansion_expandOperand(SVAsm_Translator *translator, SVAsm_Operand *operand);

/**
 * Gets the macro list containing a given symbol.
 * \param translator The translator
 * \param symbol The symbol to search
 * \return The corresponding local macro list, or NULL if the symbol was not found.
 */
SVAsm_MacroList* SVAsm_expansion_getLocalMacroList(SVAsm_Translator *translator, char const *symbol);


#endif //SVASM_EXPANSION_H
