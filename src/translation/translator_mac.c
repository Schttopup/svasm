/**
 * \file translator_mac.c
 * \brief Defines functions related to svasm inline macros.
 * \author Schttopup
 */

#include "translator_mac.h"
#include "translator.h"
#include "expansion.h"
#include "../error.h"
#include "macro_defs.h"
#include <sdefs.h>
#include <string.h>


SVAsm_MacroTranslation const SVAsm_BuiltinMacroTranslations[SVASM_BUILTINMAC_LAST] = {
    NULL,
    SVAsm_translator_builtinAdd,
    SVAsm_translator_builtinSub,
    SVAsm_translator_builtinNeg,
    SVAsm_translator_builtinMul,
    SVAsm_translator_builtinDiv,
    SVAsm_translator_builtinMod,
    SVAsm_translator_builtinEq,
    SVAsm_translator_builtinNeq,
    SVAsm_translator_builtinLt,
    SVAsm_translator_builtinNlt,
    SVAsm_translator_builtinGt,
    SVAsm_translator_builtinNgt,
    SVAsm_translator_builtinOr,
    SVAsm_translator_builtinAnd,
    SVAsm_translator_builtinXor,
    SVAsm_translator_builtinNot,
    SVAsm_translator_builtinBitOr,
    SVAsm_translator_builtinBitAnd,
    SVAsm_translator_builtinBitXor,
    SVAsm_translator_builtinBitNot,
    SVAsm_translator_builtinBitShl,
    SVAsm_translator_builtinBitShr,
    SVAsm_translator_builtinBitRol,
    SVAsm_translator_builtinBitRor,
    SVAsm_translator_builtinNum,
    SVAsm_translator_builtinStr,
    SVAsm_translator_builtinOrd,
    SVAsm_translator_builtinChr,
    SVAsm_translator_builtinLen,
    SVAsm_translator_builtinGet,
    SVAsm_translator_builtinSet,
    SVAsm_translator_builtinPush,
    SVAsm_translator_builtinPop,
    SVAsm_translator_builtinConcat,
    SVAsm_translator_builtinSlice,
    SVAsm_translator_builtinVal,
    SVAsm_translator_builtinFetch,
    SVAsm_translator_builtinCall,
    SVAsm_translator_builtinDef,
    SVAsm_translator_builtinType,
    SVAsm_translator_builtinTruth
};


SVAsm_BuiltinMacro SVAsm_translator_getBuiltinMacro(char const *symbol)
{
    if(!symbol)
        return SVASM_BUILTINMAC_NONE;
    for(size_t i = 0; i < SVASM_BUILTINMAC_LAST; i++)
    {
        if(strcmp(symbol, SVAsm_BuiltinMacroNames[i]) == 0)
            return (SVAsm_BuiltinMacro)i;
    }
    return SVASM_BUILTINMAC_NONE;
}


SVAsm_Arg* SVAsm_translator_builtinAdd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount == 0)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_MORE("_add", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    for(size_t i = 0; i < call->argCount; i++)
    {
        if(call->args[i]->type == SVASM_ARG_NUMBER)
            number += call->args[i]->value.number;
        else
        {
            SVASM_TRANSLATOR_BADPARAMS("_add", call, i, "number", call->args[i]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinSub(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_sub", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        number = call->args[0]->value.number - call->args[1]->value.number;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_sub", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNeg(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_neg", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAMS("_neg", call, 0, "number", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    number = -call->args[0]->value.number;
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinMul(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_mul", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number += call->args[0]->value.number * call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_mul", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinDiv(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_div", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[1]->value.number == 0)
        {
            if(translator->options->warnings.mathWarning)
            {
                SVAsm_error(translator->options, translator->options->warnings.mathError ? SVASM_ERROR : SVASM_WARNING,
                            block->filename, block->filepath, block->line,
                            "division by 0 (defaulting to 0)");
                SVAsm_errorMacro(translator->options, call, 3);
                SVASM_TRANSLATOR_MAC_WARNING_ERRIF(translator->options->warnings.mathError);
            }
            number = 0;
        }
        else
            number += call->args[0]->value.number / call->args[1]->value.number;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_div", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinMod(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_mod", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[1]->value.number == 0)
        {
            if(translator->options->warnings.mathWarning)
            {
                SVAsm_error(translator->options, translator->options->warnings.mathError ? SVASM_ERROR : SVASM_WARNING,
                            block->filename, block->filepath, block->line,
                            "division by 0 (defaulting to 0)");
                SVAsm_errorMacro(translator->options, call, 3);
                SVASM_TRANSLATOR_MAC_WARNING_ERRIF(translator->options->warnings.mathError);
            }
            number = 0;
        }
        else
            number += call->args[0]->value.number % call->args[1]->value.number;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_mod", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinEq(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_eq", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = SVAsm_arg_equal(call->args[0], call->args[1]) ? 1 : 0;
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNeq(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_neq", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = SVAsm_arg_equal(call->args[0], call->args[1]) ? 0 : 1;
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinLt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_lt", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[0]->value.number < call->args[1]->value.number)
            number = 1;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_lt", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNlt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_nlt", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[0]->value.number >= call->args[1]->value.number)
            number = 1;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_nlt", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinGt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_gt", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[0]->value.number > call->args[1]->value.number)
            number = 1;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_gt", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNgt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_ngt", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
    {
        if(call->args[0]->value.number <= call->args[1]->value.number)
            number = 1;
    }
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_ngt", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinOr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_MORE("_or", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    for(size_t i = 0; i < call->argCount; i++)
        number = number || SVAsm_arg_truth(call->args[i]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinAnd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_MORE("_and", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 1;
    for(size_t i = 0; i < call->argCount; i++)
        number = number && SVAsm_arg_truth(call->args[i]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinXor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_xor", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = SVAsm_arg_truth(call->args[0]) != SVAsm_arg_truth(call->args[1]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNot(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_not", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = !SVAsm_arg_truth(call->args[0]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitOr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_or", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = call->args[0]->value.number | call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_or", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitAnd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_and", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = call->args[0]->value.number & call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_and", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitXor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_xor", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = call->args[0]->value.number ^ call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_xor", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitNot(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_not", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER)
        number = ~call->args[0]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_not", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitShl(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_shl", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = call->args[0]->value.number << call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_shl", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitShr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_shr", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = call->args[0]->value.number >> call->args[1]->value.number;
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_shr", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitRol(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_rol", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    SVAsm_NumberType roll = call->args[1]->value.number % (CHAR_BIT * sizeof(SVAsm_NumberType));
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = (call->args[0]->value.number << roll) | (call->args[0]->value.number >> ((CHAR_BIT * sizeof(SVAsm_NumberType)) - roll));
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_rol", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinBitRor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_bit_ror", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    SVAsm_NumberType roll = call->args[1]->value.number % (CHAR_BIT * sizeof(SVAsm_NumberType));
    if(call->args[0]->type == SVASM_ARG_NUMBER && call->args[1]->type == SVASM_ARG_NUMBER)
        number = (call->args[0]->value.number >> roll) | (call->args[0]->value.number << ((CHAR_BIT * sizeof(SVAsm_NumberType)) - roll));
    else
    {
        int badParam = (call->args[0]->type != SVASM_ARG_NUMBER ? 0 : 1);
        SVASM_TRANSLATOR_BADPARAMS("_bit_ror", call, badParam, "number", call->args[badParam]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinNum(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1 && call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_RANGE("_num", call, 1, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_num", call, 0, "string", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType base = 10;
    if(call->argCount == 2)
    {
        if (call->args[1]->type != SVASM_ARG_NUMBER)
        {
            SVASM_TRANSLATOR_BADPARAM("_num", call, 1, "number", call->args[1]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        base = call->args[1]->value.number;
    }
    if(base < 1 || base > 36)
    {
        if(translator->options->warnings.mathWarning)
        {
            SVAsm_error(translator->options, translator->options->warnings.mathError ? SVASM_ERROR : SVASM_WARNING,
                        block->filename, block->filepath, block->line,
                        "invalid base %d, must be [2;36] (defaulting to 10)", base);
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_WARNING_ERRIF(translator->options->warnings.mathError);
        }
        base = 10;
    }
    SVAsm_NumberType number = 0;
    long long result = strtoll(call->args[0]->value.text, NULL, base);
    if(result == LONG_LONG_MAX || result > (long long)INT64_MAX || result < (long long)INT64_MIN)
    {
        SVAsm_error(translator->options, translator->options->warnings.defaultValuesError ? SVASM_ERROR : SVASM_WARNING,
                    block->filename, block->filepath, block->line,
                    "string \"%s\" cannot be converted to number (defaulting to 0)", call->args[0]->value.text);
        SVAsm_errorMacro(translator->options, call, 2);
        SVASM_TRANSLATOR_MAC_WARNING_ERRIF(translator->options->warnings.defaultValuesError);
        result = 0;
    }
    number = (SVAsm_NumberType)result;
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinStr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 1 || call->argCount > 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_RANGE("_str", call, 1, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType base = 10;
    if(call->argCount == 2)
    {
        if(call->args[1]->type != SVASM_ARG_NUMBER)
        {
            SVASM_TRANSLATOR_BADPARAM("_str", call, 1, "number", call->args[1]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        base = call->args[1]->value.number;
        if(base < 2 || base > 36)
        {
            if(translator->options->warnings.mathWarning)
            {
                SVAsm_error(translator->options, translator->options->warnings.mathError ? SVASM_WARNING : SVASM_ERROR,
                            block->filename, block->filepath, block->line,
                            "invalid base %d, must be [2;36] (defaulting to 10)", call->args[0]->value.number);
                SVAsm_errorMacro(translator->options, call, 2);
                SVASM_TRANSLATOR_MAC_WARNING_ERRIF(translator->options->warnings.mathError);
            }
            base = 10;
        }
    }
    char *newString = SVAsm_arg_toString(call->args[0], base);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_STRING, newString);
    FFREE(newString);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinOrd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_ord", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_ord", call, 0, "string", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    size_t len = strlen(call->args[0]->value.text);
    if(len != 1)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "string \"%s\" too long (%d), expected 1 character", call->args[0]->value.text, len);
        SVAsm_errorMacro(translator->options, call, 2);
        SVASM_TRANSLATOR_MAC_WARNING();
    }
    SVAsm_NumberType number = (SVAsm_NumberType)(call->args[0]->value.text[0]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinChr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{

    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_chr", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAM("_chr", call, 0, "number", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = call->args[0]->value.number;
    if(number < 1 || number > 255)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "value %d is not a valid character", number);
        SVAsm_errorMacro(translator->options, call, 2);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    char value[2] = {(char)(call->args[0]->value.number),'\0'};
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_STRING, value);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinLen(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_len", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = 0;
    if(call->args[0]->type == SVASM_ARG_STRING)
        number = strlen(call->args[0]->value.text);
    else if(call->args[0]->type == SVASM_ARG_ARRAY)
        number = call->args[0]->value.array->argCount;
    else
    {
        SVASM_TRANSLATOR_BADPARAM("_len", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinGet(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_get", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_ARRAY && call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_get", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[1]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAM("_get", call, 1, "number", call->args[1]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType arrIdx = call->args[1]->value.number;
    SVAsm_Arg *newArg = NULL;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
    {
        if(arrIdx < 0 || arrIdx >= (ssize_t)(call->args[0]->value.array->argCount))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        newArg = SVAsm_arg_copy(SVAsm_array_get(call->args[0]->value.array, arrIdx));
    }
    else // SVASM_ARG_STRING
    {
        if(arrIdx < 0 || arrIdx >= (ssize_t)strlen(call->args[0]->value.text))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        char value[2] = {call->args[0]->value.text[arrIdx], '\0'};
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, value);
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinSet(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 3)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_set", call, 3, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_ARRAY && call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_set", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[1]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAM("_set", call, 1, "number", call->args[1]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type == SVASM_ARG_STRING && call->args[2]->type != SVASM_ARG_STRING)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                 "cannot set string element to non-string value");
        SVAsm_errorMacro(translator->options, call, 4);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType arrIdx = call->args[1]->value.number;
    SVAsm_Arg *newArg = NULL;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
    {
        if (arrIdx < 0 || arrIdx >= (ssize_t)(call->args[0]->value.array->argCount))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        SVAsm_Array *newArray = SVAsm_array_copy(call->args[0]->value.array);
        SVAsm_array_set(newArray, arrIdx, SVAsm_arg_copy(call->args[2]));
        newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
    }
    else // SVASM_ARG_STRING
    {
        if(strlen(call->args[2]->value.text) != 1)
        {
            SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                        "string \"%s\" too long (%d), expected 1 character", call->args[2]->value.text, strlen(call->args[2]->value.text));
            SVAsm_errorMacro(translator->options, call, 4);
            SVASM_TRANSLATOR_MAC_WARNING();
        }
        if (arrIdx < 0 || arrIdx >= (ssize_t)strlen(call->args[0]->value.text))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, call->args[0]->value.text);
        newArg->value.text[arrIdx] = call->args[2]->value.text[0];
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinPush(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 2 || call->argCount > 3)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_RANGE("_push", call, 2, 3, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_ARRAY && call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_push", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType arrIdx = 0;
    bool arrIdxSet = false;
    if(call->argCount == 3)
    {
        if(call->args[2]->type != SVASM_ARG_NUMBER)
        {
            SVASM_TRANSLATOR_BADPARAM("_push", call, 2, "number", call->args[2]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        arrIdx = call->args[2]->value.number;
        arrIdxSet = true;
    }
    if(call->args[0]->type == SVASM_ARG_STRING && call->args[1]->type != SVASM_ARG_STRING)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "cannot push non-string value in string");
        SVAsm_errorMacro(translator->options, call, 3);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *newArg = NULL;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
    {
        if(!arrIdxSet)
            arrIdx = call->args[0]->value.array->argCount;
        if(arrIdx < 0 || arrIdx > (ssize_t)(call->args[0]->value.array->argCount))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 4);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        SVAsm_Array *newArray = SVAsm_array_copy(call->args[0]->value.array);
        SVAsm_array_insert(newArray, arrIdx, SVAsm_arg_copy(call->args[1]));
        newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
    }
    else // SVASM_ARG_STRING
    {
        if(!arrIdxSet)
            arrIdx = strlen(call->args[0]->value.text);
        if(strlen(call->args[1]->value.text) != 1)
        {
            SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                        "string \"%s\" too long (%d), expected 1 character", call->args[1]->value.text, strlen(call->args[1]->value.text));
            SVAsm_errorMacro(translator->options, call, 3);
            SVASM_TRANSLATOR_MAC_WARNING();
        }
        if(arrIdx < 0 || arrIdx > (ssize_t)strlen(call->args[0]->value.text))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 4);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        char *newString = NULL;
        MMALLOC(newString, char, strlen(call->args[0]->value.text) + 2);
        strcpy(newString, call->args[0]->value.text);
        for(size_t i = strlen(call->args[0]->value.text) + 1; (ssize_t)i > arrIdx; i--)
            newString[i] = newString[i - 1];
        newString[arrIdx] = call->args[1]->value.text[0];
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, newString);
        FFREE(newString);
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinPop(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 1 || call->argCount > 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_RANGE("_pop", call, 1, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_ARRAY && call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_pop", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType arrIdx = 0;
    bool arrIdxSet = false;
    if(call->argCount == 2)
    {
        if(call->args[1]->type != SVASM_ARG_NUMBER)
        {
            SVASM_TRANSLATOR_BADPARAM("_pop", call, 1, "number", call->args[1]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        arrIdx = call->args[1]->value.number;
        arrIdxSet = true;
    }
    SVAsm_Arg *newArg = NULL;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
    {
        if(!arrIdxSet)
            arrIdx = call->args[0]->value.array->argCount - 1;
        if(arrIdx < 0 || arrIdx >= (ssize_t)(call->args[0]->value.array->argCount))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 2);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        SVAsm_Array *newArray = SVAsm_array_copy(call->args[0]->value.array);
        SVAsm_array_remove(newArray, arrIdx);
        newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
    }
    else // SVASM_ARG_STRING
    {
        if(!arrIdxSet)
            arrIdx = strlen(call->args[0]->value.text) - 1;
        if(arrIdx < 0 || arrIdx >= (ssize_t)strlen(call->args[0]->value.text))
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "index %d out of range", arrIdx);
            SVAsm_errorMacro(translator->options, call, 2);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        char *newString = NULL;
        MMALLOC(newString, char, strlen(call->args[0]->value.text) + 2);
        strcpy(newString, call->args[0]->value.text);
        for(size_t i = arrIdx; call->args[0]->value.text[i]; i++)
            newString[i] = newString[i + 1];
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, newString);
        FFREE(newString);
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinConcat(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount < 2)
    {
        SVASM_TRANSLATOR_PARAMCOUNT_MORE("_concat", call, 2, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_ArgType type = SVASM_ARG_NONE;
    for(size_t i = 0; i < call->argCount; i++)
    {
        if(call->args[i]->type == SVASM_ARG_STRING && (type == SVASM_ARG_NONE || type == SVASM_ARG_STRING))
            type = SVASM_ARG_STRING;
        else if(call->args[i]->type == SVASM_ARG_ARRAY && (type == SVASM_ARG_NONE || type == SVASM_ARG_ARRAY))
            type = SVASM_ARG_ARRAY;
        else
        {
            SVASM_TRANSLATOR_BADPARAMS("_concat", call, i, "string or array", call->args[i]);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
    }
    SVAsm_Arg *newArg = NULL;
    if(type == SVASM_ARG_ARRAY)
    {
        SVAsm_Array *array = SVAsm_array_create();
        for(size_t i = 0; i < call->argCount; i++)
        {
            for(size_t j = 0; j < call->args[i]->value.array->argCount; j++)
                SVAsm_array_insert(array, array->argCount, SVAsm_arg_copy(call->args[i]->value.array->args[j]));
        }
        newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, array);
    }
    else // SVASM_ARG_STRING
    {
        char *newString = NULL;
        size_t len = 0;
        for(size_t i = 0; i < call->argCount; i++)
        {
            size_t pos = len;
            len += strlen(call->args[i]->value.text);
            RREALLOC(newString, char, len + 1);
            strcpy(newString + pos, call->args[i]->value.text);
        }
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, newString);
        FFREE(newString);
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinSlice(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 3)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_slice", call, 3, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_ARRAY && call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_slice", call, 0, "string or array", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[1]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAM("_slice", call, 1, "number", call->args[1]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[2]->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADPARAM("_slice", call, 2, "number", call->args[2]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType start = call->args[1]->value.number;
    SVAsm_NumberType end = call->args[2]->value.number;
    size_t itemCount = 0;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
        itemCount = call->args[0]->value.array->argCount;
    else // SVASM_ARG_STRING
        itemCount = strlen(call->args[0]->value.text);
    if(start < 0 || start >= (ssize_t)itemCount)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "index %d out of range", start);
        SVAsm_errorMacro(translator->options, call, 3);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(end < 0 || end > (ssize_t)itemCount)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "index %d out of range", end);
        SVAsm_errorMacro(translator->options, call, 4);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(start > end)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "unordered indices");
        SVAsm_errorMacro(translator->options, call, 4);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *newArg = NULL;
    if(call->args[0]->type == SVASM_ARG_ARRAY)
    {
        SVAsm_Array *newArray = SVAsm_array_create();
        for(size_t i = start; (ssize_t)i < end; i++)
            SVAsm_array_insert(newArray, newArray->argCount, SVAsm_arg_copy(call->args[0]->value.array->args[i]));
        newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
    }
    else // SVASM_ARG_STRING
    {
        char *newString = NULL;
        MMALLOC(newString, char, end - start + 1);
        for(size_t i = 0; (ssize_t)i < end - start; i++)
            newString[i] = call->args[0]->value.text[start + i];
        newString[end - start] = '\0';
        newArg = SVAsm_arg_create(SVASM_ARG_STRING, newString);
        FFREE(newString);
    }
    return newArg;
}

SVAsm_Arg* SVAsm_translator_builtinVal(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_val", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_copy(call->args[0]);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinFetch(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_fetch", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_fetch", call, 0, "string", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    char *symbol = call->args[0]->value.text;
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
        list = translator->macros;
    SVAsm_Macro *macro = SVAsm_macroList_get(list, symbol);
    if(!macro)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(macro->type == SVASM_MACRO_NONE)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' is local but undefined", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(macro->type != SVASM_MACRO_CONSTANT)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' used as a constant", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = SVAsm_arg_copy(macro->value.constant->arg);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinCall(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    UNUSED(translator);
    UNUSED(call);
    return NULL;
}

SVAsm_Arg* SVAsm_translator_builtinDef(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_def", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(call->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADPARAM("_def", call, 0, "string", call->args[0]);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_Arg *ret = NULL;
    char *symbol = call->args[0]->value.text;
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
        list = translator->macros;
    SVAsm_Macro *macro = SVAsm_macroList_get(list, symbol);
    SVAsm_NumberType retVal = 0;
    if(macro)
        retVal = 1;
    ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &retVal);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinType(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_type", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    char const *typeStr = SVAsm_argTypeValues[call->args[0]->type];
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_STRING, typeStr);
    return ret;
}

SVAsm_Arg* SVAsm_translator_builtinTruth(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(call->argCount != 1)
    {
        SVASM_TRANSLATOR_PARAMCOUNT("_truth", call, 1, call->argCount);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_NumberType number = SVAsm_arg_truth(call->args[0]);
    SVAsm_Arg *ret = SVAsm_arg_create(SVASM_ARG_NUMBER, &number);
    return ret;
}
