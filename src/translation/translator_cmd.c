/**
 * \file translator_cmd.c
 * \brief Defines functions related to svasm commands.
 * \author Schttopup
 */

#include "translator_cmd.h"
#include "translator.h"
#include "translator_mac.h"
#include "translator_var.h"
#include "expansion.h"
#include "../parsing/parser.h"
#include "../building/builder.h"
#include "../assembly/metadata.h"
#include "../error.h"
#include "macro_defs.h"
#include <sdefs.h>
#include <string.h>
#include <stdbool.h>


SVAsm_CommandTranslation const SVAsm_commandTranslations[SVASM_COMMAND_LAST] = {
    NULL,
    SVAsm_translator_translateOrigin,
    SVAsm_translator_translateExport,
    SVAsm_translator_translateMeta,
    SVAsm_translator_translateInclude,
    SVAsm_translator_translateBinary,
    // TODO load = read ; new load loads byte array
    SVAsm_translator_translateLoad,
    SVAsm_translator_translateBytes,
    SVAsm_translator_translateSpace,
    SVAsm_translator_translateStack,
    SVAsm_translator_translateDefine,
    SVAsm_translator_translateSet,
    SVAsm_translator_translateAssign,
    SVAsm_translator_translateDelete,
    SVAsm_translator_translateLocal,
    SVAsm_translator_translateUnlocal,
    SVAsm_translator_translateCall,
    SVAsm_translator_translateIf,
    SVAsm_translator_translateElse,
    SVAsm_translator_translateFor,
    SVAsm_translator_translateWhile,
    SVAsm_translator_translateMacro,
    SVAsm_translator_translateBlock,
    SVAsm_translator_translateEnd,
    SVAsm_translator_translateReturn,
    SVAsm_translator_translateMessage,
    SVAsm_translator_translateWarning,
    SVAsm_translator_translateError
};


bool SVAsm_translator_translateOrigin(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("origin");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("origin", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADARG("origin", 0, "number", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    if(arg->value.number > 0xFFFF)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "origin beyond 0xFFFF");
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    translator->location = (uint16_t)arg->value.number;
    return true;
}

bool SVAsm_translator_translateExport(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("export");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("export", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("export", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Export *export = SVAsm_export_create(arg->value.text, translator->location);
    SVAsm_exportList_set(translator->exports, export);
    return true;
}

bool SVAsm_translator_translateMeta(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("meta");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT("meta", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *argKey = command->args[0];
    if(argKey->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("meta", 0, "string", argKey);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *argValue = command->args[1];
    if(argValue->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("meta", 1, "string", argValue);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_metadata_set(translator->metadata, argKey->value.text, argValue->value.text);
    return true;
}

bool SVAsm_translator_translateInclude(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    if(translator->source == NULL)
        return false;
    if(translator->source->callback == NULL)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    for(size_t i = translator->stack->itemCount; i > 0; i--)
    {
        if(translator->stack->items[i - 1]->type != SVASM_STACK_ROOT && translator->stack->items[i - 1]->type != SVASM_STACK_CONDITION)
        {
            SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                        "\'.include\' command outside of root level");
            SVAsm_errorTranslator(translator->options, translator, block, 0);
            SVASM_TRANSLATOR_FATAL();
        }
    }
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("include");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("include", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("include", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    if(block->level >= translator->options->maxIncludeLevels)
    {
        SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                    "maximum inclusion depth reached (%d)", translator->options->maxIncludeLevels);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_FATAL();
    }
    uint8_t *data = NULL;
    size_t size = 0;
    char *filename = NULL;
    char *filepath = NULL;
    bool loaded = SVAsm_source_load(translator->source, arg->value.text, &data, &size, &filename, &filepath);
    if(!loaded)
    {
        SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                    "unable to include source file <%s>", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_FATAL();
    }
    RREALLOC(data, uint8_t, size+1);
    data[size] = '\0';

    SVAsm_TokenList *tokens = SVAsm_parser_parse((char*)data, filename, filepath, translator->options);
    FFREE(data);
    if(!tokens)
    {
        SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                    "source file <%s> contains errors", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_FATAL();
    }
    SVAsm_Builder *builder = SVAsm_builder_create(tokens, filename, filepath, block->level + 1, translator->options);
    SVAsm_builder_build(builder);
    SVAsm_tokenList_destroy(tokens);
    if(builder->error)
    {
        SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                    "source file <%s> contains errors", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVAsm_builder_destroy(builder);
        SVASM_TRANSLATOR_FATAL();
    }
    if(builder->blocks->blockCount == 0)
    {
        SVAsm_builder_destroy(builder);
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                 "empty source file <%s>", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    else
    {
        SVAsm_blockList_insert(translator->blocks, builder->blocks, index);
        SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), index - 1);
        builder->blocks = NULL;
        SVAsm_builder_destroy(builder);
    }
    return true;
}

bool SVAsm_translator_translateBinary(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    if(translator->source == NULL)
        return false;
    if(translator->source->callback == NULL)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("binary");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("binary", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("binary", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t *data = NULL;
    size_t size = 0;
    char *filename = NULL;
    char *filepath = NULL;
    bool loaded = (*translator->source->callback)(arg->value.text, translator->source->aux, &data, &size, &filename, &filepath);
    if(!loaded)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "unable to load binary file <%s>", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    if(!SVAsm_memoryObject_check(translator->memory, translator->location, size))
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "binary file <%s> exceeds write space at 0x%04X (%d needed)", arg->value.text, translator->location, size);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_memoryObject_fill(translator->memory, translator->location, data, size);
    return true;
}

bool SVAsm_translator_translateLoad(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    if(translator->source == NULL)
        return false;
    if(translator->source->callback == NULL)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("load");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT("load", 2, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg =  command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("load", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[1]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG("load", 1, "symbol", command->args[1]);
        SVASM_TRANSLATOR_ERROR();
    }
    uint8_t *data = NULL;
    size_t size = 0;
    char *filename = NULL;
    char *filepath = NULL;
    bool loaded = (*translator->source->callback)(arg->value.text, translator->source->aux, &data, &size, &filename, &filepath);
    if(!loaded)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "unable to load file <%s>", arg->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    RREALLOC(data, uint8_t, size + 1);
    data[size] = '\0';
    SVAsm_Macro *macro = SVAsm_macro_create(command->args[1]->value.text, SVASM_MACRO_CONSTANT);
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_Arg *macroArg = SVAsm_arg_create(SVASM_ARG_STRING, (char*)data);
    SVAsm_macro_setConstant(macro, macroArg);
    SVAsm_arg_destroy(macroArg);
    SVAsm_macroList_add(translator->macros, macro);
    FFREE(data);
    return true;
}

static int SVAsm_translator_writeBytes(struct SVAsm_Translator_ *translator, SVAsm_Arg *arg)
{
    if(!translator || !arg)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    bool warn = false;
    switch(arg->type)
    {
        case SVASM_ARG_NUMBER:
            if(arg->value.number < 0 || arg->value.number > 0xFF)
            {
                SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                            "byte parameters must be [0;255] (%d provided, truncating to %d)", arg->value.number, arg->value.number & 0xFF);
                warn = true;
                SVASM_TRANSLATOR_WARNING()
            }
            if(SVAsm_memoryObject_check(translator->memory, translator->location, 1))
            {
                uint8_t value = (uint8_t)(arg->value.number & 0xFF);
                SVAsm_memoryObject_fill(translator->memory, translator->location, &value, 1);
                translator->location++;
            }
            else
            {
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "invalid write space at 0x%04X", translator->location);
                SVASM_TRANSLATOR_ERROR()
            }
            break;
        case SVASM_ARG_LABEL:
            {
                SVAsm_Label *label = SVAsm_labelList_get(translator->labels, (arg->value.label->label));
                if(!label)
                {
                    label = SVAsm_label_create(arg->value.label->label);
                    SVAsm_labelList_add(translator->labels, label);
                }
                SVAsm_NumberType offset = 0;
                if(arg->value.label->offset != NULL)
                {
                    if(arg->value.label->offset->type != SVASM_ARG_NUMBER)
                    {
                        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                    "label offset must be a number");
                        SVASM_TRANSLATOR_ERROR()
                    }
                    offset = arg->value.label->offset->value.number;
                    if(offset <= -0x10000 || offset >= 0x10000)
                    {
                        SVAsm_error(translator->options, translator->options->warnings.defaultValuesError ? SVASM_ERROR : SVASM_WARNING,
                                    block->filename, block->filepath, block->line,
                                    "label offset out of range (%s0x%04X), defaulting to 0", (offset < 0 ? "-" : ""), (offset < 0 ? -offset : offset));
                        SVASM_TRANSLATOR_WARNING_ERRIF(translator->options->warnings.defaultValuesError)
                        offset = 0;
                        warn = true;
                    }
                    if(arg->value.label->negative)
                        offset = -offset;
                }
                SVAsm_label_addCall(label, (uint16_t) (translator->location), (int32_t)offset);
                if(SVAsm_memoryObject_check(translator->memory, translator->location, 1))
                {
                    uint16_t value = 0;
                    SVAsm_memoryObject_fill(translator->memory, translator->location, (uint8_t*)&value, 1);
                    translator->location += 2;
                }
                else
                {
                    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                "invalid write space at 0x%04X", translator->location);
                    SVASM_TRANSLATOR_ERROR()
                }
            }
            break;
        case SVASM_ARG_STRING:
            {
                size_t len = strlen(arg->value.text);
                if(SVAsm_memoryObject_check(translator->memory, translator->location, len))
                {
                    SVAsm_memoryObject_fill(translator->memory, translator->location, (uint8_t*)(arg->value.text), len);
                    translator->location += len;
                }
                else
                {
                    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                "invalid write space at 0x%04X", translator->location);
                    SVASM_TRANSLATOR_ERROR()
                }
            }
            break;
        case SVASM_ARG_ARRAY:
            {
                for(size_t i = 0; i < arg->value.array->argCount; i++)
                {
                    int ok = SVAsm_translator_writeBytes(translator, arg->value.array->args[i]);
                    if(ok != 0)
                    {
                        SVAsm_error(translator->options, SVASM_BLANK, block->filename, block->filepath, block->line,
                                 "in array");
                        SVAsm_errorArray(translator->options, arg->value.array, SVAsm_error_getArrayParam(arg->value.array, i));
                        if(ok > 0)
                        SVASM_TRANSLATOR_ERROR()
                        else
                        {
                            warn = true;
                            SVASM_TRANSLATOR_WARNING()
                        }
                    }
                }
            }
            break;
        default:
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "invalid type", translator->location);
            SVASM_TRANSLATOR_ERROR()
            break;
    }
    if(warn)
        return -1;
    return 0;
}

bool SVAsm_translator_translateBytes(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("bytes");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount == 0)
    {
        SVASM_TRANSLATOR_ARGCOUNT_MORE("bytes", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    for(size_t i = 0; i < command->argCount; i++)
    {
        SVAsm_Arg *arg = command->args[i];
        int ok = SVAsm_translator_writeBytes(translator, arg);
        if(ok != 0)
        {
            SVASM_TRANSLATOR_BADARG_EXP("bytes", i);
            if(ok > 0)
                return false;
        }
    }
    return true;
}

bool SVAsm_translator_translateSpace(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("space");
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("space", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_NUMBER)
    {
        SVASM_TRANSLATOR_BADARG("space", 0, "number", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    size_t len = arg->value.number;
    if(SVAsm_memoryObject_bounds(translator->memory, translator->location, len))
    {
        SVAsm_memoryObject_fill(translator->memory, translator->location, NULL, len);
        translator->location += len;
    }
    else
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "invalid write space at 0x%04X", translator->location);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    return true;
}

bool SVAsm_translator_translateStack(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(translator->nested)
    {
        SVASM_TRANSLATOR_NESTED("stack");
        SVASM_TRANSLATOR_ERROR();
    }
    if(translator->memory->stackSize > 0)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "stack space already defined");
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("stack", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type == SVASM_ARG_NUMBER)
    {
        size_t len = arg->value.number;
        if(len == 0)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "stack size is zero", translator->location);
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_ERROR();
        }
        else if(SVAsm_memoryObject_check(translator->memory, translator->location, len))
        {
            SVAsm_memoryObject_fill(translator->memory, translator->location, NULL, len);
            translator->memory->stackStart = translator->location;
            translator->location += len;
            translator->memory->stackSize = len;
        }
        else
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "invalid write space at 0x%04X", translator->location);
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_ERROR();
        }
    }
    else
    {
        SVASM_TRANSLATOR_BADARG("stack", 0, "number", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    return true;
}

bool SVAsm_translator_translateDefine(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT("define", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_MACRO)
    {
        SVASM_TRANSLATOR_BADARG("define", 0, "macro", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[1]->type != SVASM_ARG_MACRO)
    {
        SVASM_TRANSLATOR_BADARG("define", 1, "macro", command->args[1]);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_MacroCall *call = command->args[0]->value.macro;
    SVAsm_Macro *macro = SVAsm_macro_create(call->name, SVASM_MACRO_LINE);
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_MacroLine *line = macro->value.line;
    for(size_t i = 0; i < call->argCount; i++)
    {
        if(call->args[i]->type != SVASM_ARG_SYMBOL)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "macro prototype parameters must be symbols");
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 2 + i);
            SVAsm_macro_destroy(macro);
            SVASM_TRANSLATOR_ERROR();
        }
        RREALLOC(line->args, SVAsm_Arg*, line->argCount + 1);
        line->args[line->argCount] = SVAsm_arg_copy(call->args[i]);
        line->argCount++;
    }
    line->content = SVAsm_arg_copy(command->args[1]);
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, call->name);
    if(!list)
        list = translator->macros;
    SVAsm_macroList_add(list, macro);

    if(translator->options->warnings.shadowVariables &&
            (SVAsm_translator_getBuiltinMacro(call->name) || SVAsm_translator_getBuiltinVariable(call->name)))
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' shadows built-in macro", call->name);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 1);
        SVASM_TRANSLATOR_WARNING();
    }
    return true;
}

bool SVAsm_translator_translateSet(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount < 1 || command->argCount > 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT_RANGE("set", 1, 2, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG("set", 0, "symbol", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    char *symbol = command->args[0]->value.text;
    SVAsm_Macro *macro = SVAsm_macro_create(symbol, SVASM_MACRO_CONSTANT);
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_Arg *arg;
    if(command->argCount == 1)
    {
        SVAsm_NumberType value = 1;
        arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &value);
    }
    else
        arg = SVAsm_arg_copy(command->args[1]);
    SVAsm_macro_setConstant(macro, arg);
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
        list = translator->macros;
    SVAsm_macroList_add(list, macro);
    SVAsm_arg_destroy(arg);

    if(translator->options->warnings.shadowVariables &&
            (SVAsm_translator_getBuiltinMacro(symbol) || SVAsm_translator_getBuiltinVariable(symbol)))
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' shadows built-in macro", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    return true;
}

bool SVAsm_translator_translateAssign(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount < 1 || command->argCount > 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT_RANGE("assign", 1, 2, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("assign", 0, "string", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    char *symbol = command->args[0]->value.text;
    SVAsm_Macro *macro = SVAsm_macro_create(symbol, SVASM_MACRO_CONSTANT);
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_Arg *arg;
    if(command->argCount == 1)
    {
        SVAsm_NumberType value = 1;
        arg = SVAsm_arg_create(SVASM_ARG_NUMBER, &value);
    }
    else
        arg = SVAsm_arg_copy(command->args[1]);
    SVAsm_macro_setConstant(macro, arg);
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
        list = translator->macros;
    SVAsm_macroList_add(list, macro);
    SVAsm_arg_destroy(arg);

    if(translator->options->warnings.shadowVariables &&
            (SVAsm_translator_getBuiltinMacro(symbol) || SVAsm_translator_getBuiltinVariable(symbol)))
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' shadows built-in macro", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    return true;
}

bool SVAsm_translator_translateDelete(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("delete", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG("delete", 0, "symbol", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    char *symbol = command->args[0]->value.text;
    bool local = true;
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
    {
        list = translator->macros;
        local = false;
    }
    SVAsm_Macro *macro = SVAsm_macroList_get(list, symbol);
    if(!macro && translator->options->warnings.undefinedDelete)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    if(local)
    {
        SVAsm_Macro *undefinedMacro = SVAsm_macro_create(symbol, SVASM_MACRO_NONE);
        SVAsm_macroList_add(list, undefinedMacro);
    }
    else
        SVAsm_macroList_remove(translator->macros, symbol);
    return true;
}

bool SVAsm_translator_translateLocal(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount < 1 || command->argCount > 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT_RANGE("local", 1, 2, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG("local", 0, "symbol", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    char *symbol = command->args[0]->value.text;
    SVAsm_Macro *macro = NULL;
    if(command->argCount == 2)
    {
        macro = SVAsm_macro_create(symbol, SVASM_MACRO_CONSTANT);
        macro->filename = block->filename;
        macro->filepath = block->filepath;
        macro->line = block->line;
        SVAsm_macro_setConstant(macro, command->args[1]);
    }
    else
    {
        macro = SVAsm_macro_create(symbol, SVASM_MACRO_NONE);
        macro->filename = block->filename;
        macro->filepath = block->filepath;
        macro->line = block->line;
    }
    if(SVAsm_macroList_get(SVAsm_stack_top(translator->stack)->locals, symbol) != NULL)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' already local", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
        if(macro->type != SVASM_MACRO_NONE)
            SVAsm_macroList_add(SVAsm_stack_top(translator->stack)->locals, macro);
    }
    else
        SVAsm_macroList_add(SVAsm_stack_top(translator->stack)->locals, macro);
    return true;
}

bool SVAsm_translator_translateUnlocal(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("unlocal", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG("unlocal", 0, "symbol", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Macro *macro = SVAsm_macroList_get(SVAsm_stack_top(translator->stack)->locals, command->args[0]->value.text);
    if(!macro)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "constant \'%s\' undefined", command->args[0]->value.text);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    SVAsm_macroList_remove(SVAsm_stack_top(translator->stack)->locals, command->args[0]->value.text);
    return true;
}

bool SVAsm_translator_translateCall(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount < 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_MORE("call", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->args[0]->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("call", 0, "string", command->args[0]);
        SVASM_TRANSLATOR_ERROR();
    }
    char *symbol = command->args[0]->value.text;
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, symbol);
    if(!list)
        list = translator->macros;
    SVAsm_Macro *macro = SVAsm_macroList_get(list, symbol);
    if(!macro)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_WARNING();
    }
    if(macro->type == SVASM_MACRO_NONE)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' is local but undefined", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    if(macro->type != SVASM_MACRO_FUNCTION)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' called as code function", symbol);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
        SVASM_TRANSLATOR_ERROR();
    }
    size_t argCount = macro->value.function->argCount;
    if(command->argCount - 1 != argCount)
    {

        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                "macro \'%s\' requires %d parameters, %d supplied", symbol, argCount, command->argCount - 1);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_StackItemMacro *newItemMacro = SVAsm_stackItemMacro_create(macro);
    for(size_t i = 0; i < argCount; i++)
        newItemMacro->args[i] = SVAsm_arg_copy(command->args[i + 1]);
    SVAsm_StackItem *newItem = SVAsm_stackItem_create(SVASM_STACK_MACRO, newItemMacro);
    SVAsm_stack_push(translator->stack, newItem);
    return true;
}

bool SVAsm_translator_translateIf(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_ERR("if", 1, command->argCount, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }

    size_t startIf = index;
    size_t startElse = index;
    size_t endIf = index;
    if(!SVAsm_translator_getClosingEnd(translator, &endIf, &startElse))
        return false;

    SVAsm_Arg *arg = command->args[0];
    bool isTrue = SVAsm_arg_truth(arg);
    SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), endIf);
    SVAsm_StackItemCond *cond = NULL;
    if(isTrue)
        cond = SVAsm_stackItemCond_create(startIf, startIf);
    else
    {
        if(startIf != startElse)
            cond = SVAsm_stackItemCond_create(startIf, startElse);
        else
            return true;
    }
    SVAsm_stack_push(translator->stack, SVAsm_stackItem_create(SVASM_STACK_CONDITION, cond));
    return true;
}

bool SVAsm_translator_translateElse(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(SVAsm_stack_top(translator->stack)->type != SVASM_STACK_CONDITION)
    {
        SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                    "\'.else\' command outside of an \'.if\' block");
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_FATAL();
    }
    if(command->argCount > 0)
    {
        SVASM_TRANSLATOR_ARGCOUNT("else", 0, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_stack_pop(translator->stack);
    return true;
}

bool SVAsm_translator_translateFor(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount == 0 || command->argCount > 2)
    {
        SVASM_TRANSLATOR_ARGCOUNT_RANGE_ERR("for", 1, 2, command->argCount, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING && arg->type != SVASM_ARG_ARRAY)
    {
        SVASM_TRANSLATOR_BADARG_ERR("for", 0, "string or array", arg, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    if(command->argCount == 2 && command->args[1]->type != SVASM_ARG_SYMBOL)
    {
        SVASM_TRANSLATOR_BADARG_ERR("for", 1, "symbol", command->args[1], SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }

    size_t startLoop = index;
    size_t endLoop = index;
    if(!SVAsm_translator_getClosingEnd(translator, &endLoop, NULL))
        return false;

    char *name = command->argCount == 2 ? command->args[1]->value.text : NULL;
    SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), endLoop);
    if(arg->type == SVASM_ARG_STRING ? strlen(arg->value.text) > 0 : arg->value.array->argCount > 0)
    {
        SVAsm_StackItemForLoop *loop = SVAsm_stackItemForLoop_create(startLoop, arg, name);
        SVAsm_stack_push(translator->stack, SVAsm_stackItem_create(SVASM_STACK_LOOP_FOR, loop));
    }
    return true;
}

bool SVAsm_translator_translateWhile(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_ERR("while", 1, command->argCount, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }

    size_t startLoop = index;
    size_t endLoop = index;
    if(!SVAsm_translator_getClosingEnd(translator, &endLoop, NULL))
        return false;

    SVAsm_Arg *arg = command->args[0];
    bool isTrue = SVAsm_arg_truth(arg);
    SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), endLoop);
    if(isTrue)
    {
        SVAsm_StackItemWhileLoop *loop = SVAsm_stackItemWhileLoop_create(startLoop);
        SVAsm_stack_push(translator->stack, SVAsm_stackItem_create(SVASM_STACK_LOOP_WHILE, loop));
    }
    return true;
}


static bool SVAsm_translator_checkNestedMacros(struct SVAsm_Translator_ *translator, char const *macroName)
{
    if(!translator ||!macroName)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    for(size_t i = 0; i < translator->stack->itemCount; i++)
    {
        if((translator->stack->items[i]->type == SVASM_STACK_MACRO || translator->stack->items[i]->type == SVASM_STACK_BLOCK) &&
           strcmp(translator->stack->items[i]->value.macro->macro->name, macroName) == 0)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "nested macros with identical names");
            size_t outerMacroIdx = translator->stack->items[i]->value.macro->macro->value.function->start;
            SVAsm_Block *outerBlock = translator->blocks->blocks[outerMacroIdx];
            SVAsm_error(translator->options, SVASM_BLANK, outerBlock->filename, outerBlock->filepath, outerBlock->line,
                        "previous definition of macro \'%s\' :", translator->stack->items[i]->value.macro->macro->name);
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0));
            SVASM_TRANSLATOR_FATAL();
        }
    }
    return true;
}

bool SVAsm_translator_translateMacro(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_ERR("macro", 1, command->argCount, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    if(command->args[0]->type != SVASM_ARG_MACRO)
    {
        SVASM_TRANSLATOR_BADARG_ERR("macro", 1, "macro", command->args[0], SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    SVAsm_MacroCall *call = command->args[0]->value.macro;
    for(size_t i = 0; i < call->argCount; i++)
    {
        if(call->args[i]->type != SVASM_ARG_SYMBOL)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "macro prototype parameters must be symbols");
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 2 + i);
            SVASM_TRANSLATOR_ERROR();
        }
    }
    if(!SVAsm_translator_checkNestedMacros(translator, command->args[0]->value.macro->name))
        return false;

    size_t startMacro = index;
    size_t endMacro = index;
    if(!SVAsm_translator_getClosingEnd(translator, &endMacro, NULL))
        return false;

    SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), endMacro);
    SVAsm_Macro *macro = SVAsm_macro_create(command->args[0]->value.macro->name, SVASM_MACRO_FUNCTION);
    macro->value.function->start = startMacro;
    macro->value.function->argCount = call->argCount;
    MMALLOC(macro->value.function->args, SVAsm_Arg*, call->argCount);
    for(size_t i = 0; i < call->argCount; i++)
        macro->value.function->args[i] = SVAsm_arg_copy(call->args[i]);
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, call->name);
    if(!list)
        list = translator->macros;
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_macroList_add(list, macro);

    if(translator->options->warnings.shadowVariables &&
            (SVAsm_translator_getBuiltinMacro(call->name) || SVAsm_translator_getBuiltinVariable(call->name)))
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' shadows built-in macro", call->name);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 1);
        SVASM_TRANSLATOR_WARNING();
    }
    return true;
}

bool SVAsm_translator_translateBlock(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_ERR("block", 1, command->argCount, SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    if(command->args[0]->type != SVASM_ARG_MACRO)
    {
        SVASM_TRANSLATOR_BADARG_ERR("block", 1, "macro", command->args[0], SVASM_FATAL);
        SVASM_TRANSLATOR_FATAL();
    }
    SVAsm_MacroCall *call = command->args[0]->value.macro;
    for(size_t i = 0; i < call->argCount; i++)
    {
        if(call->args[i]->type != SVASM_ARG_SYMBOL)
        {
            SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                        "macro prototype parameters must be symbols");
            SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 2 + i);
            SVASM_TRANSLATOR_FATAL();
        }
    }
    if(!SVAsm_translator_checkNestedMacros(translator, command->args[0]->value.macro->name))
        return false;

    size_t startMacro = index;
    size_t endMacro = index;
    if(!SVAsm_translator_getClosingEnd(translator, &endMacro, NULL))
        return false;

    SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), endMacro);
    SVAsm_Macro *macro = SVAsm_macro_create(command->args[0]->value.macro->name, SVASM_MACRO_BLOCK);
    macro->value.function->start = startMacro;
    macro->value.function->argCount = call->argCount;
    MMALLOC(macro->value.function->args, SVAsm_Arg*, call->argCount);
    for(size_t i = 0; i < call->argCount; i++)
        macro->value.function->args[i] = SVAsm_arg_copy(call->args[i]);
    SVAsm_MacroList *list = SVAsm_expansion_getLocalMacroList(translator, call->name);
    if(!list)
        list = translator->macros;
    macro->filename = block->filename;
    macro->filepath = block->filepath;
    macro->line = block->line;
    SVAsm_macroList_add(list, macro);

    if(translator->options->warnings.shadowVariables &&
            (SVAsm_translator_getBuiltinMacro(call->name) || SVAsm_translator_getBuiltinVariable(call->name)))
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "macro \'%s\' shadows built-in macro", call->name);
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 1);
        SVASM_TRANSLATOR_WARNING();
    }
    return true;
}

bool SVAsm_translator_translateEnd(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    SVAsm_StackItem *top = SVAsm_stack_top(translator->stack);
    size_t index = SVAsm_stackItem_getLocation(top);
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount > 0)
    {
        SVASM_TRANSLATOR_ARGCOUNT_ERR("end", 0, command->argCount, SVASM_ERROR);
        SVASM_TRANSLATOR_ERROR();
    }
    switch(top->type)
    {
        case SVASM_STACK_ROOT:
            {
                SVAsm_error(translator->options, SVASM_FATAL, block->filename, block->filepath, block->line,
                            "command .end outside of a block");
                SVAsm_errorTranslator(translator->options, translator, block, 0);
                SVASM_TRANSLATOR_FATAL();
            }
            break;
        case SVASM_STACK_LOOP_FOR:
            {
                SVAsm_StackItemForLoop *loop = top->value.forLoop;
                loop->step++;
                if((loop->iterable->type == SVASM_ARG_STRING && loop->step >= strlen(loop->iterable->value.text)) ||
                   (loop->iterable->type == SVASM_ARG_ARRAY && loop->step >= loop->iterable->value.array->argCount))
                {
                    SVAsm_stack_pop(translator->stack);
                    return true;
                }
                loop->index = loop->start;
            }
            break;
        case SVASM_STACK_LOOP_WHILE:
            {
                SVAsm_StackItemWhileLoop *loop = top->value.whileLoop;
                size_t start = loop->start - 1;
                SVAsm_stack_pop(translator->stack);
                SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), start);
            }
            break;
        case SVASM_STACK_CONDITION:
            {
                SVAsm_StackItemCond *cond = top->value.condition;
                size_t start = cond->start;
                size_t startElse;
                bool ok = SVAsm_translator_getClosingEnd(translator, &start, &startElse);
                if(!ok)
                    SVASM_TRANSLATOR_FATAL();
                SVAsm_stack_pop(translator->stack);
                SVAsm_stackItem_setLocation(SVAsm_stack_top(translator->stack), start);
            }
            break;
        case SVASM_STACK_BLOCK:
            {
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "block command .return not reached");
                SVAsm_errorTranslator(translator->options, translator, block, 0);
                SVAsm_stack_pop(translator->stack);
                SVASM_TRANSLATOR_ERROR();
            }
            break;
        default:
            SVAsm_stack_pop(translator->stack);
            break;
    }
    return true;
}

bool SVAsm_translator_translateReturn(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("return", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(translator->returnValue != NULL)
    {
        SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                    "two .return commands in the same block macro");
        SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, 0) + 1);
        SVASM_TRANSLATOR_WARNING();
        SVAsm_arg_destroy(translator->returnValue);
    }
    translator->returnValue = SVAsm_arg_copy(command->args[0]);
    return true;
}

bool SVAsm_translator_translateMessage(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("message", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("message", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_error(translator->options, SVASM_INFO, block->filename, block->filepath, block->line,
                "%s", arg->value.text);
    return true;
}

bool SVAsm_translator_translateWarning(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{

    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount != 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT("warning", 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_Arg *arg = command->args[0];
    if(arg->type != SVASM_ARG_STRING)
    {
        SVASM_TRANSLATOR_BADARG("warning", 0, "string", arg);
        SVASM_TRANSLATOR_ERROR();
    }
    SVAsm_error(translator->options, SVASM_WARNING, block->filename, block->filepath, block->line,
                "%s", arg->value.text);
    SVASM_TRANSLATOR_WARNING();
    return true;
}

bool SVAsm_translator_translateError(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command)
{
    if(!translator || !command)
        return false;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    if(command->argCount > 1)
    {
        SVASM_TRANSLATOR_ARGCOUNT_RANGE("error", 0, 1, command->argCount);
        SVASM_TRANSLATOR_ERROR();
    }
    if(command->argCount == 1)
    {
        SVAsm_Arg *arg = command->args[0];
        if(arg->type != SVASM_ARG_STRING)
        {
            SVASM_TRANSLATOR_BADARG("error", 0, "string", arg);
            SVASM_TRANSLATOR_ERROR();
        }
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "%s", arg->value.text);
    }
    else
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                "error");
    SVAsm_errorTranslator(translator->options, translator, block, index);
    SVASM_TRANSLATOR_FATAL();
}


bool SVAsm_translator_getClosingEnd(struct SVAsm_Translator_ *translator, size_t *index, size_t *elseIndex)
{
    if(!translator || !index)
        return false;
    size_t blockCount = 0;
    bool end = false;
    size_t startBlock = *index;
    size_t startElse = *index;
    while(!end)
    {
        (*index)++;
        if(*index >= translator->blocks->blockCount)
        {
            SVAsm_error(translator->options, SVASM_FATAL, translator->blocks->blocks[startBlock]->filename,
                        translator->blocks->blocks[startBlock]->filepath, translator->blocks->blocks[startBlock]->line,
                        "missing closing \'.end\' command");
            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[startBlock], 0);
            SVASM_TRANSLATOR_FATAL();
        }
        SVAsm_Block *block = translator->blocks->blocks[*index];
        if(block->type == SVASM_BLOCK_COMMAND)
        {
            switch(block->content.command->command)
            {
                case SVASM_COMMAND_END:
                    if(blockCount == 0)
                        end = true;
                    else
                        blockCount--;
                    break;
                case SVASM_COMMAND_IF:
                case SVASM_COMMAND_FOR:
                case SVASM_COMMAND_WHILE:
                case SVASM_COMMAND_MACRO:
                case SVASM_COMMAND_BLOCK:
                    blockCount++;
                    break;
                case SVASM_COMMAND_ELSE:
                    if(blockCount == 0)
                    {
                        if(elseIndex)
                        {
                            if(startElse != startBlock)
                            {
                                SVAsm_error(translator->options, SVASM_FATAL, translator->blocks->blocks[*index]->filename,
                                            translator->blocks->blocks[*index]->filepath, translator->blocks->blocks[*index]->line,
                                            "multiple \'.else\' commands in an \'.if\' block");
                                SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[*index], 0);
                                SVASM_TRANSLATOR_FATAL();
                            }
                            startElse = *index;
                        }
                        else
                        {
                            SVAsm_error(translator->options, SVASM_FATAL, translator->blocks->blocks[*index]->filename,
                                        translator->blocks->blocks[*index]->filepath, translator->blocks->blocks[*index]->line,
                                        "\'.else\' command outside of an \'.if\' block");
                            SVAsm_errorTranslator(translator->options, translator, translator->blocks->blocks[*index], 0);
                            SVASM_TRANSLATOR_FATAL();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
    if(startElse != startBlock)
        *elseIndex = startElse;
    return true;
}
