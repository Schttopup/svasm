/**
 * \file translator_mac.h
 * \brief Declares functions related to svasm inline macros.
 * \author Schttopup
 */

#ifndef SVASM_TRANSLATOR_MAC_H
#define SVASM_TRANSLATOR_MAC_H

#include "../building/macrocall.h"
#include "macro.h"
#include "builtins.h"


/**
 * Code translator.
 */
struct SVAsm_Translator_;


/// Function name for built-in inline macro translations
extern SVAsm_MacroTranslation const SVAsm_BuiltinMacroTranslations[SVASM_BUILTINMAC_LAST];

/// List of built-in inline macro translations
SVAsm_BuiltinMacro SVAsm_translator_getBuiltinMacro(char const *symbol);


SVAsm_Arg* SVAsm_translator_builtinAdd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinSub(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNeg(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinMul(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinDiv(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinMod(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinEq(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNeq(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinLt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNlt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinGt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNgt(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinOr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinAnd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinXor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNot(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitOr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitAnd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitXor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitNot(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitShl(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitShr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitRol(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinBitRor(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinNum(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinStr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinOrd(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinChr(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinLen(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinGet(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinSet(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinPush(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinPop(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinConcat(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinSlice(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinVal(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinFetch(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinCall(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinDef(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinType(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);

SVAsm_Arg* SVAsm_translator_builtinTruth(struct SVAsm_Translator_ *translator, SVAsm_MacroCall *call);


#endif //SVASM_TRANSLATOR_MAC_H
