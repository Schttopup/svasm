/**
 * \file labelcall.c
 * \brief Defines functions related to label calls.
 * \author Schttopup
 */

#include "labelcall.h"
#include "../building/arg.h"
#include <sdefs.h>
#include <string.h>


SVAsm_LabelCall* SVAsm_labelCall_create(char const *label, bool negative, SVAsm_Arg *offset)
{
    if(!label)
        return NULL;
    SVAsm_LabelCall *call = NULL;
    MMALLOC(call, SVAsm_LabelCall, 1);
    call->label = NULL;
    MMALLOC(call->label, char, strlen(label) + 1);
    strcpy(call->label, label);
    call->negative = negative;
    call->offset = offset;
    return call;
}

void SVAsm_labelCall_destroy(SVAsm_LabelCall *label)
{
    if(!label)
        return;
    FFREE(label->label);
    FFREE(label);
}

SVAsm_LabelCall* SVAsm_labelCall_copy(SVAsm_LabelCall *label)
{
    if(!label)
        return NULL;
    SVAsm_LabelCall *newLabel = SVAsm_labelCall_create(label->label, label->negative, SVAsm_arg_copy(label->offset));
    return newLabel;
}
