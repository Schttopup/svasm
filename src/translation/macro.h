/**
 * \file macro.h
 * \brief Declares structures and functions related to macro calls.
 * \author Schttopup
 */

#ifndef SVASM_MACRO_H
#define SVASM_MACRO_H

#include "../building/arg.h"
#include <stdlib.h>


/**
 * Macro type.
 */
enum SVAsm_MacroType_
{
    SVASM_MACRO_NONE,       //!< No content
    SVASM_MACRO_CONSTANT,   //!< Constant value
    SVASM_MACRO_FUNCTION,   //!< Function
    SVASM_MACRO_LINE,       //!< Inline macro
    SVASM_MACRO_BLOCK,      //!< Block macro
};

/// Type name for macro types.
typedef enum SVAsm_MacroType_ SVAsm_MacroType;

struct SVAsm_Macro_;

/**
 * Macro constant value.
 */
struct SVAsm_MacroConstant_
{
    struct SVAsm_Macro_ *macro;     //!< Containing macro
    SVAsm_Arg *arg;                 //!< Value
};

/// Type name for macro constant values.
typedef struct SVAsm_MacroConstant_ SVAsm_MacroConstant;

/**
 * Function.
 */
struct SVAsm_MacroFunction_
{
    struct SVAsm_Macro_ *macro;     //!< Containing macro
    SVAsm_Arg **args;               //!< Function arguments
    size_t argCount;                //!< Function argument callCount
    size_t start;                   //!< Start block index
};

/// Type name for functions.
typedef struct SVAsm_MacroFunction_ SVAsm_MacroFunction;

/**
 * Inline macro.
 */
struct SVAsm_MacroLine_
{
    struct SVAsm_Macro_ *macro;     //!< Containing macro
    SVAsm_Arg **args;               //!< Macro arguments
    size_t argCount;                //!< Macro argument callCount
    SVAsm_Arg *content;             //!< Macro content
};

/// Type name for inline macros.
typedef struct SVAsm_MacroLine_ SVAsm_MacroLine;

/**
 * Block.
 */
struct SVAsm_MacroBlock_
{
    struct SVAsm_Macro_ *macro;     //!< Containing macro
    SVAsm_Arg **args;               //!< Function arguments
    size_t argCount;                //!< Function argument callCount
    size_t start;                   //!< Start block index
};

/// Type name for functions.
typedef struct SVAsm_MacroBlock_ SVAsm_MacroBlock;


/**
 * Macro.
 */
struct SVAsm_Macro_
{
    char *name;                         //!< Macro name
    SVAsm_MacroType type;               //!< Macro type
    union SVAsm_MacroValue_
    {
        SVAsm_MacroConstant *constant;  //!< Constant value
        SVAsm_MacroFunction *function;  //!< Function
        SVAsm_MacroLine *line;          //!< Inline macro
        SVAsm_MacroBlock *block;        //!< Block macro
    } value;                            //!< Macro value
    char const *filename;               //!< Definition file name
    char const *filepath;               //!< Definition file path
    unsigned int line;                  //!< Definition line
};

/// Type name for macros
typedef struct SVAsm_Macro_ SVAsm_Macro;


/**
 * Macro list.
 */
struct SVAsm_MacroList_
{
    SVAsm_Macro **macros;   //!< Macros
    size_t macroCount;      //!< Macro callCount
};
/// Type name for macro lists.
typedef struct SVAsm_MacroList_ SVAsm_MacroList;


/**
 * Creates a new macro.
 * \param name The macro name
 * \param type The macro type
 * \return The created macro
 */
SVAsm_Macro* SVAsm_macro_create(char const *name, SVAsm_MacroType type);

/**
 * Destroys a macro.
 * \param macro The macro to destroy
 */
void SVAsm_macro_destroy(SVAsm_Macro *macro);

/**
 * Sets a value to a constant macro.
 * \param macro The macro to update
 * \param arg The macro value
 */
void SVAsm_macro_setConstant(SVAsm_Macro *macro, SVAsm_Arg *arg);

/**
 * Creates a new macro list.
 * \return The created macro list
 */
SVAsm_MacroList* SVAsm_macroList_create(void);

/**
 * Destroys a macro list.
 * \param list The macro list to destroy
 */
void SVAsm_macroList_destroy(SVAsm_MacroList *list);

/**
 * Adds a macro to a macro list. If a macro with the same name already exists, the old macro is replaced.
 * \param list The macro list in which to add the macro
 * \param macro The macro to add
 */
void SVAsm_macroList_add(SVAsm_MacroList *list, SVAsm_Macro *macro);

/**
 * Removes a macro from a macro list.
 * \param list The macro list from which to remove the macro
 * \param name The name of the macro to remove
 */
void SVAsm_macroList_remove(SVAsm_MacroList *list, char const *name);

/**
 * Gets the corresponding macro for a given name from a macro list.
 * \param list The list in which to search for the macro
 * \param name The name of the macro
 * \return The corresponding macro, or NULL if no macro is found
 */
SVAsm_Macro* SVAsm_macroList_get(SVAsm_MacroList *list, char const *name);


#endif //SVASM_MACRO_H
