/**
 * \file translator_ins.h
 * \brief Declares structures and functions related to code translation for instructions.
 * \author Schttopup
 */

#ifndef SVASM_TRANSLATOR_INS_H
#define SVASM_TRANSLATOR_INS_H

#include "../building/instruction.h"
#include <stdbool.h>


/**
 * Code translator.
 */
struct SVAsm_Translator_;

/**
 * Instruction translation syntax.
 */
struct SVAsm_InstructionTranslation_
{
    SVAsm_Keyword keyword;                                  //!< Instruction keyword
    SVAsm_OperandType operands[2];                          //!< Operand types
    size_t operandCount;                                    //!< Operand callCount
    bool (*function)(struct SVAsm_Translator_*, SVAsm_Instruction*, uint8_t);   //!< Translation function
    uint8_t opcode;                                         //!< Instruction opcode
};

/// Type name for instruction translation syntaxes.
typedef struct SVAsm_InstructionTranslation_ SVAsm_InstructionTranslation;

/// Translation syntax list.
extern SVAsm_InstructionTranslation const SVAsm_instructionTranslations[];


/**
 * \brief Translates an operation with register and address operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsOpRA(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates an operation with address and register operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsOpAR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates an operation with two register operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsOpRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates an operation with a register operand.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsOpR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates an operation without operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsOp(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a memory load operation with two register operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsLdRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a memory store operation with two register operands.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsStRR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a jump with an address operand.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsJumpA(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a jump with a register operand.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsJumpR(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a LDI instruction.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsLDI(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a MV instruction.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsMV(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);

/**
 * \brief Translates a SYS instruction.
 * \param translator The code translator
 * \param opcode The opcode of the instruction
 * \return true if the translation was successful, false otherwise
 */
bool SVAsm_translator_translateInsSYS(struct SVAsm_Translator_ *translator, SVAsm_Instruction *instruction, uint8_t opcode);


#endif //SVASM_TRANSLATOR_INS_H
