#ifndef SVASM_MACRO_DEFS_H
#define SVASM_MACRO_DEFS_H


#define SVASM_TRANSLATOR_INVALIDWRITE() {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "invalid write space at 0x%04X", translator->location);\
    SVAsm_errorTranslator(translator->options, translator, block, 0);\
    }


#define SVASM_TRANSLATOR_ARGCOUNT(command, nbparams, nbsupplied) SVASM_TRANSLATOR_ARGCOUNT_ERR(command, nbparams, nbsupplied, SVASM_ERROR)
#define SVASM_TRANSLATOR_ARGCOUNT_ERR(command, nbparams, nbsupplied, lvl) {\
    SVAsm_error(translator->options, lvl, block->filename, block->filepath, block->line,\
                "command \'.%s\' requires %d parameters, %d supplied", (command), (nbparams), (nbsupplied));\
    SVAsm_errorTranslator(translator->options, translator, block, 0);\
    }

#define SVASM_TRANSLATOR_ARGCOUNT_MORE(command, nbparams, nbsupplied) SVASM_TRANSLATOR_ARGCOUNT_MORE_ERR(command, nbparams, nbsupplied, SVASM_ERROR)
#define SVASM_TRANSLATOR_ARGCOUNT_MORE_ERR(command, nbparams, nbsupplied, lvl) {\
    SVAsm_error(translator->options, lvl, block->filename, block->filepath, block->line,\
                "command \'.%s\' requires %d or more parameters, %d supplied", (command), (nbparams), (nbsupplied));\
    SVAsm_errorTranslator(translator->options, translator, block, 0);\
    }

#define SVASM_TRANSLATOR_ARGCOUNT_RANGE(command, minparams, maxparams, nbsupplied) SVASM_TRANSLATOR_ARGCOUNT_RANGE_ERR(command, minparams, maxparams, nbsupplied, SVASM_ERROR)
#define SVASM_TRANSLATOR_ARGCOUNT_RANGE_ERR(command, minparams, maxparams, nbsupplied, lvl) {\
    SVAsm_error(translator->options, lvl, block->filename, block->filepath, block->line,\
                "command \'.%s\' requires between %d and %d parameters, %d supplied", (command), (minparams), (maxparams), (nbsupplied));\
    SVAsm_errorTranslator(translator->options, translator, block, 0);\
    }

#define SVASM_TRANSLATOR_BADARG(command, param, ty, argty) SVASM_TRANSLATOR_BADARG_ERR(command, param, ty, argty, SVASM_ERROR)
#define SVASM_TRANSLATOR_BADARG_ERR(command, param, ty, argty, lvl) {\
    SVAsm_error(translator->options, lvl, block->filename, block->filepath, block->line,\
                "command \'.%s\' requires parameter %d to be of type %s, %s supplied", (command), (param + 1), (ty), SVAsm_argTypeValues[(argty)->type]);\
    SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, param));\
    }

#define SVASM_TRANSLATOR_BADARG_EXP(command, param) {\
    SVAsm_error(translator->options, SVASM_BLANK, block->filename, block->filepath, block->line,\
                "in command \'.%s\'", (command));\
    SVAsm_errorTranslator(translator->options, translator, block, SVAsm_error_getParam(block, param));\
    }

#define SVASM_TRANSLATOR_NESTED(command) SVASM_TRANSLATOR_NESTED_ERR(command, SVASM_ERROR)
#define SVASM_TRANSLATOR_NESTED_ERR(command, lvl) {\
    SVAsm_error(translator->options, lvl, block->filename, block->filepath, block->line,\
                "command \'.%s\' cannot be used in block macros", (command));\
    SVAsm_errorTranslator(translator->options, translator, block, 1);\
    }


#define SVASM_TRANSLATOR_WARNING_ERRIF(flag) { translator->warning = true; if((flag) || translator->options->warningError) SVASM_TRANSLATOR_ERROR(); }
#define SVASM_TRANSLATOR_WARNING() { translator->warning = true; if(translator->options->warningError) SVASM_TRANSLATOR_ERROR(); }
#define SVASM_TRANSLATOR_ERROR() { translator->error = true; if(translator->options->fatalError) SVASM_TRANSLATOR_FATAL(); return false; }
#define SVASM_TRANSLATOR_FATAL() { translator->error = true; translator->stop = true; return false; }


#define SVASM_TRANSLATOR_PARAMCOUNT(macro, call, nbparams, nbsupplied) {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "built-in macro \'%s\' requires %d parameters, %d supplied", (macro), (nbparams), (nbsupplied));\
    SVAsm_errorMacro(translator->options, call, 0);\
    }

#define SVASM_TRANSLATOR_PARAMCOUNT_MORE(macro, call, nbparams, nbsupplied) {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "built-in macro \'%s\' requires %d or more parameters, %d supplied", (macro), (nbparams), (nbsupplied));\
    SVAsm_errorMacro(translator->options, call, 0);\
    }

#define SVASM_TRANSLATOR_PARAMCOUNT_RANGE(macro, call, minparams, maxparams, nbsupplied) {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "built-in macro \'%s\' requires between %d and %d parameters, %d supplied", (macro), (minparams), (maxparams), (nbsupplied));\
    SVAsm_errorMacro(translator->options, call, 0);\
    }

#define SVASM_TRANSLATOR_BADPARAM(macro, call, param, ty, argty) {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "built-in macro \'%s\' requires parameter %d to be of type %s, %s supplied", (macro), (param + 1), (ty), SVAsm_argTypeValues[(argty)->type]);\
    SVAsm_errorMacro(translator->options, call, (param + 2));\
    }

#define SVASM_TRANSLATOR_BADPARAMS(macro, call, param, ty, argty) {\
    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,\
                "built-in macro \'%s\' requires its parameters to be of type %s, %s supplied", (macro), (ty), SVAsm_argTypeValues[(argty)->type]);\
    SVAsm_errorMacro(translator->options, call, (param + 2));\
    }


#define SVASM_TRANSLATOR_MAC_WARNING_ERRIF(flag) { translator->warning = true; if((flag) || translator->options->warningError) SVASM_TRANSLATOR_MAC_ERROR(); }
#define SVASM_TRANSLATOR_MAC_WARNING() { translator->warning = true; if(translator->options->warningError) SVASM_TRANSLATOR_MAC_ERROR(); }
#define SVASM_TRANSLATOR_MAC_ERROR() { translator->error = true; if(translator->options->fatalError) SVASM_TRANSLATOR_MAC_FATAL(); return NULL; }
#define SVASM_TRANSLATOR_MAC_FATAL() { translator->error = true; translator->stop = true; return NULL; }


#endif //SVASM_MACRO_DEFS_H
