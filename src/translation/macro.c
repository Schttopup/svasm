/**
 * \file macro.c
 * \brief Defines functions related to macro calls.
 * \author Schttopup
 */

#include "macro.h"
#include <sdefs.h>
#include <string.h>
#include <stdbool.h>


SVAsm_Macro* SVAsm_macro_create(char const *name, SVAsm_MacroType type)
{
    if(!name)
        return NULL;
    SVAsm_Macro *macro = NULL;
    MMALLOC(macro, SVAsm_Macro, 1);
    macro->name = NULL;
    MMALLOC(macro->name, char, strlen(name) + 1);
    strcpy(macro->name, name);
    macro->type = type;
    switch(type)
    {
        case SVASM_MACRO_CONSTANT:
            macro->value.constant = NULL;
            MMALLOC(macro->value.constant, SVAsm_MacroConstant, 1);
            macro->value.constant->macro = macro;
            macro->value.constant->arg = NULL;
            break;
        case SVASM_MACRO_FUNCTION:
        case SVASM_MACRO_BLOCK:
            macro->value.function = NULL;
            MMALLOC(macro->value.function, SVAsm_MacroFunction, 1);
            macro->value.function->macro = macro;
            macro->value.function->start = 0;
            macro->value.function->argCount = 0;
            macro->value.function->args = NULL;
            break;
        case SVASM_MACRO_LINE:
            macro->value.line = NULL;
            MMALLOC(macro->value.line, SVAsm_MacroLine, 1);
            macro->value.line->macro = macro;
            macro->value.line->argCount = 0;
            macro->value.line->args = NULL;
            macro->value.line->content = NULL;
            break;
        default:
            break;
    }
    macro->filename = NULL;
    macro->filepath = NULL;
    macro->line = 0;
    return macro;
}

void SVAsm_macro_destroy(SVAsm_Macro *macro)
{
    if(!macro)
        return;
    switch(macro->type)
    {
        case SVASM_MACRO_CONSTANT:
            if(macro->value.constant->arg)
                SVAsm_arg_destroy(macro->value.constant->arg);
            FFREE(macro->value.constant);
            break;
        case SVASM_MACRO_FUNCTION:
        case SVASM_MACRO_BLOCK:
            for(size_t i = 0; i < macro->value.function->argCount; i++)
                SVAsm_arg_destroy(macro->value.function->args[i]);
            FFREE(macro->value.function->args);
            FFREE(macro->value.function);
            break;
        case SVASM_MACRO_LINE:
            for(size_t i = 0; i < macro->value.line->argCount; i++)
                SVAsm_arg_destroy(macro->value.line->args[i]);
            SVAsm_arg_destroy(macro->value.line->content);
            FFREE(macro->value.line->args);
            FFREE(macro->value.line);
        default:
            break;
    }
    FFREE(macro->name);
    FFREE(macro);
}

void SVAsm_macro_setConstant(SVAsm_Macro *macro, SVAsm_Arg *arg)
{
    if(!macro || !arg)
        return;
    if(macro->type != SVASM_MACRO_CONSTANT)
        return;
    if(macro->value.constant->arg != NULL)
        SVAsm_arg_destroy(macro->value.constant->arg);
    macro->value.constant->arg = SVAsm_arg_copy(arg);
}


SVAsm_MacroList* SVAsm_macroList_create(void)
{
    SVAsm_MacroList *list = NULL;
    MMALLOC(list, SVAsm_MacroList, 1);
    list->macroCount = 0;
    list->macros = NULL;
    return list;
}

void SVAsm_macroList_destroy(SVAsm_MacroList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->macroCount; i++)
        SVAsm_macro_destroy(list->macros[i]);
    FFREE(list->macros);
    FFREE(list);
}

void SVAsm_macroList_add(SVAsm_MacroList *list, SVAsm_Macro *macro)
{
    if(!list || !macro)
        return;
    for(size_t i = 0; i < list->macroCount; i++)
    {
        if(strcmp(list->macros[i]->name, macro->name) == 0)
        {
            SVAsm_macro_destroy(list->macros[i]);
            list->macros[i] = macro;
            return;
        }
    }
    RREALLOC(list->macros, SVAsm_Macro*, list->macroCount + 1);
    list->macros[list->macroCount] = macro;
    list->macroCount++;
}

void SVAsm_macroList_remove(SVAsm_MacroList *list, char const *name)
{
    if(!list || !name)
        return;
    bool found = false;
    for(size_t i = 0; i < list->macroCount; i++)
    {
        if(!found && strcmp(list->macros[i]->name, name) == 0)
        {
            found = true;
            SVAsm_macro_destroy(list->macros[i]);
        }
        if(found && i < list->macroCount - 1)
            list->macros[i] = list->macros[i + 1];
    }
    list->macroCount--;
    RREALLOC(list->macros, SVAsm_Macro*, list->macroCount);
}

SVAsm_Macro* SVAsm_macroList_get(SVAsm_MacroList *list, char const *name)
{
    if(!list || !name)
        return NULL;
    for(size_t i = 0; i < list->macroCount; i++)
    {
        if(strcmp(list->macros[i]->name, name) == 0)
            return list->macros[i];
    }
    return NULL;
}
