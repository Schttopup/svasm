/**
 * \file translator_var.h
 * \brief Declares functions related to svasm variable macros.
 * \author Schttopup
 */

#ifndef SVASM_TRANSLATOR_VAR_H
#define SVASM_TRANSLATOR_VAR_H


#include "../building/arg.h"
#include "builtins.h"


/**
 * Code translator.
 */
struct SVAsm_Translator_;


/// Function name for built-in variable macro translations
typedef SVAsm_Arg* (*SVAsm_BuiltinVariableTranslation)(struct SVAsm_Translator_*);

/// List of built-in variable macro translations
extern SVAsm_BuiltinVariableTranslation const SVAsm_BuiltinVariableTranslations[SVASM_BUILTINVAR_LAST];

/**
 * Gets the built-in variable corresponding to a symbol
 * \param symbol The symbol to search
 * \return The corresponding built-in variable, or SVASM_BUILTINVAR_NONE if the symbol does not correspond to any
 */
SVAsm_BuiltinVariable SVAsm_translator_getBuiltinVariable(char const *symbol);


SVAsm_Arg* SVAsm_translator_builtinAddress(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinVarStart(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinVarSize(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinConstSize(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinStackStart(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinStackSize(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinCounter(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinNil(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinFile(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinPath(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinBaseFile(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinBasePath(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinLine(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinLevel(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinTime(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinAsmPrg(struct SVAsm_Translator_ *translator);

SVAsm_Arg* SVAsm_translator_builtinAsmVer(struct SVAsm_Translator_ *translator);


#endif //SVASM_TRANSLATOR_VAR_H
