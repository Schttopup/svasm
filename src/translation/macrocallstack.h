/**
 * \file macrocallstack.h
 * \brief Declares structures and functions related to macro call expansion stacks.
 * \author Schttopup
 */

#ifndef SVASM_MACROCALLSTACK_H
#define SVASM_MACROCALLSTACK_H

#include "macro.h"
#include "builtins.h"
#include "../building/arg.h"
#include "../building/command.h"
#include "../building/macrocall.h"


/**
 * Macro call stack item type.
 */
enum SVAsm_MacroCallStackItemType_
{
    SVASM_MACROCALLSTACKITEM_NONE,          //!< No content
    SVASM_MACROCALLSTACKITEM_LINE,          //!< Inline macro
    SVASM_MACROCALLSTACKITEM_BUILTIN,       //!< Built-in macro
    SVASM_MACROCALLSTACKITEM_LABEL,         //!< Label expansion
    SVASM_MACROCALLSTACKITEM_ARRAY,         //!< Array expansion
    SVASM_MACROCALLSTACKITEM_BLOCK,         //!< Block macro
    SVASM_MACROCALLSTACKITEM_BLOCKPARAMS,   //!< Block parameters
    SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND,  //!< Block macro step
    SVASM_MACROCALLSTACKITEM_BLOCKMACRO     //!< Block macro step
};

/// Type name for macro call stack item types.
typedef enum SVAsm_MacroCallStackItemType_ SVAsm_MacroCallStackItemType;

/**
 * Macro call stack item.
 */
struct SVAsm_MacroCallStackItem_
{
    SVAsm_MacroCallStackItemType type;  //!< Item type
    union SVAsm_MacroCallStackItemValue_
    {
        SVAsm_MacroLine *line;          //!< Inline macro
        struct SVAsm_MacroCallStackItemValueBuiltin_
        {
            SVAsm_BuiltinMacro macro;           //!< Built-in macro
            SVAsm_MacroTranslation function;    //!< Built-in macro function
        } builtin;                      //!< Built-in macro
        struct SVAsm_MacroCallStackItemValueBlock_
        {
            struct SVAsm_Translator_ *translator;   //!< Nested translator
            SVAsm_CommandLine *command;             //!< Command
            SVAsm_MacroBlock *block;
        } block;                        //!< Block macro
    } value;                            //!< Item value
    char *valueName;                    //!< Macro content name
    union SVAsm_MacroCallStackItemOrigin_
    {
        SVAsm_MacroCall *call;          //!< Macro call
        SVAsm_Array *array;             //!< Arg array
        SVAsm_LabelCall *label;         //!< Arg array
        SVAsm_CommandLine *command;     //!< Command line
    } origin;                           //!< Item origin
    SVAsm_Macro *macro;                 //!< Corresponding macro
    SVAsm_Arg **args;                   //!< Current arguments
    size_t index;                       //!< Current index
    size_t depth;                       //!< Item depth
};

/// Type name for macro call stack items.
typedef struct SVAsm_MacroCallStackItem_ SVAsm_MacroCallStackItem;


/**
 * Macro call stack.
 */
struct SVAsm_MacroCallStack_
{
    SVAsm_MacroCallStackItem **items;   //!< Stack items
    size_t itemCount;                   //!< Stack item count
    size_t depth;                       //!< Stack depth
};

/// Type name for macro call stacks.
typedef struct SVAsm_MacroCallStack_ SVAsm_MacroCallStack;


/**
 * Creates a new macro call stack item.
 * \return The created macro call stack item
 */
SVAsm_MacroCallStackItem* SVAsm_macroCallStackItem_create(void);

/**
 * Destroys a macro call stack item.
 * \param item The macro call stack item to destroy
 */
void SVAsm_macroCallStackItem_destroy(SVAsm_MacroCallStackItem *item);


/**
 * Creates a macro call stack.
 * \return The created macro call stack
 */
SVAsm_MacroCallStack* SVAsm_macroCallStack_create(void);

/**
 * Destroys a macro call stack.
 * \param stack The macro call stack to destroy
 */
void SVAsm_macroCallStack_destroy(SVAsm_MacroCallStack *stack);

/**
 * Pushes an item on a macro call stack.
 * \param stack The stack
 * \param item The item to push
 */
void SVAsm_macroCallStack_push(SVAsm_MacroCallStack *stack, SVAsm_MacroCallStackItem *item);

/**
 * Pops an item from a macro call stack.
 * \param stack The stack.
 */
void SVAsm_macroCallStack_pop(SVAsm_MacroCallStack *stack);

/**
 * Gets the top of a macro call stack.
 * \param stack The stack
 * \return The item on top of the stack
 */
SVAsm_MacroCallStackItem* SVAsm_macroCallStack_top(SVAsm_MacroCallStack *stack);


#endif //SVASM_MACROCALLSTACK_H
