/**
 * \file builtins.c
 * \brief Defines functions related to svasm built-in variable and inline macros.
 * \author Schttopup
 */

#include "builtins.h"


char const * const SVAsm_BuiltinMacroNames[SVASM_BUILTINMAC_LAST] = {
        "",
        "_add",
        "_sub",
        "_neg",
        "_mul",
        "_div",
        "_mod",
        "_eq",
        "_neq",
        "_lt",
        "_nlt",
        "_gt",
        "_ngt",
        "_or",
        "_and",
        "_xor",
        "_not",
        "_bit_or",
        "_bit_and",
        "_bit_xor",
        "_bit_not",
        "_bit_shl",
        "_bit_shr",
        "_bit_rol",
        "_bit_ror",
        "_num",
        "_str",
        "_ord",
        "_chr",
        "_len",
        "_get",
        "_set",
        "_push",
        "_pop",
        "_concat",
        "_slice",
        "_val",
        "_fetch",
        "_call",
        "_def",
        "_type",
        "_truth"
};


char const * const SVAsm_BuiltinVariableNames[SVASM_BUILTINVAR_LAST] = {
        "",
        "_address",
        "_var_start",
        "_var_size",
        "_const_size",
        "_stack_start",
        "_stack_size",
        "_counter",
        "_nil",
        "_file",
        "_path",
        "_base_file",
        "_base_path",
        "_line",
        "_level",
        "_time",
        "_asm_prg",
        "_asm_ver"
};
