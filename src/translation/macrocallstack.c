/**
 * \file macrocallstack.c
 * \brief Defines functions related to macro call expansion stacks.
 * \author Schttopup
 */

#include "macrocallstack.h"
#include "translator.h"
#include <sdefs.h>


SVAsm_MacroCallStackItem* SVAsm_macroCallStackItem_create(void)
{
    SVAsm_MacroCallStackItem *item = NULL;
    MMALLOC(item, SVAsm_MacroCallStackItem, 1);
    item->type = SVASM_MACROCALLSTACKITEM_NONE;
    item->valueName = NULL;
    item->macro = NULL;
    item->args = NULL;
    item->index = 0;
    item->depth = 1;
    item->value.line = NULL;
    item->value.block.translator = NULL;
    item->value.block.command = NULL;
    item->value.block.block = NULL;
    item->value.builtin.macro = SVASM_BUILTINMAC_NONE;
    item->value.builtin.function = NULL;
    return item;
}

void SVAsm_macroCallStackItem_destroy(SVAsm_MacroCallStackItem *item)
{
    if(!item)
        return;
    if(item->type != SVASM_MACROCALLSTACKITEM_NONE)
    {
        size_t argCount = 0;
        switch(item->type)
        {
            case SVASM_MACROCALLSTACKITEM_LINE:
            case SVASM_MACROCALLSTACKITEM_BUILTIN:
            case SVASM_MACROCALLSTACKITEM_BLOCKPARAMS:
            case SVASM_MACROCALLSTACKITEM_BLOCKMACRO:
                argCount = item->origin.call->argCount;
                break;
            case SVASM_MACROCALLSTACKITEM_ARRAY:
                argCount = item->origin.array->argCount;
                break;
            case SVASM_MACROCALLSTACKITEM_BLOCK:
                argCount = 1;
                if(item->value.block.translator && item->value.block.translator->nested)
                    SVAsm_translator_destroy(item->value.block.translator);
                break;
            case SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND:
                argCount = item->value.block.command->argCount;
                SVAsm_commandLine_destroy(item->value.block.command);
                break;
            default:
                break;
        }
        for(size_t i = 0; i < argCount; i++)
            SVAsm_arg_destroy(item->args[i]);
        FFREE(item->args);
        FFREE(item->valueName);
    }
    FFREE(item);
}

SVAsm_MacroCallStack* SVAsm_macroCallStack_create(void)
{
    SVAsm_MacroCallStack *stack = NULL;
    MMALLOC(stack, SVAsm_MacroCallStack, 1);
    stack->items = NULL;
    stack->itemCount = 0;
    stack->depth = 0;
    return stack;
}

void SVAsm_macroCallStack_destroy(SVAsm_MacroCallStack *stack)
{
    if(!stack)
        return;
    for(size_t i = 0; i < stack->itemCount; i++)
        SVAsm_macroCallStackItem_destroy(stack->items[i]);
    FFREE(stack->items);
    FFREE(stack);
}

void SVAsm_macroCallStack_push(SVAsm_MacroCallStack *stack, SVAsm_MacroCallStackItem *item)
{
    if(!stack || !item)
        return;
    RREALLOC(stack->items, SVAsm_MacroCallStackItem*, stack->itemCount + 1);
    stack->items[stack->itemCount] = item;
    stack->itemCount++;
    stack->depth += item->depth;
}

void SVAsm_macroCallStack_pop(SVAsm_MacroCallStack *stack)
{
    if(!stack)
        return;
    if(stack->itemCount == 0)
        return;
    stack->depth -= stack->items[stack->itemCount - 1]->depth;
    SVAsm_macroCallStackItem_destroy(stack->items[stack->itemCount - 1]);
    stack->itemCount--;
    RREALLOC(stack->items, SVAsm_MacroCallStackItem*, stack->itemCount);
}

SVAsm_MacroCallStackItem* SVAsm_macroCallStack_top(SVAsm_MacroCallStack *stack)
{
    if(!stack)
        return NULL;
    if(stack->itemCount == 0)
        return NULL;
    return stack->items[stack->itemCount - 1];
}
