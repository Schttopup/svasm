/**
 * \file stack.h
 * \brief Declares structures and functions related to translator stacks.
 * \author Schttopup
 */

#ifndef SVASM_STACK_H
#define SVASM_STACK_H

#include "macro.h"
#include "../building/macrocall.h"
#include <stdlib.h>

/**
 * Translator stack item type.
 */
enum SVAsm_StackItemType_
{
    SVASM_STACK_NONE,       //!< No type
    SVASM_STACK_ROOT,       //!< Root level
    SVASM_STACK_MACRO,      //!< Function macro
    SVASM_STACK_BLOCK,      //!< Block macro
    SVASM_STACK_LOOP_FOR,   //!< For loop
    SVASM_STACK_LOOP_WHILE, //!< While loop
    SVASM_STACK_CONDITION   //!< Condition
};

/// Type name for translator stack item types.
typedef enum SVAsm_StackItemType_ SVAsm_StackItemType;

/**
 * Translator root stack item.
 */
struct SVAsm_StackItemRoot_
{
    size_t index;   //!< Block index
};

/// Type name for translator root stack items.
typedef struct SVAsm_StackItemRoot_ SVAsm_StackItemRoot;

/**
 * Translator macro stack item.
 */
struct SVAsm_StackItemMacro_
{
    SVAsm_Macro *macro;     //!< Called macro
    size_t index;           //!< Block index
    SVAsm_Arg **args;       //!< Macro args
};

/// Type name for translator macro stack items.
typedef struct SVAsm_StackItemMacro_ SVAsm_StackItemMacro;

/**
 * Translator loop stack item.
 */
struct SVAsm_StackItemForLoop_
{
    size_t start;           //!< Loop start block index
    size_t step;            //!< Iteration step
    SVAsm_Arg *iterable;    //!< Loop iterable
    char *name;             //!< Loop variable name
    size_t index;           //!< Block index
};

/// Type name for translator loop stack items.
typedef struct SVAsm_StackItemForLoop_ SVAsm_StackItemForLoop;

/**
 * Translator loop stack item.
 */
struct SVAsm_StackItemWhileLoop_
{
    size_t start;   //!< Loop start block index
    size_t index;   //!< Block index
};

/// Type name for translator loop stack items.
typedef struct SVAsm_StackItemWhileLoop_ SVAsm_StackItemWhileLoop;

/**
 * Translator condition stack item.
 */
struct SVAsm_StackItemCondition_
{
    size_t start;   //!< Condition start block index
    size_t index;   //!< Block index
};

/// Type name for translator condition stack items.
typedef struct SVAsm_StackItemCondition_ SVAsm_StackItemCond;

/**
 * Translator stack item.
 */
struct SVAsm_StackItem_
{
    SVAsm_StackItemType type;                   //!< Item type
    union SVAsm_StackItemValue_
    {
        SVAsm_StackItemRoot *root;              //!< Root level
        SVAsm_StackItemMacro *macro;            //!< Macro
        SVAsm_StackItemForLoop *forLoop;        //!< For loop
        SVAsm_StackItemWhileLoop *whileLoop;    //!< While loop
        SVAsm_StackItemCond *condition;         //!< Condition
    } value;                                    //!< Item value
    SVAsm_MacroList *locals;                    //!< Local macro list
};

/// Type name for translator stack items.
typedef struct SVAsm_StackItem_ SVAsm_StackItem;

/**
 * Translator stack.
 */
struct SVAsm_Stack_
{
    SVAsm_StackItem **items;    //!< Stack items
    size_t itemCount;           //!< Stack item count
};

/// Type name for translator stacks.
typedef struct SVAsm_Stack_ SVAsm_Stack;


/**
 * Creates a translator stack item.
 * \param type The item type
 * \param value The item value
 * \return The created translator stack item
 */
SVAsm_StackItem* SVAsm_stackItem_create(SVAsm_StackItemType type, void *value);

/**
 * Destroys a translator stack item.
 * \param item The item to destroy
 */
void SVAsm_stackItem_destroy(SVAsm_StackItem *item);

/**
 * Gets the block index of a translator stack item.
 * \param item The translator stack item
 * \return The corresponding block index
 */
size_t SVAsm_stackItem_getLocation(SVAsm_StackItem *item);

/**
 * Sets the block index of a translator stack item.
 * \param item The translator stack item
 * \param location The new block index
 */
void SVAsm_stackItem_setLocation(SVAsm_StackItem *item, size_t location);

/**
 * Creates a new translator root stack item.
 * \param location The starting block index
 * \return The created translator root stack item
 */
SVAsm_StackItemRoot* SVAsm_stackItemRoot_create(size_t location);

/**
 * Destroys a translator root stack item.
 * \param item The translator root stack item to destroy
 */
void SVAsm_stackItemRoot_destroy(SVAsm_StackItemRoot *item);

/**
 * Creates a new translator macro stack item.
 * \param macro The macro for the item
 * \return The created translator macro stack item
 */
SVAsm_StackItemMacro* SVAsm_stackItemMacro_create(SVAsm_Macro *macro);

/**
 * Destroys a translator macro stack item.
 * \param item The translator root stack item to destroy
 */
void SVAsm_stackItemMacro_destroy(SVAsm_StackItemMacro *item);

/**
 * Creates a new translator for loop stack item
 * \param start The start block index
 * \param end The end block index
 * \param steps The number of steps
 * \param name The name of the counter
 * \return The created translator loop stack item
 */
SVAsm_StackItemForLoop* SVAsm_stackItemForLoop_create(size_t start, SVAsm_Arg *iterable, char *name);

/**
 * Destroys a translator for loop stack item.
 * \param item The translator root stack item to destroy
 */
void SVAsm_stackItemForLoop_destroy(SVAsm_StackItemForLoop *item);

/**
 * Creates a new translator while loop stack item
 * \param start The start block index
 * \return The created translator loop stack item
 */
SVAsm_StackItemWhileLoop* SVAsm_stackItemWhileLoop_create(size_t start);

/**
 * Destroys a translator while loop stack item.
 * \param item The translator root stack item to destroy
 */
void SVAsm_stackItemWhileLoop_destroy(SVAsm_StackItemWhileLoop *item);

/**
 * Creates a new translator condition stack item
 * \param start The start block index
 * \return The created translator condition stack item
 */
SVAsm_StackItemCond* SVAsm_stackItemCond_create(size_t start, size_t index);

/**
 * Destroys a translator condition stack item.
 * \param item The translator root stack item to destroy
 */
void SVAsm_stackItemCond_destroy(SVAsm_StackItemCond *item);


/**
 * Creates a new translator stack.
 * \return The created stack
 */
SVAsm_Stack* SVAsm_stack_create(void);

/**
 * Destroys a translator stack
 * \param stack The translator stack to destroy
 */
void SVAsm_stack_destroy(SVAsm_Stack *stack);

/**
 * Pushes an item on a translator stack
 * \param stack The translator stack
 * \param item The item to push
 */
void SVAsm_stack_push(SVAsm_Stack *stack, SVAsm_StackItem *item);

/**
 * Pops an item from a translator stack
 * \param stack The translator stack
 */
void SVAsm_stack_pop(SVAsm_Stack *stack);

/**
 * Gets the top item of a translator stack
 * \param stack The translator stack
 * \return The top item of the stack
 */
SVAsm_StackItem* SVAsm_stack_top(SVAsm_Stack *stack);


#endif //SVASM_STACK_H
