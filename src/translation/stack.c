/**
 * \file stack.c
 * \brief Defines functions related to translator stacks.
 * \author Schttopup
 */

#include "stack.h"
#include <sdefs.h>
#include <string.h>


SVAsm_StackItem* SVAsm_stackItem_create(SVAsm_StackItemType type, void *value)
{
    if(type == SVASM_STACK_NONE || !value)
        return NULL;
    SVAsm_StackItem *item = NULL;
    MMALLOC(item, SVAsm_StackItem, 1);
    item->type = type;
    switch(type)
    {
        case SVASM_STACK_ROOT:
            item->value.root = (SVAsm_StackItemRoot*)value;
            break;
        case SVASM_STACK_MACRO:
        case SVASM_STACK_BLOCK:
            item->value.macro = (SVAsm_StackItemMacro*)value;
            break;
        case SVASM_STACK_LOOP_FOR:
            item->value.forLoop = (SVAsm_StackItemForLoop*)value;
            break;
        case SVASM_STACK_LOOP_WHILE:
            item->value.whileLoop = (SVAsm_StackItemWhileLoop*)value;
            break;
        case SVASM_STACK_CONDITION:
            item->value.condition = (SVAsm_StackItemCond*)value;
            break;
        default:
            break;
    }
    item->locals = SVAsm_macroList_create();
    return item;
}

void SVAsm_stackItem_destroy(SVAsm_StackItem *item)
{
    if(!item)
        return;
    switch(item->type)
    {
        case SVASM_STACK_ROOT:
            SVAsm_stackItemRoot_destroy(item->value.root);
            break;
        case SVASM_STACK_MACRO:
        case SVASM_STACK_BLOCK:
            SVAsm_stackItemMacro_destroy(item->value.macro);
            break;
        case SVASM_STACK_LOOP_FOR:
            SVAsm_stackItemForLoop_destroy(item->value.forLoop);
            break;
        case SVASM_STACK_LOOP_WHILE:
            SVAsm_stackItemWhileLoop_destroy(item->value.whileLoop);
            break;
        case SVASM_STACK_CONDITION:
            SVAsm_stackItemCond_destroy(item->value.condition);
            break;
        default:
            break;
    }
    SVAsm_macroList_destroy(item->locals);
    FFREE(item);
}

size_t SVAsm_stackItem_getLocation(SVAsm_StackItem *item)
{
    if(!item)
        return 0;
    switch(item->type)
    {
        case SVASM_STACK_ROOT:
            return item->value.root->index;
        case SVASM_STACK_MACRO:
        case SVASM_STACK_BLOCK:
            return item->value.macro->index;
        case SVASM_STACK_LOOP_FOR:
            return item->value.forLoop->index;
        case SVASM_STACK_LOOP_WHILE:
            return item->value.whileLoop->index;
        case SVASM_STACK_CONDITION:
            return item->value.condition->index;
        default:
            return 0;
    }
}

void SVAsm_stackItem_setLocation(SVAsm_StackItem *item, size_t location)
{
    if(!item)
        return;
    switch(item->type)
    {
        case SVASM_STACK_ROOT:
            item->value.root->index = location;
            break;
        case SVASM_STACK_MACRO:
        case SVASM_STACK_BLOCK:
            item->value.macro->index = location;
            break;
        case SVASM_STACK_LOOP_FOR:
            item->value.forLoop->index = location;
            break;
        case SVASM_STACK_LOOP_WHILE:
            item->value.whileLoop->index = location;
            break;
        case SVASM_STACK_CONDITION:
            item->value.condition->index = location;
            break;
        default:
            break;
    }
}


SVAsm_StackItemRoot* SVAsm_stackItemRoot_create(size_t location)
{
    SVAsm_StackItemRoot *item = NULL;
    MMALLOC(item, SVAsm_StackItemRoot, 1);
    item->index = location;
    return item;
}

void SVAsm_stackItemRoot_destroy(SVAsm_StackItemRoot *item)
{
    if(!item)
        return;
    FFREE(item);
}

SVAsm_StackItemMacro* SVAsm_stackItemMacro_create(SVAsm_Macro *macro)
{
    if(!macro)
        return NULL;
    SVAsm_StackItemMacro *item = NULL;
    MMALLOC(item, SVAsm_StackItemMacro, 1);
    item->macro = macro;
    item->index = macro->value.function->start;
    item->args = NULL;
    MMALLOC(item->args, SVAsm_Arg*, macro->value.function->argCount);
    for(size_t i = 0; i < macro->value.function->argCount; i++)
        item->args[i] = NULL;
    return item;
}

void SVAsm_stackItemMacro_destroy(SVAsm_StackItemMacro *item)
{
    if(!item)
        return;
    for(size_t i = 0; i < item->macro->value.function->argCount; i++)
        SVAsm_arg_destroy(item->args[i]);
    FFREE(item->args);
    FFREE(item);
}

SVAsm_StackItemForLoop* SVAsm_stackItemForLoop_create(size_t start, SVAsm_Arg *iterable, char *name)
{
    SVAsm_StackItemForLoop *item = NULL;
    MMALLOC(item, SVAsm_StackItemForLoop, 1);
    item->start = start;
    item->iterable = SVAsm_arg_copy(iterable);
    item->name = NULL;
    if(name)
    {
        MMALLOC(item->name, char, strlen(name) + 1);
        strcpy(item->name, name);
    }
    item->index = start;
    item->step = 0;
    return item;
}

void SVAsm_stackItemForLoop_destroy(SVAsm_StackItemForLoop *item)
{
    if(!item)
        return;
    SVAsm_arg_destroy(item->iterable);
    if(item->name)
        FFREE(item->name);
    FFREE(item);
}

SVAsm_StackItemWhileLoop* SVAsm_stackItemWhileLoop_create(size_t start)
{
    SVAsm_StackItemWhileLoop *item = NULL;
    MMALLOC(item, SVAsm_StackItemWhileLoop, 1);
    item->start = start;
    item->index = start;
    return item;
}

void SVAsm_stackItemWhileLoop_destroy(SVAsm_StackItemWhileLoop *item)
{
    if(!item)
        return;
    FFREE(item);
}

SVAsm_StackItemCond* SVAsm_stackItemCond_create(size_t start, size_t index)
{
    SVAsm_StackItemCond *item = NULL;
    MMALLOC(item, SVAsm_StackItemCond, 1);
    item->start = start;
    item->index = index;
    return item;
}

void SVAsm_stackItemCond_destroy(SVAsm_StackItemCond *item)
{
    if(!item)
        return;
    FFREE(item);
}

SVAsm_Stack* SVAsm_stack_create(void)
{
    SVAsm_Stack *stack = NULL;
    MMALLOC(stack, SVAsm_Stack, 1);
    stack->items = NULL;
    stack->itemCount = 0;
    return stack;
}

void SVAsm_stack_destroy(SVAsm_Stack *stack)
{
    if(!stack)
        return;
    for(size_t i = 0; i < stack->itemCount; i++)
        SVAsm_stackItem_destroy(stack->items[i]);
    FFREE(stack->items);
    FFREE(stack);
}

void SVAsm_stack_push(SVAsm_Stack *stack, SVAsm_StackItem *item)
{
    if(!stack || !item)
        return;
    RREALLOC(stack->items, SVAsm_StackItem*, stack->itemCount + 1);
    stack->items[stack->itemCount] = item;
    stack->itemCount++;
}

void SVAsm_stack_pop(SVAsm_Stack *stack)
{
    if(!stack)
        return;
    stack->itemCount--;
    SVAsm_stackItem_destroy(stack->items[stack->itemCount]);
    RREALLOC(stack->items, SVAsm_StackItem*, stack->itemCount);
}

SVAsm_StackItem* SVAsm_stack_top(SVAsm_Stack *stack)
{
    if(!stack)
        return NULL;
    return stack->items[stack->itemCount - 1];
}
