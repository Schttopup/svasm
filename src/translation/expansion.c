/**
 * \file expansion.c
 * \brief Defines functions related to macro call expansion.
 * \author Schttopup
 */

#include "expansion.h"
#include "builtins.h"
#include "macrocallstack.h"
#include "translator_ins.h"
#include "translator_cmd.h"
#include "translator_mac.h"
#include "translator_var.h"
#include "../error.h"
#include "macro_defs.h"
#include <sdefs.h>
#include <string.h>


static SVAsm_Translator* SVAsm_expansion_getTopTranslator(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack)
{
    if(!translator || !stack)
        return NULL;
    for(ssize_t i = stack->itemCount - 1; i >= 0; i--)
    {
        if(stack->items[i]->type == SVASM_MACROCALLSTACKITEM_BLOCK)
            return stack->items[i]->value.block.translator;
    }
    return translator;
}

bool SVAsm_expansion_processMacroElement(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack)
{
    if(!translator)
        return false;
    SVAsm_Translator *localTranslator = SVAsm_expansion_getTopTranslator(translator, stack);
    SVAsm_MacroCallStackItem *top = SVAsm_macroCallStack_top(stack);
    if(top->type == SVASM_MACROCALLSTACKITEM_LINE ||
       top->type == SVASM_MACROCALLSTACKITEM_BUILTIN ||
       top->type == SVASM_MACROCALLSTACKITEM_ARRAY ||
       top->type == SVASM_MACROCALLSTACKITEM_LABEL ||
       top->type == SVASM_MACROCALLSTACKITEM_BLOCKPARAMS ||
       top->type == SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND ||
       top->type == SVASM_MACROCALLSTACKITEM_BLOCKMACRO)
    {
        SVAsm_Arg *arg = NULL;
        SVAsm_Arg *newArg = NULL;
        if(top->type == SVASM_MACROCALLSTACKITEM_LINE || top->type == SVASM_MACROCALLSTACKITEM_BUILTIN ||
           top->type == SVASM_MACROCALLSTACKITEM_BLOCKPARAMS || top->type == SVASM_MACROCALLSTACKITEM_BLOCKMACRO)
            arg = top->origin.call->args[top->index];
        else if(top->type == SVASM_MACROCALLSTACKITEM_ARRAY)
            arg = top->origin.array->args[top->index];
        else if(top->type == SVASM_MACROCALLSTACKITEM_LABEL)
            arg = top->origin.label->offset;
        else if(top->type == SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND)
        {
            arg = top->origin.command->args[top->index];
            if(!SVAsm_command_isArgExpandable(top->origin.command->command, top->index))
            {
                top->args[top->index] = SVAsm_arg_copy(arg);
                top->index++;
                return true;
            }
        }
        switch(arg->type)
        {
            case SVASM_ARG_NONE:
            case SVASM_ARG_NUMBER:
            case SVASM_ARG_STRING:
            case SVASM_ARG_REGISTER:
                newArg = SVAsm_arg_copy(arg);
                if(!newArg)
                    return false;
                top->args[top->index] = newArg;
                top->index++;
                break;
            case SVASM_ARG_LABEL:
            {
                if(arg->value.label->offset != NULL)
                {
                    SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getLabelStackItem(localTranslator, arg->value.label);
                    if(!newItem)
                    {
                        SVAsm_errorMacroStack(translator->options, stack);
                        return false;
                    }
                    SVAsm_macroCallStack_push(stack, newItem);
                }
                else
                {
                    newArg = SVAsm_arg_copy(arg);
                    if(!newArg)
                        return false;
                    top->args[top->index] = newArg;
                    top->index++;
                }
            }
                break;
            case SVASM_ARG_SYMBOL:
                newArg = SVAsm_expansion_expand(localTranslator, stack, arg->value.text);
                if(!newArg)
                    return false;
                top->args[top->index] = newArg;
                top->index++;
                break;
            case SVASM_ARG_MACRO:
            {
                SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getMacroStackItem(localTranslator, arg->value.macro);
                if(!newItem)
                {
                    SVAsm_errorMacroStack(translator->options, stack);
                    return false;
                }
                SVAsm_macroCallStack_push(stack, newItem);
            }
                break;
            case SVASM_ARG_ARRAY:
            {
                SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getArrayStackItem(localTranslator, arg->value.array);
                if(!newItem)
                {
                    SVAsm_errorMacroStack(translator->options, stack);
                    return false;
                }
                SVAsm_macroCallStack_push(stack, newItem);
            }
                break;
            default:
                SVAsm_errorMacroStack(translator->options, stack);
                return false;
                break;
        }
        if(!arg)
            return false;
    }
    else if(top->type == SVASM_MACROCALLSTACKITEM_BLOCK)
    {
        SVAsm_StackItem *localTop = SVAsm_stack_top(localTranslator->stack);
        size_t localIndex = SVAsm_stackItem_getLocation(localTop);
        if(localIndex >= localTranslator->blocks->blockCount)
            return false;
        SVAsm_Block *localBlock = localTranslator->blocks->blocks[localIndex];
        if(translator->stack->itemCount >= translator->options->maxTranslatorDepth)
        {
            SVAsm_error(translator->options, SVASM_ERROR, localBlock->filename, localBlock->filepath, localBlock->line,
                        "maximum translator depth reached (%d)", translator->options->maxTranslatorDepth);
            SVAsm_errorTranslator(translator->options, translator, localBlock, 0);
            SVASM_TRANSLATOR_ERROR();
        }
        SVAsm_MacroCallStackItem *newItem;
        switch(localBlock->type)
        {
            case SVASM_BLOCK_COMMAND:
                newItem = SVAsm_expansion_getBlockCommandStackItem(translator, localTranslator,
                                                                   localBlock->content.command);
                if(!newItem)
                {
                    SVAsm_errorMacroStack(translator->options, stack);
                    return false;
                }
                newItem->value.block.block = top->value.block.block;
                SVAsm_macroCallStack_push(stack, newItem);
                break;
            case SVASM_BLOCK_MACRO:
                newItem = SVAsm_expansion_getBlockMacroStackItem(translator, localTranslator,
                                                                 localBlock->content.macro);
                if(!newItem)
                {
                    SVAsm_errorMacroStack(translator->options, stack);
                    return false;
                }
                newItem->value.block.block = top->value.block.block;
                SVAsm_macroCallStack_push(stack, newItem);
                break;
            case SVASM_BLOCK_NONE:
                SVAsm_stackItem_setLocation(localTop, localIndex + 1);
                break;
            default:
                SVAsm_error(translator->options, SVASM_ERROR, localBlock->filename, localBlock->filepath, localBlock->line,
                            "only commands and macros can be used in block macros");
                SVAsm_errorTranslator(translator->options, translator, localBlock, 0);
                SVAsm_errorMacroStack(translator->options, stack);
                SVASM_TRANSLATOR_ERROR();
        }
    }
    else
        return false;
    return true;
}

SVAsm_Arg* SVAsm_expansion_processMacro(SVAsm_Translator *translator, SVAsm_MacroCall *macro)
{
    if(!translator || !macro)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_MacroCallStackItem *item = SVAsm_expansion_getMacroStackItem(translator, macro);
    if(!item)
        return NULL;
    SVAsm_Arg *ret = NULL;
    SVAsm_MacroCallStack *stack = SVAsm_macroCallStack_create();
    SVAsm_macroCallStack_push(stack, item);
    bool done = false;
    while(!done)
    {
        if(stack->itemCount >= translator->options->maxMacroDepth || stack->depth >= translator->options->maxMacroDepth)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "maximum macro depth reached (%d)", translator->options->maxMacroDepth);
            SVAsm_errorMacroStack(translator->options, stack);
            SVAsm_macroCallStack_destroy(stack);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
        SVAsm_MacroCallStackItem *top = SVAsm_macroCallStack_top(stack);
        SVAsm_Translator *topTranslator = SVAsm_expansion_getTopTranslator(translator, stack);
        size_t argCount = 0;
        if(top->type == SVASM_MACROCALLSTACKITEM_LINE || top->type == SVASM_MACROCALLSTACKITEM_BUILTIN ||
           top->type == SVASM_MACROCALLSTACKITEM_BLOCKPARAMS || top->type == SVASM_MACROCALLSTACKITEM_BLOCKMACRO)
            argCount = top->origin.call->argCount;
        else if(top->type == SVASM_MACROCALLSTACKITEM_ARRAY)
            argCount = top->origin.array->argCount;
        else if(top->type == SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND)
            argCount = top->origin.command->argCount;
        else if(top->type == SVASM_MACROCALLSTACKITEM_BLOCK || top->type == SVASM_MACROCALLSTACKITEM_LABEL)
            argCount = 1;
        if(top->index >= argCount)
        {
            SVAsm_Arg *newArg = NULL;
            switch(top->type)
            {
                case SVASM_MACROCALLSTACKITEM_LINE:
                {
                    SVAsm_MacroCall *newCall = SVAsm_macroCall_create(top->valueName);
                    for(size_t i = 0; i < top->value.line->content->value.macro->argCount; i++)
                        SVAsm_macroCall_add(newCall, SVAsm_arg_copy(top->value.line->content->value.macro->args[i]));
                    SVAsm_MacroCall *newCallExp = SVAsm_expansion_expandMacro(translator, stack, newCall);
                    SVAsm_macroCall_destroy(newCall);
                    SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getMacroStackItem(translator, newCallExp);
                    if(!newItem)
                    {
                        SVAsm_errorMacroStack(translator->options, stack);
                        SVAsm_macroCallStack_destroy(stack);
                        return NULL;
                    }
                    newItem->depth += top->depth;
                    SVAsm_macroCallStack_pop(stack);
                    SVAsm_macroCallStack_push(stack, newItem);
                    continue;
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_BUILTIN:
                {
                    if(top->value.builtin.macro == SVASM_BUILTIN_CALL)
                    {
                        if(top->origin.call->argCount < 1)
                        {
                            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                        "built-in macro \'_call\' requires at least 1 parameter, %d supplied", top->origin.call->argCount);
                            SVAsm_errorMacroStack(translator->options, stack);
                            SVAsm_macroCallStack_destroy(stack);
                            SVASM_TRANSLATOR_MAC_ERROR();
                        }
                        if(top->args[0]->type != SVASM_ARG_STRING)
                        {
                            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                        "built-in macro \'_call\' requires parameter 1 to be a string");
                            SVAsm_errorMacroStack(translator->options, stack);
                            SVAsm_macroCallStack_destroy(stack);
                            SVASM_TRANSLATOR_MAC_ERROR();
                        }
                        SVAsm_MacroCall *newCall = SVAsm_macroCall_create(top->args[0]->value.text);
                        for(size_t i = 1; i < top->origin.call->argCount; i++)
                            SVAsm_macroCall_add(newCall, SVAsm_arg_copy(top->args[i]));
                        SVAsm_MacroCall *newCallExp = SVAsm_expansion_expandMacro(translator, stack, newCall);
                        SVAsm_macroCall_destroy(newCall);
                        SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getMacroStackItem(translator, newCallExp);
                        if(!newItem)
                        {
                            SVAsm_errorMacroStack(translator->options, stack);
                            SVAsm_macroCallStack_destroy(stack);
                            return NULL;
                        }
                        newItem->depth += top->depth;
                        SVAsm_macroCallStack_pop(stack);
                        SVAsm_macroCallStack_push(stack, newItem);
                        continue;
                    }
                    else
                    {
                        newArg = SVAsm_expansion_callMacro(translator, top);
                        if(!newArg)
                        {
                            SVAsm_errorMacroStack(translator->options, stack);
                            SVAsm_macroCallStack_destroy(stack);
                            return NULL;
                        }
                        SVAsm_macroCallStack_pop(stack);
                    }
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_ARRAY:
                {
                    SVAsm_Array *newArray = SVAsm_array_create();
                    for(size_t i = 0; i < top->origin.array->argCount; i++)
                        SVAsm_array_insert(newArray, newArray->argCount, SVAsm_arg_copy(top->args[i]));
                    newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
                    SVAsm_macroCallStack_pop(stack);
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_LABEL:
                {
                    SVAsm_LabelCall *newLabel = SVAsm_labelCall_create(top->origin.label->label, top->origin.label->negative, top->args[0]);
                    newArg = SVAsm_arg_create(SVASM_ARG_LABEL, newLabel);
                    SVAsm_macroCallStack_pop(stack);
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_BLOCK:
                {
                    SVAsm_Translator *localTranslator = top->value.block.translator;
                    if(!localTranslator->returnValue)
                    {
                        SVAsm_macroCallStack_destroy(stack);
                        return NULL;
                    }
                    newArg = SVAsm_arg_copy(localTranslator->returnValue);
                    top->value.block.translator = NULL;
                    SVAsm_macroCallStack_pop(stack);
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_BLOCKPARAMS:
                {
                    SVAsm_MacroCallStackItem *newItem = SVAsm_expansion_getBlockStackItem(translator, top->value.block.translator, top);
                    top->value.block.translator = NULL;
                    if(!newItem)
                    {
                        SVAsm_errorMacroStack(translator->options, stack);
                        SVAsm_macroCallStack_destroy(stack);
                        return NULL;
                    }
                    newItem->depth += top->depth;
                    SVAsm_macroCallStack_pop(stack);
                    SVAsm_macroCallStack_push(stack, newItem);
                    continue;
                }
                case SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND:
                {
                    SVAsm_Translator *localTranslator = top->value.block.translator;
                    SVAsm_CommandLine *command = top->value.block.command;
                    for(size_t i = 0; i < top->value.block.command->argCount; i++)
                        command->args[i] = SVAsm_arg_copy(top->args[i]);
                    bool localRet = SVAsm_commandTranslations[command->command](localTranslator, command);
                    bool isReturn = (command->command == SVASM_COMMAND_RETURN);
                    if(!localRet)
                    {
                        SVAsm_errorMacroStack(translator->options, stack);
                        SVAsm_macroCallStack_destroy(stack);
                        return NULL;
                    }
                    SVAsm_macroCallStack_pop(stack);
                    if(isReturn)
                    {
                        top = SVAsm_macroCallStack_top(stack);
                        top->args[top->index] = SVAsm_arg_copy(localTranslator->returnValue);
                        top->index++;
                    }
                    if(translator->stop)
                    {
                        SVAsm_errorMacroStack(translator->options, stack);
                        SVAsm_macroCallStack_destroy(stack);
                        return NULL;
                    }
                    SVAsm_StackItem *localTop = SVAsm_stack_top(localTranslator->stack);
                    size_t localIndex = SVAsm_stackItem_getLocation(localTop);
                    localIndex = SVAsm_stackItem_getLocation(localTop);
                    localIndex++;
                    if(localIndex >= localTranslator->blocks->blockCount)
                        localTranslator->stop = true;
                    SVAsm_stackItem_setLocation(SVAsm_stack_top(localTranslator->stack), localIndex);
                    continue;
                }
                    break;
                case SVASM_MACROCALLSTACKITEM_BLOCKMACRO:
                {
                    SVAsm_Translator *localTranslator = top->value.block.translator;
                    SVAsm_StackItemMacro *itemMacro = SVAsm_stackItemMacro_create(top->macro);
                    for(size_t i = 0; i < top->macro->value.function->argCount; i++)
                        itemMacro->args[i] = SVAsm_arg_copy(top->args[i]);
                    SVAsm_stack_push(localTranslator->stack, SVAsm_stackItem_create(SVASM_STACK_MACRO, itemMacro));
                    SVAsm_macroCallStack_pop(stack);
                    SVAsm_StackItem *localTop = SVAsm_stack_top(localTranslator->stack);
                    size_t localIndex = SVAsm_stackItem_getLocation(localTop);
                    localIndex = SVAsm_stackItem_getLocation(localTop);
                    localIndex++;
                    if(localIndex >= localTranslator->blocks->blockCount)
                        localTranslator->stop = true;
                    SVAsm_stackItem_setLocation(SVAsm_stack_top(localTranslator->stack), localIndex);
                    continue;
                }
                    break;
                default:
                    return NULL;
                    break;
            }
            top = SVAsm_macroCallStack_top(stack);
            if(top)
            {
                if(!newArg)
                {
                    SVAsm_macroCallStack_destroy(stack);
                    return NULL;
                }
                top->args[top->index] = newArg;
                top->index++;
            }
            else
            {
                ret = newArg;
                done = true;
            }
            continue;
        }
        if(!SVAsm_expansion_processMacroElement(topTranslator, stack))
        {
            SVAsm_macroCallStack_destroy(stack);
            return NULL;
        }
    }
    SVAsm_macroCallStack_destroy(stack);
    return ret;
}

SVAsm_Arg* SVAsm_expansion_callMacro(SVAsm_Translator *translator, SVAsm_MacroCallStackItem *macro)
{
    if(!translator || !macro)
        return NULL;
    if(macro->type != SVASM_MACROCALLSTACKITEM_BUILTIN)
        return NULL;
    SVAsm_Arg *ret = NULL;
    SVAsm_MacroCall *builtinCall = SVAsm_macroCall_create(macro->origin.call->name);
    for(size_t i = 0; i < macro->origin.call->argCount; i++)
        SVAsm_macroCall_add(builtinCall, SVAsm_arg_copy(macro->args[i]));
    ret = (*(macro->value.builtin.function))(translator, builtinCall);
    SVAsm_macroCall_destroy(builtinCall);
    return ret;
}

SVAsm_MacroCallStackItem *SVAsm_expansion_getMacroStackItem(SVAsm_Translator *translator, SVAsm_MacroCall *macro)
{
    if(!translator || !macro)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_MacroCallStackItem *item = SVAsm_macroCallStackItem_create();
    SVAsm_Macro *definedMacro = NULL;
    SVAsm_MacroList *localList = SVAsm_expansion_getLocalMacroList(translator, macro->name);
    if(!localList)
        localList = translator->macros;
    definedMacro = SVAsm_macroList_get(localList, macro->name);
    if(definedMacro)
    {
        switch(definedMacro->type)
        {
            case SVASM_MACRO_LINE:
            {
                SVAsm_MacroLine *line = definedMacro->value.line;
                if (line->argCount != macro->argCount)
                {
                    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                "macro \'%s\' requires %d parameters, %d supplied",
                                macro->name, line->argCount, macro->argCount);
                    SVAsm_errorTranslator(translator->options, translator, block, 0);
                    SVAsm_macroCallStackItem_destroy(item);
                    SVASM_TRANSLATOR_MAC_ERROR();
                }
                MMALLOC(item->valueName, char, strlen(line->content->value.macro->name) + 1);
                strcpy(item->valueName, line->content->value.macro->name);
                item->type = SVASM_MACROCALLSTACKITEM_LINE;
                item->value.line = line;
            }
                break;
            case SVASM_MACRO_BLOCK:
            {
                SVAsm_MacroBlock *macroBlock = definedMacro->value.block;
                if (macroBlock->argCount != macro->argCount)
                {
                    SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                                "macro \'%s\' requires %d parameters, %d supplied",
                                macro->name, macroBlock->argCount, macro->argCount);
                    SVAsm_errorTranslator(translator->options, translator, block, 0);
                    SVAsm_macroCallStackItem_destroy(item);
                    SVASM_TRANSLATOR_MAC_ERROR();
                }
                MMALLOC(item->valueName, char, strlen(macroBlock->macro->name) + 1);
                strcpy(item->valueName, macroBlock->macro->name);
                item->type = SVASM_MACROCALLSTACKITEM_BLOCKPARAMS;
                SVAsm_Translator *nestedTranslator = SVAsm_translator_createNested(translator);
                SVAsm_StackItem *root = SVAsm_stackItem_create(SVASM_STACK_BLOCK, SVAsm_stackItemMacro_create(definedMacro));
                root->value.macro->index++;
                SVAsm_stack_push(nestedTranslator->stack, root);
                item->value.block.translator = nestedTranslator;
                item->value.block.command = NULL;
                item->value.block.block = macroBlock;
            }
                break;
            default:
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "macro \'%s\' called as inline macro", macro->name);
                SVAsm_errorTranslator(translator->options, translator, block, 0);
                SVAsm_macroCallStackItem_destroy(item);
                SVASM_TRANSLATOR_MAC_ERROR();
                break;
        }
    }
    else
    {
        SVAsm_BuiltinMacro builtin = SVAsm_translator_getBuiltinMacro(macro->name);
        if(builtin != SVASM_BUILTINMAC_NONE)
        {
            item->type = SVASM_MACROCALLSTACKITEM_BUILTIN;
            item->value.builtin.macro = builtin;
            item->value.builtin.function = SVAsm_BuiltinMacroTranslations[builtin];
            MMALLOC(item->valueName, char, strlen(SVAsm_BuiltinMacroNames[builtin]) + 1);
            strcpy(item->valueName, SVAsm_BuiltinMacroNames[builtin]);
        }
    }

    if(item->type == SVASM_MACROCALLSTACKITEM_NONE)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", macro->name);
        SVAsm_macroCallStackItem_destroy(item);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    item->origin.call = macro;
    item->macro = definedMacro;
    item->args = NULL;
    MMALLOC(item->args, SVAsm_Arg*, item->origin.call->argCount);
    for(size_t i = 0; i < item->origin.call->argCount; i++)
        item->args[i] = NULL;
    item->index = 0;
    return item;
}

SVAsm_MacroCallStackItem* SVAsm_expansion_getArrayStackItem(SVAsm_Translator *translator, SVAsm_Array *array)
{
    if(!translator || !array)
        return NULL;
    SVAsm_MacroCallStackItem *item = SVAsm_macroCallStackItem_create();
    item->type = SVASM_MACROCALLSTACKITEM_ARRAY;
    item->origin.array = array;
    item->args = NULL;
    MMALLOC(item->args, SVAsm_Arg*, array->argCount);
    for(size_t i = 0; i < array->argCount; i++)
        item->args[i] = NULL;
    item->index = 0;
    return item;
}

SVAsm_MacroCallStackItem* SVAsm_expansion_getLabelStackItem(SVAsm_Translator *translator, SVAsm_LabelCall *label)
{
    if(!translator || !label)
        return NULL;
    SVAsm_MacroCallStackItem *item = SVAsm_macroCallStackItem_create();
    item->type = SVASM_MACROCALLSTACKITEM_LABEL;
    item->origin.label = label;
    item->args = NULL;
    MMALLOC(item->args, SVAsm_Arg*, 1);
    item->args[0] = NULL;
    item->index = 0;
    return item;
}

SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_MacroCallStackItem *parent)
{
    if(!translator || !nestedTranslator)
        return NULL;
    SVAsm_MacroCallStackItem *newItem = SVAsm_macroCallStackItem_create();
    newItem->type = SVASM_MACROCALLSTACKITEM_BLOCK;
    newItem->args = NULL;
    MMALLOC(newItem->args, SVAsm_Arg*, 1);
    newItem->args[0] = NULL;
    newItem->value.block.translator = nestedTranslator;
    newItem->value.block.block = parent->value.block.block;
    newItem->origin.call = parent->origin.call;
    newItem->macro = parent->macro;
    for(size_t i = 0; i < newItem->origin.call->argCount; i++)
        newItem->value.block.translator->stack->items[0]->value.macro->args[i] = SVAsm_arg_copy(parent->args[i]);
    newItem->index = 0;
    return newItem;
}

SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockCommandStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_CommandLine *command)
{
    if(!translator || !nestedTranslator || !command)
        return NULL;
    SVAsm_MacroCallStackItem *newItem = SVAsm_macroCallStackItem_create();
    newItem->type = SVASM_MACROCALLSTACKITEM_BLOCKCOMMAND;
    newItem->origin.command = command;
    newItem->args = NULL;
    MMALLOC(newItem->args, SVAsm_Arg*, command->argCount);
    for(size_t i = 0; i < command->argCount; i++)
        newItem->args[i] = NULL;
    newItem->value.block.translator = nestedTranslator;
    newItem->value.block.command = SVAsm_commandLine_create(command->command);
    newItem->value.block.command->argCount = command->argCount;
    MMALLOC(newItem->value.block.command->args, SVAsm_Arg*, command->argCount);
    for(size_t i = 0; i < command->argCount; i++)
        newItem->value.block.command->args[i] = NULL;
    return newItem;
}

SVAsm_MacroCallStackItem* SVAsm_expansion_getBlockMacroStackItem(SVAsm_Translator *translator, SVAsm_Translator *nestedTranslator, SVAsm_MacroCall *macro)
{
    if(!translator || !nestedTranslator || !macro)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_Macro *definedMacro = NULL;
    SVAsm_MacroList *localList = SVAsm_expansion_getLocalMacroList(translator, macro->name);
    if(!localList)
        localList = translator->macros;
    definedMacro = SVAsm_macroList_get(localList, macro->name);
    if(!definedMacro)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' undefined", macro->name);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    if(definedMacro->type != SVASM_MACRO_FUNCTION)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "macro \'%s\' called as function macro", macro->name);
        SVAsm_errorTranslator(translator->options, translator, block, 0);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    SVAsm_MacroCallStackItem *newItem = SVAsm_macroCallStackItem_create();
    newItem->type = SVASM_MACROCALLSTACKITEM_BLOCKMACRO;
    newItem->macro = definedMacro;
    newItem->origin.call = macro;
    newItem->args = NULL;
    MMALLOC(newItem->args, SVAsm_Arg*, macro->argCount);
    for(size_t i = 0; i < macro->argCount; i++)
        newItem->args[i] = NULL;
    newItem->value.block.translator = nestedTranslator;
    return newItem;
}

SVAsm_Arg* SVAsm_expansion_expand(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, char const *symbol)
{
    if(!translator || !symbol)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_Arg *arg = NULL;
    bool done = false;
    // Look for inline macro parameters
    if(stack)
    {
        SVAsm_MacroCallStackItem *top = SVAsm_macroCallStack_top(stack);
        if(top->type == SVASM_MACROCALLSTACKITEM_LINE)
        {
            for(size_t i = 0; i < top->origin.call->argCount; i++)
            {
                if(strcmp(top->value.line->args[i]->value.text, symbol) == 0)
                {
                    arg = SVAsm_arg_copy(top->args[i]);
                    break;
                }
            }
        }
    }
    if(arg)
        return arg;
    // Look for locals and for-loop counters
    for(size_t i = translator->stack->itemCount; i > 0 && !done; i--)
    {
        SVAsm_StackItem *item = translator->stack->items[i - 1];
        // Search locals
        SVAsm_Macro *macro = SVAsm_macroList_get(item->locals, symbol);
        if(macro)
        {
            if(macro->type == SVASM_MACRO_CONSTANT)
            {
                arg = SVAsm_arg_copy(macro->value.constant->arg);
                done = true;
            }
            else if(macro->type == SVASM_MACRO_FUNCTION || macro->type == SVASM_MACRO_LINE || macro->type == SVASM_MACRO_BLOCK)
            {
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "macro \'%s\' used as a constant", symbol);
                SVAsm_errorTranslator(translator->options, translator, block, 0);
                SVASM_TRANSLATOR_MAC_ERROR();
            }
            else
            {
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "macro \'%s\' is local but undefined", symbol);
                SVAsm_errorTranslator(translator->options, translator, block, 0);
                SVASM_TRANSLATOR_MAC_ERROR();
            }
        }
        if(!arg)
        {
            // Search macro parameters
            if(item->type == SVASM_STACK_MACRO || item->type == SVASM_STACK_BLOCK)
            {
                for(size_t j = 0; j < item->value.macro->macro->value.function->argCount; j++)
                {
                    if(strcmp(item->value.macro->macro->value.function->args[j]->value.text, symbol) == 0)
                    {
                        arg = SVAsm_arg_copy(item->value.macro->args[j]);
                        done = true;
                        break;
                    }
                }
                done = true;
                break;
            }
            // search for loop iterator
            if(item->type == SVASM_STACK_LOOP_FOR)
            {
                if(item->value.forLoop->name)
                {
                    if(strcmp(symbol, item->value.forLoop->name) == 0)
                    {
                        if(item->value.forLoop->iterable->type == SVASM_ARG_ARRAY)
                            arg = SVAsm_arg_copy(item->value.forLoop->iterable->value.array->args[item->value.forLoop->step]);
                        if(item->value.forLoop->iterable->type == SVASM_ARG_STRING)
                        {
                            char newString[2] = {item->value.forLoop->iterable->value.text[item->value.forLoop->step], '\0'};
                            arg = SVAsm_arg_create(SVASM_ARG_STRING, newString);
                        }
                        done = true;
                    }
                }
            }
        }
    }
    if(arg)
        return arg;
    // Look for globals
    SVAsm_Macro *macro = SVAsm_macroList_get(translator->macros, symbol);
    if(macro)
    {
        if(macro->type == SVASM_MACRO_CONSTANT)
            arg = SVAsm_arg_copy(macro->value.constant->arg);
        else
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "macro \'%s\' used as a constant", symbol);
            SVAsm_errorTranslator(translator->options, translator, block, 0);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
    }
    if(arg)
        return arg;
    // Look for builtins
    for(size_t i = 0; i < SVASM_BUILTINVAR_LAST; i++)
    {
        if(strcmp(SVAsm_BuiltinVariableNames[i], symbol) == 0)
        {
            arg = (*SVAsm_BuiltinVariableTranslations[i])(translator);
            break;
        }
    }
    if(!arg)
    {
        SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                    "symbol \'%s\' undefined", symbol);
        if(stack)
            SVAsm_errorMacroStack(translator->options, stack);
        SVASM_TRANSLATOR_MAC_ERROR();
    }
    return arg;
}

SVAsm_Arg* SVAsm_expansion_expandLabel(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_LabelCall *call)
{
    if(!translator ||!call)
        return NULL;
    size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
    SVAsm_Block *block = translator->blocks->blocks[index];
    SVAsm_Arg *expandedOffset = NULL;
    if(call->offset)
    {
        expandedOffset = SVAsm_expansion_expandArg(translator, call->offset);
        if(!expandedOffset)
            return NULL;
        if(expandedOffset->type != SVASM_ARG_NUMBER)
        {
            SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                        "label offset must be a number");
            if(stack)
                SVAsm_errorMacroStack(translator->options, stack);
            SVAsm_arg_destroy(expandedOffset);
            SVASM_TRANSLATOR_MAC_ERROR();
        }
    }
    SVAsm_LabelCall *newCall = SVAsm_labelCall_create(call->label, call->negative, expandedOffset);
    SVAsm_Arg *newArg = SVAsm_arg_create(SVASM_ARG_LABEL, newCall);
    return newArg;
}

SVAsm_MacroCall* SVAsm_expansion_expandMacro(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_MacroCall *call)
{
    if(!translator || !call)
        return NULL;
    SVAsm_MacroCall *newCall = SVAsm_macroCall_create(call->name);
    for(size_t i = 0; i < call->argCount; i++)
    {
        switch(call->args[i]->type)
        {
            case SVASM_ARG_SYMBOL:
            {
                SVAsm_Arg *newArg = SVAsm_expansion_expand(translator, stack, call->args[i]->value.text);
                if(!newArg)
                {
                    SVAsm_macroCall_destroy(newCall);
                    return NULL;
                }
                SVAsm_macroCall_add(newCall, newArg);

            }
                break;
            case SVASM_ARG_MACRO:
            {
                SVAsm_MacroCall *innerCall = SVAsm_expansion_expandMacro(translator, stack, call->args[i]->value.macro);
                if(!innerCall)
                {
                    SVAsm_macroCall_destroy(newCall);
                    return NULL;
                }
                SVAsm_macroCall_add(newCall, SVAsm_arg_create(SVASM_ARG_MACRO, innerCall));
            }
                break;
            case SVASM_ARG_ARRAY:
            {
                SVAsm_Array *innerArray = SVAsm_expansion_expandArray(translator, stack, call->args[i]->value.array);
                if(!innerArray)
                {
                    SVAsm_macroCall_destroy(newCall);
                    return NULL;
                }
                SVAsm_macroCall_add(newCall, SVAsm_arg_create(SVASM_ARG_ARRAY, innerArray));
            }
                break;
            default:
                SVAsm_macroCall_add(newCall, SVAsm_arg_copy(call->args[i]));
                break;
        }
    }
    return newCall;
}

SVAsm_Array* SVAsm_expansion_expandArray(SVAsm_Translator *translator, SVAsm_MacroCallStack *stack, SVAsm_Array *array)
{
    if(!translator || !array)
        return NULL;
    SVAsm_Array *newArray = SVAsm_array_create();
    for(size_t i = 0; i < array->argCount; i++)
    {
        switch(array->args[i]->type)
        {
            case SVASM_ARG_SYMBOL:
            {
                SVAsm_Arg *newArg = SVAsm_expansion_expand(translator, stack, array->args[i]->value.text);
                if(!newArg)
                {
                    SVAsm_array_destroy(newArray);
                    return NULL;
                }
                SVAsm_array_insert(newArray, newArray->argCount, newArg);
            }
                break;
            case SVASM_ARG_MACRO:
            {
                SVAsm_MacroCall *innerCall = SVAsm_expansion_expandMacro(translator, stack, array->args[i]->value.macro);
                if(!innerCall)
                {
                    SVAsm_array_destroy(newArray);
                    return NULL;
                }
                SVAsm_array_insert(newArray, newArray->argCount, SVAsm_arg_create(SVASM_ARG_MACRO, innerCall));
            }
                break;
            case SVASM_ARG_ARRAY:
            {
                SVAsm_Array *innerArray = SVAsm_expansion_expandArray(translator, stack, array->args[i]->value.array);
                if(!innerArray)
                {
                    SVAsm_array_destroy(newArray);
                    return NULL;
                }
                SVAsm_array_insert(newArray, newArray->argCount, SVAsm_arg_create(SVASM_ARG_ARRAY, innerArray));
            }
                break;
            default:
                SVAsm_array_insert(newArray, newArray->argCount, SVAsm_arg_copy(array->args[i]));
                break;
        }
    }
    return newArray;
}

SVAsm_Arg* SVAsm_expansion_expandArg(SVAsm_Translator *translator, SVAsm_Arg *arg)
{
    if(!translator || !arg)
        return NULL;
    SVAsm_Arg *newArg = NULL;
    switch(arg->type)
    {
        case SVASM_ARG_MACRO:
        {
            SVAsm_MacroCall *call = SVAsm_expansion_expandMacro(translator, NULL, arg->value.macro);
            newArg = SVAsm_expansion_processMacro(translator, call);
            SVAsm_macroCall_destroy(call);
        }
            break;
        case SVASM_ARG_SYMBOL:
            newArg = SVAsm_expansion_expand(translator, NULL, arg->value.text);
            break;
        case SVASM_ARG_ARRAY:
        {
            SVAsm_Array *newArray = SVAsm_expansion_expandArray(translator, NULL, arg->value.array);
            newArg = SVAsm_arg_create(SVASM_ARG_ARRAY, newArray);
        }
            break;
        default:
            newArg = SVAsm_arg_copy(arg);
            break;
    }
    return newArg;
}

SVAsm_Operand* SVAsm_expansion_expandOperand(SVAsm_Translator *translator, SVAsm_Operand *operand)
{
    if(!translator || !operand)
        return NULL;
    SVAsm_Arg *newArg = NULL;
    SVAsm_Operand *newOperand = NULL;
    if(operand->type != SVASM_OPERAND_MACRO && operand->type != SVASM_OPERAND_SYMBOL && operand->type != SVASM_OPERAND_LABEL)
        newOperand = SVAsm_operand_copy(operand);
    else
    {
        switch(operand->type)
        {
            case SVASM_OPERAND_MACRO:
            {
                SVAsm_MacroCall *call = SVAsm_expansion_expandMacro(translator, NULL, operand->value.macro);
                newArg = SVAsm_expansion_processMacro(translator, call);
                SVAsm_macroCall_destroy(call);
            }
                break;
            case SVASM_OPERAND_SYMBOL:
                newArg = SVAsm_expansion_expand(translator, NULL, operand->value.text);
                break;
            case SVASM_OPERAND_LABEL:
                newArg = SVAsm_expansion_expandLabel(translator, NULL, operand->value.label);
                break;
            default:
                break;
        }
        if(newArg)
        {
            newOperand = SVAsm_operand_fromArg(newArg);
            SVAsm_arg_destroy(newArg);
            if(!newOperand)
            {
                size_t index = SVAsm_stackItem_getLocation(SVAsm_stack_top(translator->stack));
                SVAsm_Block *block = translator->blocks->blocks[index];
                SVAsm_error(translator->options, SVASM_ERROR, block->filename, block->filepath, block->line,
                            "invalid operand type");
                SVASM_TRANSLATOR_MAC_ERROR();
            }
        }
    }
    return newOperand;
}

SVAsm_MacroList* SVAsm_expansion_getLocalMacroList(SVAsm_Translator *translator, char const *symbol)
{
    if(!translator || !symbol)
        return NULL;
    SVAsm_MacroList *list = NULL;
    for(ssize_t i = translator->stack->itemCount - 1; i >= 0; i--)
    {
        if(SVAsm_macroList_get(translator->stack->items[i]->locals, symbol))
        {
            list = translator->stack->items[i]->locals;
            break;
        }
        if(translator->stack->items[i]->type == SVASM_STACK_MACRO)
            break;
    }
    return list;
}
