/**
 * \file translator_cmd.h
 * \brief Declares functions related to svasm commands.
 * \author Schttopup
 */

#ifndef SVASM_TRANSLATOR_CMD_H
#define SVASM_TRANSLATOR_CMD_H

#include "../building/command.h"
#include <stdbool.h>
#include <stdlib.h>


/**
 * Code translator.
 */
struct SVAsm_Translator_;


/// Function name for command translations.
typedef bool (*SVAsm_CommandTranslation)(struct SVAsm_Translator_*, SVAsm_CommandLine *);

/// List of command translation functions.
extern SVAsm_CommandTranslation const SVAsm_commandTranslations[SVASM_COMMAND_LAST];


bool SVAsm_translator_translateOrigin(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateExport(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateMeta(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateInclude(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateBinary(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateLoad(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateBytes(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateSpace(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateStack(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateDefine(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateSet(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateAssign(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateDelete(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateLocal(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateUnlocal(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateCall(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateIf(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateElse(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateFor(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateWhile(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateMacro(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateBlock(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateEnd(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateReturn(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateMessage(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateWarning(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

bool SVAsm_translator_translateError(struct SVAsm_Translator_ *translator, SVAsm_CommandLine *command);

/**
 * Gets the index of the closing .end command of a block.
 * \param translator The translator
 * \param index The index of the closing .end command
 * \param elseIndex The index of the corresponding .else command, if applicable
 * \return true if the closing .end command is found, false otherwise
 */
bool SVAsm_translator_getClosingEnd(struct SVAsm_Translator_ *translator, size_t *index, size_t *elseIndex);

#endif //SVASM_TRANSLATOR_CMD_H
