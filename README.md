SVAsm
=====
SVAsm is a powerful assembler for [SVExec](https://gitlab.com/Schttopup/svexec) bytecode.


## License
SVAsm is under [LGPLv3](LICENSE) license (https://www.gnu.org/licenses/lgpl-3.0.txt).

