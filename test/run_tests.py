import os
import sys
import subprocess
import sfile_ssf as ssf


INPUT_FOLDER = "input"
OUTPUT_FOLDER = "output"
REFERENCE_FOLDER = "reference"
VALGRIND_OPTS = ["valgrind", "--leak-check=full", "--show-reachable=yes", "--log-file=TODO", "--"]


def compare_svb(printer, out_file, ref_file):
    out_name = os.path.basename(out_file)
    ref_name = os.path.basename(ref_file)
    try:
        with open(out_file, "rb") as f:
            out_tree = ssf.SSFTree.decode(f.read())
    except IOError:
        printer.test_fail(f"{out_name}: missing output file")
        return False
    try:
        with open(ref_file, "rb") as f:
            ref_tree = ssf.SSFTree.decode(f.read())
    except IOError:
        printer.test_warn(f"{ref_name}: missing reference file")
        return True

    def compare_svb_chunk(chunk_name):
        out_data = out_tree.get_chunks(chunk_name)
        if not out_data:
            printer.test_fail(f"{out_name}: missing chunk {chunk_name}")
            return False
        if len(out_data) > 1:
            printer.test_fail(f"{out_name}: invalid SVB chunk {chunk_name}")
            return False
        ref_data = ref_tree.get_chunks(chunk_name)
        if not out_data or len(out_data) > 1:
            printer.test_warn(f"{ref_name}: invalid reference {chunk_name}")
            return True
        if out_data[0].data != ref_data[0].data:
            printer.test_fail(f"{out_name}: {chunk_name} different from reference")
            return False
        return True

    failing = False
    failing = failing or not compare_svb_chunk("HEAD")
    failing = failing or not compare_svb_chunk("DATA")
    failing = failing or not compare_svb_chunk("EXPT")
    return not failing


def cleanup_paths(filename, path):
    with open(filename, "r") as f:
        filedata = f.readlines()
    filedata_clean = [line.replace(path, "<OUTPUT>") for line in filedata]
    with open(filename, "w") as f:
        f.writelines(filedata_clean)


class TestPrinter:
    def __init__(self):
        self.rank = 0

    def test_start(self, message):
        if self.rank > 0:
            prefix = '\u2502 ' * (self.rank - 1) + '\u251C'
            print(f"{prefix}\u2500\u252C\u2500 {message}")
        else:
            print(f"\u250C\u2500 {message}")
        self.rank += 1

    def test_end(self, message, passing=True):
        if passing:
            self.test_pass(message, True)
        else:
            self.test_fail(message, True)
        self.rank -= 1

    def test_pass(self, message, bottom=False):
        prefix = '\u2502 ' * (self.rank - 1)
        pipe = '\u251C\u2500' if not bottom else '\u2514\u2500'
        print(f"{prefix}{pipe} \x1B[0;92m\u2713\x1B[0m {message}")

    def test_warn(self, message, bottom=False):
        prefix = '\u2502 ' * (self.rank - 1)
        pipe = '\u251C\u2500' if not bottom else '\u2514\u2500'
        print(f"{prefix}{pipe} \x1B[0;93m?\x1B[0m {message}")

    def test_fail(self, message, bottom=False):
        prefix = '\u2502 ' * (self.rank - 1)
        pipe = '\u251C\u2500' if not bottom else '\u2514\u2500'
        print(f"{prefix}{pipe} \x1B[0;91m\u2717\x1B[0m {message}")


def main():
    print("SVAsm automatic tests")
    if len(sys.argv) != 3:
        print("Invalid program arguments")
        return
    program_dir = os.path.abspath(sys.argv[1])
    test_dir = os.path.abspath(sys.argv[2])
    print(f"Program = {program_dir}\n")
    print(f"Test directory = {test_dir}\n")
    input_dir = os.path.join(test_dir, "input")
    output_dir = os.path.join(test_dir, "output")
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    reference_dir = os.path.join(test_dir, "reference")
    passing_tests = 0
    total_tests = 0
    printer = TestPrinter()

    for folder in os.listdir(input_dir):
        printer.test_start(folder)
        folder_failing = False
        for file in (f for f in os.listdir(os.path.join(input_dir, folder)) if f.endswith(".sva")):
            if not os.path.exists(os.path.join(output_dir, folder)):
                os.mkdir(os.path.join(output_dir, folder))
            filename, fileext = os.path.splitext(file)
            printer.test_start(filename)

            output_binary = os.path.join(output_dir, folder, f"{filename}.svb")
            reference_binary = os.path.join(reference_dir, folder, f"{filename}.svb")
            output_binary_invalid = os.path.join(output_dir, folder, f"{filename}_invalid.svb")
            process_opts = [program_dir,
                            "-i", os.path.join(input_dir, folder, file),
                            "-o", output_binary,
                            "-I", os.path.join(input_dir, folder),
                            "--no-color"]
            process_output = os.path.join(output_dir, folder, f"{filename}_out.txt")
            with open(process_output, "w") as of:
                process_valid = subprocess.Popen(process_opts, stdout=of)
                process_valid.wait()
            cleanup_paths(process_output, output_binary)
            process_opts = [program_dir,
                            "-i", os.path.join(input_dir, folder, file),
                            "-o", output_binary_invalid,
                            "-I", os.path.join(input_dir, folder),
                            "-DINVALID",
                            "--no-color"]
            process_output = os.path.join(output_dir, folder, f"{filename}_out_invalid.txt")
            with open(process_output, "w") as of:
                process_invalid = subprocess.Popen(process_opts, stdout=of)
                process_invalid.wait()
            cleanup_paths(process_output, output_binary_invalid)
            failing = False
            if process_valid.returncode != 0 or process_invalid.returncode != 1:
                printer.test_fail(f"{file} : bad return code")
                failing = True

            for suffix in ["_out.txt", "_out_invalid.txt"]:
                reference_file = os.path.join(reference_dir, folder, f"{filename}{suffix}")
                output_file = os.path.join(output_dir, folder, f"{filename}{suffix}")
                if not os.path.exists(reference_file):
                    continue
                if not os.path.exists(output_file):
                    printer.test_fail(f"{filename}{suffix} : file does not exist")
                    failing = True
                else:
                    output_file_content = open(output_file).read()
                    reference_file_content = open(reference_file).read()
                    if output_file_content != reference_file_content:
                        printer.test_fail(f"{filename}{suffix} : different from reference")
                        failing = True
                    else:
                        printer.test_pass(f"{filename}{suffix}")

            binary_ok = compare_svb(printer, output_binary, reference_binary)
            failing = failing or (not binary_ok)

            if not failing:
                passing_tests += 1
            else:
                folder_failing = True
            printer.test_end(f"{filename}", not failing)
            total_tests += 1
        printer.test_end(folder, not folder_failing)

    print(f"\n{passing_tests} / {total_tests} tests passed")


if __name__ == "__main__":
    main()
